<?php

ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

require 'app/Mage.php';
Mage::setIsDeveloperMode(true);
Mage::app('admin');

error_reporting(E_ERROR);

Mage::getModel('tunda_deleteorders/sales')->cancelOldOrders();