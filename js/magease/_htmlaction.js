$j(document).ready(function(){

    /**
    *
    *decrease increase cart input function.
    *
    **/

    $j('.cart-quantity-decrease').click(function() {
      currentval = parseInt($j(this).next('.qty-count').children('input.qty').val());
      if (currentval > 1) {
        $j(this).next('.qty-count').children('input.qty').val(currentval - 1);
      }
    });
    $j('.cart-quantity-increase').click(function() {
      currentval = parseInt($j(this).prev('.qty-count').children('input.qty').val());
      $j(this).prev('.qty-count').children('input.qty').val(currentval + 1);
    });


    /**
    *
    *add to card
    *
    **/
    $j('li .list-cart').click(function(){
      var pid = $j(this).attr('id');
      $j('body').append('<div id="add-to-cart-success"><div>添加商品成功</div></div>');
      setTimeout(function () {$j('#add-to-cart-success').fadeOut(500).remove()}, 2000);                                                
    });
    /*PC qty*/
    $j(".qty-decrease").click(function(){
      currentval=parseInt($j(this).next("input.qty").val());
      if(currentval>1){
        $j(this).next("input.qty").val(currentval-1);
        // $j("input.qty").attr("val",currentval-1);
      }
    });
    $j(".qty-increase").click(function(){
      currentval=parseInt($j(this).prev("input.qty").val());
      currentmax=parseInt($j(this).prev("input.qty").attr("max"));
      if(currentmax&&currentval<currentmax)
      {
        $j(this).prev("input.qty").val(currentval+1);
        // $j("input.qty").attr("val",currentval+1);
      }else{
        $j(this).prev("input.qty").val(currentval+1);
      }
    });
    /*window function scroll*/
    $j(window).scroll(function(){
      if($j('#header').length){
        var winTop2= $j(window).scrollTop(); 

        var producttab = $j('.header-standard').offset().top;
        if(winTop2 >= producttab){
          $j('.right-top').addClass('active');
          //$j('#header').addClass('tabfix');
          $j('.page').addClass('tabfix');
          // $j('.cms-index-index #header').removeClass('tabfix');
        }else{
          $j('.right-top').removeClass('active');
          //$j('#header').removeClass('tabfix');
          $j('.page').removeClass('tabfix');
        };
      }
    });

    var actionheight=parseInt($j('#topCartContent .actions').outerHeight());
    var documentheight=parseInt($j(window).height());
    var cartheight=documentheight-actionheight;
    $j('#topCartContent .minicart-wrapper').height(cartheight+'px');

    $j(window).resize(function(){
      var actionheight=parseInt($j('#topCartContent .actions').outerHeight());
      var documentheight=parseInt($j(window).height());
      var cartheight=documentheight-actionheight;
      $j('#topCartContent .minicart-wrapper').height(cartheight+'px');
    });


    $j("button.btn-cart").click(function(event){
      var offset = $j(".top-cart #cartHeader").offset();
      var addcar = $j(this);
      if(addcar.parents('.products-grid li.item').find('.product-image img').attr('src')){
        var img = addcar.parents('.products-grid li.item').find('.product-image img').attr('src');
      }else{
        var img = addcar.parents('.category-box-list li.item').find('.product-image img').attr('src');
      }
      var flyer = $j('<img class="img-flyer" src="'+img+'">');
      flyer.fly({
        start: {
          left: event.pageX,
          top: event.pageY-$j(document).scrollTop()-10
        },
        end: {
          left: offset.left+10,
          top: offset.top-$j(document).scrollTop()+50,
          width: 0,
          height: 0
        },
        onEnd: function(){
          // $j("#msg").show().animate({width: '250px'}, 200).fadeOut(1000);
          this.destory();
        }
      });
    });

    /*限时抢购页面点击上滑*/
    $j(document).ready(function($j){
      // var tuesday=$j('#tuesday').offset().top;
      // var wednesday=$j('#wednesday').offset().top;
      // var thursday=$j('#thursday').offset().top;
      // var friday=$j('#friday').offset().top;
      // var saturday=$j('#saturday').offset().top;
      // var sunday=$j('#sunday').offset().top;
      $j(".rush-category .category-title li").click(function(){
        var distance=parseInt($j('#'+$j(this).attr('class')).offset().top)-118;
        $j('html,body').animate({scrollTop: distance+'px'}, 1000);
      });
    });

    // $j(document).ready(function(){
    //   $j(".all-select").click(function(){
    //     if(this.checked){
    //       $j("input[name='cart-select']").attr("checked","true");
    //     }else{
    //       $j("input[name='cart-select']").attr("checked","false");
    //     }
    //   });
    // });

  })
