jQuery(document).ready(function(){
    var $customerOriginChooser = jQuery('#customer-origin-chooser');

    if(Mage.Cookies.get('show-cap-modal') === null){
        Mage.Cookies.set('show-cap-modal', true);
    }

    if ($customerOriginChooser.length > 0) {
        var hasValidated = Mage.Cookies.get('customer_origin_modal-no-selection');
        //se non è ancora stato validata, si apre la modale in automatico
        if (hasValidated !== 'false' || hasValidated === null) {
            if (Mage.Cookies.get('show-cap-modal') === 'true') {

                jQuery(".delivery-content").show();

                jQuery('.remove-shut').on('click', function(event){
                    event.preventDefault();
                    if(jQuery(this).hasClass('guest-navigation')){
                        Mage.Cookies.set('show-cap-modal', false);
                    }
                    jQuery(".delivery-content").hide();
                });
            }else{
                jQuery(".delivery-content").hide();
                jQuery('.remove-shut').on('click', function(event){
                    event.preventDefault();
                    jQuery(".delivery-content").hide();
                });
            }
        } else {
            jQuery(".delivery-content").hide();
            jQuery('.remove-shut').on('click', function(event){
                event.preventDefault();
                jQuery(".delivery-content").hide();
            });
        }
    }
});
