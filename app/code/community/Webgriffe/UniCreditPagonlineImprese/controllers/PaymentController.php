<?php


class Webgriffe_UniCreditPagonlineImprese_PaymentController
    extends Mage_Core_Controller_Front_Action
{
    protected $_paymentMethodCode = Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod::PAYMENT_METHOD_CODE;

    public function initAction()
    {
        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');

        $helper->log(
            sprintf('Called "%s" with params %s', __METHOD__, print_r($this->getRequest()->getParams(), true))
        );

        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_WebServiceHelper $webServiceHelper */
        $webServiceHelper = Mage::helper('wgupi/webServiceHelper');

        /** @var Webgriffe_UniCreditPagonlineImprese_Model_PaymentInit $paymentInit */
        $paymentInit = Mage::getModel('wgupi/paymentInit');

        try {
            $order = $helper->getLastOrder();

            $paymentInit->initialize($order);

            $clientWebService = $webServiceHelper->getGatewayClient($paymentInit->getPaymentURL(), $order->getStore());
            $requestWebService = $paymentInit->getRequestParams();

            $helper->log(
                "PaymentInit RequestParams : \n%s\n",
                $requestWebService
            );

            $responseWebService = $clientWebService->Init(
                array('request' => $requestWebService)
            );

            $helper->log(
                "WebService->Init response on Order %s : \n%s\n",
                $order->getIncrementId(),
                $responseWebService->response
            );

            if ($responseWebService->response->error) {
                $helper->log('Last XML request: '.$clientWebService->getLastRequest());
                $helper->log('Last XML response: '.$clientWebService->getLastResponse());

                throw new LogicException("Webservice error.");
            }

            $helper->createParentTransaction($responseWebService->response->paymentID, $requestWebService);

            $helper->log("Generated redirect URL :\n%s\n", $responseWebService->response->redirectURL);

            return $this->_redirectUrl($responseWebService->response->redirectURL);
        } catch (Exception $e) {
            $helper->log("Exception: %s\n%s\n", $e->getMessage(), $e->getTraceAsString());
            Mage::helper('checkout')->getCheckout()->addError($e->getMessage());
        }
        return $this->_redirect('checkout/cart');
    }

    public function notifyAction()
    {
        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');

        $helper->log(
            sprintf('Called "%s" with params %s', __METHOD__, print_r($this->getRequest()->getParams(), true))
        );

        try {
            $helper->verifyRemotePayment();
            $this->_redirect($helper->responseURL);
        } catch (Exception $e) {
            $helper->log("Exception: %s\n%s\n", $e->getMessage(), $e->getTraceAsString());
            Mage::throwException($e->getMessage());
        }
    }

    public function errorAction()
    {
        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');

        $helper->log(
            sprintf('Called "%s" with params %s', __METHOD__, print_r($this->getRequest()->getParams(), true))
        );

        $this->_redirect(Webgriffe_UniCreditPagonlineImprese_Helper_Data::FAIL_URL);
    }
}
