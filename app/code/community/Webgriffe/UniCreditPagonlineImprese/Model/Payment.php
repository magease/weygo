<?php


abstract class Webgriffe_UniCreditPagonlineImprese_Model_Payment
{
    protected $_paymentMethodCode = Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod::PAYMENT_METHOD_CODE;

    const EUR_CURRENCY_CODE = 'EUR';
    const USD_CURRENCY_CODE = 'USD';

    /**
     * URL
     *
     * @var string
     */
    protected $_paymentURL;

    protected function _getPaymentUrl(Mage_Core_Model_Store $store)
    {
        if (Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/test_mode', $store)) {
            return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/payment_test_url_upi', $store);
        }

        return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/payment_production_url_upi', $store);
    }

    public function getPaymentURL()
    {
        return $this->_paymentURL;
    }

    abstract public function setSignature();

    abstract public function getRequestParams();
}