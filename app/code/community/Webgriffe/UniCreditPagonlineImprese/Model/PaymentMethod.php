<?php


class Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod
    extends Mage_Payment_Model_Method_Abstract
{
    const PAYMENT_METHOD_CODE = 'wgupi';

    protected $_code = self::PAYMENT_METHOD_CODE;

    /*
     * Force the call to the initialize() method
     * @var bool
     */
    protected $_isInitializeNeeded = true;

    /*
     * Multiaddress checkout doesn’t support redirects
     * @var bool
     */
    protected $_canUseForMultishipping = false;

    /*
     * Disable payment method in Admin because of redirection
    * @var bool
    */
    protected $_canUseInternal = false;

    public function initialize($paymentAction, $stateObject)
    {
        $this->debugData(array('method_called' => __METHOD__));

        $helper = Mage::helper('wgupi');

        $stateObject->setState($helper->getNewOrderState());
        $stateObject->setStatus($helper->getNewOrderStatus());

        return $this;
    }

    public function getOrderPlaceRedirectUrl()
    {
        $this->debugData(array('method_called' => __METHOD__));

        $secureCheckout = (bool) $this->getConfigData('secure_checkout');
        $this->debugData(array('use_secure_urls' => $secureCheckout));

        $params = array();
        if ($secureCheckout) {
            $params['_forced_secure'] = true;
        }

        return Mage::getUrl('unicreditpagonlineimprese/payment/init', $params);
    }
}