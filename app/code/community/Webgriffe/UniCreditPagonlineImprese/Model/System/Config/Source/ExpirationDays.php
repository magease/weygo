<?php


class Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_ExpirationDays
{
    public function toOptionArray()
    {
        $helper = Mage::helper('adminhtml');
        return array(
            array('label' => '1', 'value' => '1'),
            array('label' => '2', 'value' => '2'),
            array('label' => '3', 'value' => '3'),
        );
    }
}