<?php


class Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_Language
{
    public function toOptionArray()
    {
        $helper = Mage::helper('adminhtml');
        return array(
            array('label' => $helper->__('Italian'), 'value' => 'IT'),
            array('label' => $helper->__('English'), 'value' => 'EN'),
        );
    }
}