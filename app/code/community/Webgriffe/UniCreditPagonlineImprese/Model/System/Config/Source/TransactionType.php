<?php


class Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_TransactionType
{
    const PURCHASE  = 'PURCHASE';
    const AUTHORIZE = 'AUTH';

    public function toOptionArray()
    {
        return array(
            array('label' => Mage::helper('adminhtml')->__('-- Please Select --'), 'value' => ''),
            array('label' => Mage::helper('adminhtml')->__('Sale (PURCHASE)'), 'value' => self::PURCHASE),
            array('label' => Mage::helper('adminhtml')->__('Authorization only (AUTH)'), 'value' => self::AUTHORIZE),
        );
    }
}