<?php


class Webgriffe_UniCreditPagonlineImprese_Model_PaymentInit
    extends Webgriffe_UniCreditPagonlineImprese_Model_Payment
{
    /**
     * Codice terminale dell’esercente
     *
     * @var string
     */
    protected $_tid = null;

    /**
     * Chiave per firmare il messaggio
     *
     * @var string
     */
    protected $_kSig = null;

    /**
     * Chiave esterna identificante il pagamento
     *
     * @var string
     */
    protected $_shopID = null;

    /**
     * Identificativo cliente (es:email)
     *
     * @var string
     */
    protected $_shopUserRef = null;

    /**
     * Cognome e Nome del cliente separati da virgola
     *
     * @var string
     */
    protected $_shopUserName = null;

    /**
     * Account cliente del portale merchant
     *
     * @var string
     */
    protected $_shopUserAccount = null;

    /**
     *
     * trType - Tipo di Transazione
     *
     * AUTH - Autorizzazione
     * Autorizzazione di una transazione con impegno dell' importo senza
     * movimentazione contabile.
     *
     *
     * PURCHASE - Movimentazione
     * Autorizzazione con movimentazione contabile.
     *
     *
     * VERIFY - Verifica
     * Verifica dati della richiesta.
     *
     *
     * CONFIRM - Conferma
     * Movimentazione di una transazione precedentemente autorizzata
     *
     *
     * CREDIT - Credito
     * Storno di una transazione movimentata
     *
     *
     * VOID - Storno
     * Storno di una transazione autorizzata
     *
     *
     * @var string
     *
     */
    protected $_trType;

    /**
     * Importo in virgola virtuale (es. 100 = 1,00 EUR)
     *
     * @var int
     */
    protected $_amount = null;

    /**
     * Valuta
     * [EUR, USD]
     *
     * @var string
     */
    protected $_currencyCode = null;

    /**
     * Codice iso 639-2 relativo inserimento dei dati di pagamento
     * [IT, EN]
     *
     * @var string
     */
    protected $_langID = null;

    /**
     * URL relativo alla pagina di notifica esito
     *
     * @var string
     */
    protected $_notifyURL = null;

    /**
     * URL relativo alla pagina di errore
     *
     * @var string
     */
    protected $_errorURL = null;

    /**
     * Campo a disposizione dell'esercente
     *
     * @var string
     */
    protected $_addInfo1 = null;

    /**
     * Campo a disposizione dell'esercente
     *
     * @var string
     */
    protected $_addInfo2 = null;

    /**
     * Campo a disposizione dell'esercente
     *
     * @var string
     */
    protected $_addInfo3 = null;

    /**
     * Campo a disposizione dell'esercente
     *
     * @var string
     */
    protected $_addInfo4 = null;

    /**
     * Campo a disposizione dell'esercente
     *
     * @var string
     */
    protected $_addInfo5 = null;

    /**
     * Causale di pagamento
     *
     * @var string
     */
    protected $_Description = null;

    /**
     * Pagamento ricorrente
     *
     * @var bool
     */
    protected $_Recurrent = false;

    /**
     * Testo libero
     *
     * @var null
     */
    protected $_FreeText = null;

    /**
     * @var bool
     */
    protected $_initialized = false;

    protected $_signature;

    protected $description;

    public function initialize(Mage_Sales_Model_Order $order)
    {
        $store = $order->getStore();
        /** @var Mage_Payment_Model_Method_Abstract $paymentMethod */
        $paymentMethod = $order->getPayment()->getMethodInstance();

        $secureCheckout = (bool) $paymentMethod->getConfigData('secure_checkout', $store);
        $params = array();
        if ($secureCheckout) {
            $params['_forced_secure'] = true;
        }

        $notifyUrl = $store->getUrl('unicreditpagonlineimprese/payment/notify', $params);
        $errorUrl = $store->getUrl('unicreditpagonlineimprese/payment/error', $params);

        $this->_paymentURL   = $this->_getPaymentUrl($store);
        $this->_tid          = Mage::helper('wgupi')->getTerminalId($store);
        $this->_kSig         = Mage::helper('wgupi')->getApiKey($store);
        $this->_shopID       = Mage::helper('wgupi')->getShopId($order);
        $this->_shopUserRef  = $order->getCustomerEmail();
        $this->_shopUserName = "name"; //$order->getCustomerName();
        /*
        $this->_shopUserAccount = "";
        */
        $this->_trType = $paymentMethod->getConfigData('txntype', $store);

        $this->initCurrencyAndAmount($order);

        $this->_langID       = $paymentMethod->getConfigData('language', $store);
        $this->_notifyURL    = $notifyUrl;
        $this->_errorURL     = $errorUrl;
        /*
        $this->_addInfo1        = "";
        $this->_addInfo2        = "";
        $this->_addInfo3        = "";
        $this->_addInfo4        = "";
        $this->_addInfo5        = "";
        $this->_Description     = "";
        $this->_Recurrent       = "";
        $this->_FreeText        = "";
        */
        $this->description = $paymentMethod->getConfigData('description', $store);

        $this->_initialized = true;
    }

    protected function initCurrencyAndAmount(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Directory_Model_Currency $currency */
        $orderBaseCurrency = $order->getBaseCurrency();

        $amount = $order->getBaseGrandTotal();
        $currencyCode = $order->getBaseCurrencyCode();
        if (!in_array($currencyCode, array(self::EUR_CURRENCY_CODE, self::USD_CURRENCY_CODE))) {
            $amount = $orderBaseCurrency->convert($amount, self::EUR_CURRENCY_CODE);
            $currencyCode = self::EUR_CURRENCY_CODE;
        }

        $this->_amount = 100 * $order->getStore()->roundPrice($amount);
        $this->_currencyCode = $currencyCode;
    }

    protected function convertChargeTotal(Mage_Sales_Model_Order $order, Mage_Core_Model_Store $store)
    {
        $currency        = Mage::getSingleton('directory/currency')->load($order->getBaseCurrencyCode());
        $convertedAmount = $currency->convert($order->getBaseGrandTotal(), self::EUR_CURRENCY_CODE);
        $chargeTotal     = $store->roundPrice($convertedAmount);

        return $chargeTotal;
    }

    public function setSignature()
    {
        $data = $this->_tid;
        $data .= $this->_shopID;
        $data .= $this->_shopUserRef;
        $data .= $this->_shopUserName;
        $data .= $this->_shopUserAccount;
        $data .= $this->_trType;
        $data .= $this->_amount;
        $data .= $this->_currencyCode;
        $data .= $this->_langID;
        $data .= $this->_notifyURL;
        $data .= $this->_errorURL;
        $data .= $this->_addInfo1;
        $data .= $this->_addInfo2;
        $data .= $this->_addInfo3;
        $data .= $this->_addInfo4;
        $data .= $this->_addInfo5;

        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');
        $helper->log('Signature calculated on string: "'.$data.'"');

        $this->_signature = base64_encode(hash_hmac('sha256', $data, $this->_kSig, true));
    }

    public function getRequestParams()
    {
        if ($this->_signature == null) {
            $this->setSignature();
        }

        return array(
            'signature'       => $this->_signature,
            'tid'             => $this->_tid,
            'shopID'          => $this->_shopID,
            'shopUserRef'     => $this->_shopUserRef,
            'shopUserName'    => $this->_shopUserName,
            'shopUserAccount' => $this->_shopUserAccount,
            'trType'          => $this->_trType,
            'amount'          => (string)$this->_amount,
            'currencyCode'    => $this->_currencyCode,
            'langID'          => $this->_langID,
            'notifyURL'       => $this->_notifyURL,
            'errorURL'        => $this->_errorURL,
            'addInfo1'        => $this->_addInfo1,
            'addInfo2'        => $this->_addInfo2,
            'addInfo3'        => $this->_addInfo3,
            'addInfo4'        => $this->_addInfo4,
            'addInfo5'        => $this->_addInfo5,
            'Description'     => $this->_Description,
            'Recurrent'       => $this->_Recurrent,
            'FreeText'        => $this->_FreeText,
        );
    }

    public function setHtml($url)
    {
        if (!$this->_initialized) {
            throw new LogicException('Cannot generate the HTML due to uninitialized state.');
        }

        $msg = Mage::helper('wgupi')->__("Click <a href='%s' id='redirect'>here</a> if you are not redirected within 5 seconds...", $url);

        return <<<HTML
        <html>
            <head>
                <title>UniCredit PagOnline Imprese</title>
                <script type="text/javascript">
                    function redirect() {
                        document.getElementById('redirect').click();
                    }
                </script>
            </head>
            <body onload="javascript:setTimeout('redirect()', 5000);">
                <p style="text-align: center">{$this->description}</p>
                <p style="text-align: center">{$msg}</p>
            </body>
        </html>
HTML;
    }
}