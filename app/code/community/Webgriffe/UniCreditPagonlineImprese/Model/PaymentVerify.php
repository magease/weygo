<?php


class Webgriffe_UniCreditPagonlineImprese_Model_PaymentVerify
    extends Webgriffe_UniCreditPagonlineImprese_Model_Payment
{
    /**
     * Codice terminale dell'esercente
     *
     * @var string
     */
    protected $_tid = null;

    /**
     * Chiave per firmare il messaggio
     *
     * @var string
     */
    protected $_kSig = null;

    /**
     * Chiave esterna identificante il pagamento
     *
     * @var string
     */
    protected $_shopID = null;


    /**
     * Codice paymentID associato alla richiesta
     *
     * @var string
     */
    protected $_paymentID = null;

    /**
     * @var bool
     */
    protected $_initialized = false;

    protected $_signature;

    public function initialize(Mage_Sales_Model_Order $order, $paymentID)
    {
        $store = $order->getStore();

        $this->_paymentURL = $this->_getPaymentUrl($store);
        $this->_tid = Mage::helper('wgupi')->getTerminalId($store);
        $this->_kSig = Mage::helper('wgupi')->getApiKey($store);
        $this->_shopID = Mage::helper('wgupi')->getShopId($order);
        $this->_paymentID = $paymentID;

        $this->_initialized = true;
    }

    public function setSignature()
    {
        $data = $this->_tid;
        $data .= $this->_shopID;
        $data .= $this->_paymentID;

        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');
        $helper->log('Signature calculated on string: "'.$data.'"');

        $this->_signature = base64_encode(hash_hmac('sha256', $data, $this->_kSig, true));
    }

    public function getRequestParams()
    {
        if ($this->_signature == null) {
            $this->setSignature();
        }

        return array(
            'signature' => $this->_signature,
            'tid'       => $this->_tid,
            'shopID'    => $this->_shopID,
            'paymentID' => $this->_paymentID,
        );
    }
}