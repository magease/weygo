<?php
class Webgriffe_UniCreditPagonlineImprese_Model_Resource_Order_Payment_Transaction_Collection extends Mage_Sales_Model_Resource_Order_Payment_Transaction_Collection
{
    protected $_methodCode  = null;
    protected $_isClosed    = null;
    protected $_isRoot      = false;

    public function addPaymentMethodFilter($methodCode)
    {
        $this->_methodCode = $methodCode;
        return $this->addPaymentInformation(array('method'));
    }

    public function addIsClosedFilter($isClosed)
    {
        $this->_isClosed = $isClosed;
        return $this;
    }

    public function addIsRootFilter()
    {
        $this->_isRoot = true;
        return $this;
    }

    protected function _beforeLoad()
    {
        parent::_beforeLoad();

        if ($this->isLoaded()) {
            return $this;
        }

        // filters
        if ($this->_methodCode) {
            $this->getSelect()->where('sop.method = ?', $this->_methodCode);
        }

        if (!is_null($this->_isClosed)) {
            $this->getSelect()->where('main_table.is_closed = ?', $this->_isClosed);
        }

        if ($this->_isRoot) {
            $this->getSelect()->where('main_table.parent_txn_id IS NULL');
        }

        return $this;
    }

}