<?php
class Webgriffe_UniCreditPagonlineImprese_Model_Observer
{
    public function verifyPayment()
    {
        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $helper */
        $helper = Mage::helper('wgupi');
        $helper->log("Called Webgriffe_UniCreditPagonlineImprese_Model_Observer::verifyPayment()");

        // Recupera tutte le transazioni open associate a pagamento wgupi degli ultimi N giorni
        $now = Varien_Date::toTimestamp(true);
        $expiredays = $helper->getTransactionExpireDays();
        $from = date("Y-m-d G:i:s", $now - $expiredays * 86400);
        $to = date("Y-m-d G:i:s", $now);

        $transactions = Mage::getResourceModel('wgupi/order_payment_transaction_collection')
            ->addPaymentMethodFilter('wgupi')
            ->addIsClosedFilter(0)
            ->addIsRootFilter()
            ->addFieldToFilter(
                'created_at',
                array(
                    'from'  => $from,
                    'to'    => $to,
                )
            )
            ->setOrder('created_at', Varien_Data_Collection::SORT_ORDER_ASC)
            ->setOrder('transaction_id', Varien_Data_Collection::SORT_ORDER_ASC)
        ;

        $helper->log("Found %d transactions/payments to verify", count($transactions));

        /** @var Mage_Sales_Model_Order_Payment_Transaction $txn */
        foreach ($transactions as $txn)
        {
            try {
                $helper->verifyRemotePayment($txn->getOrder(), $txn->getTxnId());
            } catch (Exception $e) {
                $helper->log("Error: %s", $e->getMessage());
            }
        }
    }
}