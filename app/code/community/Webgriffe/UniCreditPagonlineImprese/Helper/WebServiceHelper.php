<?php


class Webgriffe_UniCreditPagonlineImprese_Helper_WebServiceHelper extends Mage_Core_Helper_Abstract
{
    protected $_paymentMethodCode = Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod::PAYMENT_METHOD_CODE;

    const PAYMENT_METHOD_NAME = 'UniCredit PagOnline Imprese';

    /**
     * @param $url
     * @param $storeId
     * @return Zend_Soap_Client
     */
    public function getGatewayClient($url, $storeId)
    {
        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_Data $_helper */
        $_helper = Mage::helper('wgupi');

        if (!extension_loaded('soap')) {
            $_helper->log(
                "Non è stato possibile creare il client per il webserver." .
                "\nL\'estensione PHP_SOAP è necessaria per il funzionamento del modulo"
            );

            throw new RuntimeException('PHP SOAP extension is required.');
        }

        $options = array(
            'compression' => SOAP_COMPRESSION_ACCEPT,
            'soap_version' => SOAP_1_1,
        );

        if ($_helper->isTestMode($storeId)) {
            //Unicredit usa un certificato self-signed in ambiente di test
            $contextOptions = array(
                'ssl' => array(
                    'allow_self_signed' => true,
                )
            );
            $sslContext = stream_context_create($contextOptions);
            $options['stream_context'] = $sslContext;
        }

        $client = new Zend_Soap_Client($url, $options);

        return $client;
    }
}
