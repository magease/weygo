<?php
class Webgriffe_UniCreditPagonlineImprese_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_paymentMethodCode = Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod::PAYMENT_METHOD_CODE;

    const ORDER_NEW_STATUS_CONFIG_NAME = 'order_new_status';
    const ORDER_FAULT_STATUS_CONFIG_NAME = 'order_fault_status';
    const ORDER_SUCCESS_STATUS_CONFIG_NAME = 'order_success_status';
    const SUCCESS_URL = 'checkout/onepage/success';
    const FAIL_URL = 'checkout/onepage/failure';
    const TRANSACTION_IN_PROGRESS_RETURN_CODE = 'IGFS_814';

    /**
     * @var Mage_Sales_Model_Order
     */
    private $_lastOrder;

    public $responseURL;

    public function getLastOrder()
    {
        if ($this->_lastOrder == null) {
            $checkoutSession = Mage::helper('checkout')->getCheckout();
            $incrementId     = $checkoutSession->getLastRealOrderId();

            if (empty($incrementId)) {
                throw new LogicException(
                    'Cannot get last order because there isn\'t any last real order ID in session.'
                );
            }
            $this->_lastOrder = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
        }

        return $this->_lastOrder;
    }

    public function createParentTransaction($paymentID, $additionalInfo)
    {
        $payment = $this->_lastOrder->getPayment();
        if ($payment->getId()) {
            try {
                switch ($additionalInfo['trType'])
                {
                    case Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_TransactionType::PURCHASE:
                        $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE;
                        break;
                    case Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_TransactionType::AUTHORIZE:
                        $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;
                        break;
                    default:
                        $transactionType = Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER;
                        break;
                }

                Mage::getModel('sales/order_payment_transaction')
                    ->setOrderId($this->_lastOrder->getId())
                    ->setPaymentId($payment->getId())
                    ->setTxnId($paymentID)
                    ->setOrderPaymentObject($payment)
                    ->setAdditionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $additionalInfo)
                    ->setIsClosed(false)
                    ->setTxnType($transactionType)
                    ->save();

                $this->log(
                    'Created Transaction with PaymentID '.$paymentID.' for Order '.$this->_lastOrder->getIncrementId()
                );
            } catch (Exception $e) {
                $this->log('Couldn\'t create Transaction for Order ' . $this->_lastOrder->getIncrementId());
                $this->log($e->getMessage());
            }
        }
    }

    protected function _getPaymentID()
    {
        $payment = $this->_lastOrder->getPayment();
        if ($payment->getId()) {
            try {
                $transaction = Mage::getModel('sales/order_payment_transaction')->getCollection()
                    ->setOrderFilter($this->_lastOrder)
                    ->addPaymentIdFilter($payment->getId())
                    ->setOrder('created_at', Varien_Data_Collection::SORT_ORDER_DESC)
                    ->setOrder('transaction_id', Varien_Data_Collection::SORT_ORDER_DESC)
                    ->getLastItem();

                return $transaction->getTxnId();
            } catch (Exception $e) {
                $this->log('Couldn\'t retrieve Transaction for Order ' . $this->_lastOrder->getIncrementId());
                $this->log($e->getMessage());
            }
        }

        return false;
    }

    public function updateOrderStatus($response, $order = null)
    {
        if (!is_null($order)) {
            $this->_lastOrder = $order;
        }

        $targetState = $this->getFaultOrderState();
        $targetStatus = $this->getFaultOrderStatus();
        $this->responseURL = self::FAIL_URL;

        if (!$response->error) {
            $targetState = $this->getSuccessOrderState();
            $targetStatus = $this->getSuccessOrderStatus();
            $this->responseURL = self::SUCCESS_URL;

            if (!$this->_lastOrder->getEmailSent()) {
                $this->_lastOrder->sendNewOrderEmail();
            }
        }

        $this->log("targetState and targetStatus will be '%s' and '%s'", $targetState, $targetStatus);

        if ($this->_lastOrder->getState() == $targetState && $this->_lastOrder->getStatus() == $targetStatus) {
            $this->log(
                "Order actual state and status ('%s' and '%s') are equals to target state and status ('%s' and '%s')",
                $this->_lastOrder->getState(),
                $this->_lastOrder->getStatus(),
                $targetState,
                $targetStatus
            );

            return;
        }

        if ($this->_lastOrder->canUnhold()) {
            $this->log("Unholding order '%s'", $this->_lastOrder->getIncrementId());
            $this->_lastOrder->unhold();
        }

        switch ($targetState) {
            case Mage_Sales_Model_Order::STATE_HOLDED:
                if ($this->_lastOrder->canHold()) {
                    $this->log("Holding order '%s'", $this->_lastOrder->getIncrementId());
                    $this->_lastOrder->hold();
                } else {
                    $this->log(
                        "Failed to Holding order '%s', order state '%s'.",
                        $this->_lastOrder->getIncrementId(),
                        $this->_lastOrder->getState()
                    );
                    throw new Exception("You are trying to HOLD an order not Holdable");
                }

                break;
            case Mage_Sales_Model_Order::STATE_CANCELED:
                if ($this->_lastOrder->canCancel()) {
                    $this->log("Canceling order '%s'", $this->_lastOrder->getIncrementId());
                    $this->_lastOrder->cancel();
                } else {
                    $this->log(
                        "Failed to Canceling order '%s', order state '%s'.",
                        $this->_lastOrder->getIncrementId(),
                        $this->_lastOrder->getState()
                    );
                    throw new Exception("You are trying to CANCEL an order not Cancelable");
                }

                break;
            default:
                $this->log(
                    "Setting order state and status to '%s' and '%s'",
                    $targetState,
                    $targetStatus
                );
                $this->_lastOrder->setState($targetState, $targetStatus, 'Updated due to Unicredit transaction');
                break;
        }

        $this->_lastOrder->save();
    }

    public function createChildTransaction($response)
    {
        $this->log("createChildTransaction() called with following response: %s", print_r($response, 1));

        $txnId = $response->paymentID;

        /** @var Mage_Sales_Model_Order_Payment_Transaction  $parentTxn */
        $parentTxn = Mage::getResourceModel('wgupi/order_payment_transaction_collection')
            ->addPaymentMethodFilter('wgupi')
            ->addIsRootFilter()
            ->addFieldToFilter('txn_id', $txnId)
            ->setOrder('created_at', Varien_Data_Collection::SORT_ORDER_DESC)
            ->setOrder('transaction_id', Varien_Data_Collection::SORT_ORDER_DESC)
            ->getLastItem();

        if (!$parentTxn->getId()) {
            $this->log("Couldn't retrieve transaction by txn_id '%s'", $txnId);
            return;
        }

        if ($parentTxn->getIsClosed()) {
            $this->log("Parent transaction '%s' already closed, no child transaction create", $txnId);
            return;
        }

        $order = $parentTxn->getOrder();
        $payment = $order->getPayment();

        /** @var Mage_Sales_Model_Order_Payment_Transaction  $child */
        $childTxn = Mage::getModel('sales/order_payment_transaction');

        $numChildren = count($parentTxn->getChildTransactions()) + 1;
        $childTxnIdSuffix = str_pad($numChildren, 3, '0', STR_PAD_LEFT);

        $childTxn
            ->setOrderId($order->getId())
            ->setPaymentId($payment->getId())
            ->setTxnId($parentTxn->getTxnId().'-'.$childTxnIdSuffix)
            ->setOrderPaymentObject($payment)
            ->setAdditionalInformation(
                Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,
                json_decode(json_encode($response), true)  // convert stdClass to array
            )->setIsClosed($response->error ? false : true)
            ->setTxnType($parentTxn->getTxnType())
            ->setParentTxnId($parentTxn->getTxnId())
            ->save();

        // Se l'esito è positivo, chiudo la transazione padre
        if (!$response->error) {
            $parentTxn->setIsClosed(true);
            $parentTxn->getResource()->saveAttribute($parentTxn, 'is_closed');
        }

    }

    public function getNewOrderState()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_NEW_STATUS_CONFIG_NAME)
        );

        return $state;
    }

    public function getNewOrderStatus()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_NEW_STATUS_CONFIG_NAME)
        );

        return $status;
    }

    public function getFaultOrderState()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_FAULT_STATUS_CONFIG_NAME)
        );

        return $state;
    }

    public function getFaultOrderStatus()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_FAULT_STATUS_CONFIG_NAME)
        );

        return $status;
    }

    public function getSuccessOrderState()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_SUCCESS_STATUS_CONFIG_NAME)
        );

        return $state;
    }

    public function getSuccessOrderStatus()
    {
        list($state, $status) = $this->getStateAndStatusArray(
            $this->generateConfigPathForValue(self::ORDER_SUCCESS_STATUS_CONFIG_NAME)
        );

        return $status;
    }

    private function generateConfigPathForValue($valueName)
    {
        return 'payment/' . $this->_paymentMethodCode . '/' . $valueName;
    }

    private function getStateAndStatusArray($configPath)
    {
        return explode(
            Webgriffe_UniCreditPagonlineImprese_Model_System_Config_Source_Order_Status::STATE_STATUS_SEPARATOR,
            Mage::getStoreConfig($configPath)
        );
    }

    public function isTestMode($storeId)
    {
        return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/test_mode', $storeId);
    }

    public function getTransactionExpireDays($storeId = null)
    {
        return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/tnxexpiredays', $storeId);
    }

    public function getTerminalId($storeId)
    {
        if ($this->isTestMode($storeId)) {
            return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/test_terminal_id', $storeId);
        } else {
            return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/terminal_id', $storeId);
        }
    }

    public function getApiKey($storeId)
    {
        if ($this->isTestMode($storeId)) {
            return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/test_api_key', $storeId);
        } else {
            return Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/api_key', $storeId);
        }
    }

    public function getShopId(Mage_Sales_Model_Order $order)
    {
        $shopId = $order->getIncrementId();
        if ($this->isTestMode($order->getStoreId())) {
            $shopId = sprintf(
                "%s-%s-%s",
                strtoupper($this->_paymentMethodCode),
                $shopId,
                date("YmdGis", Varien_Date::toTimestamp($order->getCreatedAt()))
            );
        }
        return $shopId;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param string $paymentId
     */
    public function verifyRemotePayment(Mage_Sales_Model_Order $order = null, $paymentId = null)
    {
        if (is_null($order)) {
            $order = $this->getLastOrder();
        }
        $this->_lastOrder = $order;

        if (is_null($paymentId)) {
            $paymentId = $this->_getPaymentID();
        }

        /** @var Webgriffe_UniCreditPagonlineImprese_Helper_WebServiceHelper $webServiceHelper */
        $webServiceHelper = Mage::helper('wgupi/webServiceHelper');

        /** @var Webgriffe_UniCreditPagonlineImprese_Model_PaymentVerify $paymentVerify */
        $paymentVerify = Mage::getModel('wgupi/paymentVerify');
        $paymentVerify->initialize($order, $paymentId);

        $clientWebService = $webServiceHelper->getGatewayClient($paymentVerify->getPaymentURL(), $order->getStore());
        $requestWebService = $paymentVerify->getRequestParams();

        $this->log(
            "\nWebService->Verify request parameters on Order %s : \n%s\n",
            $order->getIncrementId(),
            print_r($requestWebService, 1)
        );

        $responseWebService = $clientWebService->Verify(
            array('request' => $requestWebService)
        );

        $this->log(
            "\nWebService->Verify response on Order %s : \n%s\n",
            $order->getIncrementId(),
            $responseWebService->response
        );

        $this->createChildTransaction($responseWebService->response);

        if ($this->isTransactionStillInProgress($responseWebService)) {
            $this->log('Nothing to do, transaction is still progress for order "%s".');
            return;
        }

        $this->log(
            "Before calling updateOrderStatus() for Order '%s' with state '%s' and status '%s'.",
            $order->getIncrementId(),
            $order->getState(),
            $order->getStatus()
        );

        $this->updateOrderStatus($responseWebService->response, $order);

        $this->log(
            "Updated order '%s' with state '%s' and status '%s'.",
            $order->getIncrementId(),
            $order->getState(),
            $order->getStatus()
        );
    }

    public function log()
    {
        $_args = func_get_args();

        foreach ($_args as $arg) {
            $args[] = print_r($arg, true);
        }

        $formattedMsg = call_user_func_array('sprintf', $args);
        Mage::log(
            $formattedMsg,
            null,
            'payment_' . Webgriffe_UniCreditPagonlineImprese_Model_PaymentMethod::PAYMENT_METHOD_CODE . '.log',
            Mage::getStoreConfig('payment/' . $this->_paymentMethodCode . '/debug')
        );
    }

    /**
     * @param $responseWebService
     * @return bool
     */
    private function isTransactionStillInProgress($responseWebService)
    {
        return $responseWebService->response->rc == self::TRANSACTION_IN_PROGRESS_RETURN_CODE;
    }
}
