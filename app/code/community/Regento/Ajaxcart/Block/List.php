<?php

/**
 * Class Regento_Ajaxcart_Block_List
 */
class Regento_Ajaxcart_Block_List extends Mage_Core_Block_Abstract
{
    protected $_blocks = array();

    public function addBlock($name, $html_reference)
    {
        $this->_blocks[$name] = array(
            'name'=>$name,
            'html_reference'=>$html_reference
        );

    }

    public function removeBlock($name)
    {
        if(array_key_exists($name,$this->_blocks)){
            unset($this->_blocks[$name]);
        }
    }

    public function getBlocks()
    {
        return $this->_blocks;
    }
}