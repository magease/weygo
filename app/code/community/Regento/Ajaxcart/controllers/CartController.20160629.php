<?php
/**
 * Regento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Please do not edit or add to this file if you wish to upgrade
 * Magento or this extension to newer versions in the future.
 ** Regento *give their best to conform to
 * "non-obtrusive, best Magento practices" style of coding.
 * However,* Regento *guarantee functional accuracy of
 * specific extension behavior. Additionally we take no responsibility
 * for any possible issue(s) resulting from extension usage.
 * We reserve the full right not to provide any kind of support for our free extensions.
 * Thank you for your understanding.
 *
 * @category Regento
 * @package Regento_Ajaxcart
 * @file CartController.php
 * @author Valerio Masciotta <sviluppo@valeriomasciotta.it>
 * @copyright Copyright (c) Regento (http://www.regento.it/)
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */
require_once 'Mage/Checkout/controllers/CartController.php';

/**
 * Class Regento_Ajaxcart_CartController
 * Override original Mage_Checkout_CartController
 */
class Regento_Ajaxcart_CartController extends Mage_Checkout_CartController
{

    /**
     * Override original addAction adding ajax functionality
     * @return Mage_Core_Controller_Varien_Action
     * @see Mage_Checkout_CartController::addAction()
     * @throws Exception
     */
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $cart->setIsAjax(false);
        if ($params['isAjax'] == 1) {
           $cart->setIsAjax(true);
           $response = array();
           try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Unable to find Product ID');
                }

                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();

                $this->_getSession()->setCartWasUpdated(true);

                /**
                 * @todo remove wishlist observer processAddToCart
                 */
                Mage::dispatchEvent(
                    'checkout_cart_add_product_complete',
                    array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                    );

                if (!$this->_getSession()->getNoCartRedirect(true)) {
                    if (!$cart->getQuote()->getHasError()) {
                        $message = $this->__(
                            '%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName())
                            );
                        $response['status'] = 'SUCCESS';
                        $response['message'] = $message;

                        $this->loadLayout();
                        $cartSide = $this->getLayout()->getBlock('footer')->getChild('minicart_head')->getChild('minicart_content')->toHtml();
                        $response['sidebar'] = $cartSide;
                        $cartHead = $this->getLayout()->getBlock('minicart_head')->toHtml();
                        $response['head'] = $cartHead;
                        $response['qty'] = $this->_getCart()->getSummaryQty();
                    }
                }
            } catch (Mage_Core_Exception $e) {
                $msg = '';
                if ($this->_getSession()->getUseNotice(true)) {
                    $msg = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $msg .= $message . '<br/>';
                    }
                }

                $response['status'] = 'ERROR';
                $response['message'] = $msg;
                $_customerOriginCap = Mage::getSingleton('checkout/session')->getCustomerOriginCap();
                if ($_customerOriginCap == null) {
                    $response['overstock'] = false;
                } else {
                    $response['overstock'] = true;
                }
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Cannot add the item to shopping cart because '. $e->getMessage());
                Mage::logException($e);
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        } else {
            return parent::addAction();
        }
    }
}