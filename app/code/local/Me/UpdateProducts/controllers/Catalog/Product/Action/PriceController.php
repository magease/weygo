<?php

class Me_UpdateProducts_Catalog_Product_Action_PriceController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->_title($this->__("Backend Page Title"));
        $this->renderLayout();
    }

    public function updateAction() {
        $this->loadLayout();
        $this->_title($this->__("Update Products Price"));
        $this->renderLayout();
    }

    /**
     * Update product attributes
     */
    public function saveAction() {

        if (!$this->_validateProducts()) {
            return;
        }

        /* Collect Data */
        $attributesData = $this->getRequest()->getParam('updateprice', array());

        /* Prepare inventory data item options (use config settings) */

        try {
            if ($attributesData) {
                $dateFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
                $storeId = $this->_getHelper()->getSelectedStoreId();

                foreach ($attributesData as $attributeCode => $value) {
                    $attribute = Mage::getSingleton('eav/config')
                            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
                    if (!$attribute->getAttributeId()) {
                        unset($attributesData[$attributeCode]);
                        continue;
                    }
                }

                Mage::getSingleton('updateproducts/product_update')
                        ->updatePrice($this->_getHelper()->getUpdateProductIds(), $attributesData, $storeId);
            }

            $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were updated', count($this->_getHelper()->getUpdateProductIds()))
            );
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('An error occurred while updating the product(s) attributes.'));
        }

        $this->_redirect('admin_updateproducts/adminhtml_updateproductsbackend', array('store' => $this->_getHelper()->getSelectedStoreId()));
    }

    /**
     * Validate selection of products for massupdate
     *
     * @return boolean
     */
    protected function _validateProducts() {
        $error = false;
        $productIds = $this->_getHelper()->getUpdateProductIds();
        if (!is_array($productIds)) {
            $error = $this->__('Please select products for attributes update');
        } else if (!Mage::getModel('catalog/product')->isProductsHasSku($productIds)) {
            $error = $this->__('Some of the processed products have no SKU value defined. Please fill it prior to performing operations on these products.');
        }

        if ($error) {
            $this->_getSession()->addError($error);
            $this->_redirect('*/catalog_product_action_price/', array('_current' => true));
        }

        return !$error;
    }

    /**
     * Rertive data manipulation helper
     *
     * @return Mage_Adminhtml_Helper_Catalog_Product_Edit_Action_Attribute
     */
    protected function _getHelper() {
        return Mage::helper('updateproducts/catalog_product_update_action_price');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/update_attributes');
    }

    /**
     * Attributes validation action
     *
     */
    public function validateAction() {
        $response = new Varien_Object();
        $response->setError(false);
        $attributesData = $this->getRequest()->getParam('attributes', array());
        $data = new Varien_Object();

        try {
            if ($attributesData) {
                $dateFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
                $storeId = $this->_getHelper()->getSelectedStoreId();

                foreach ($attributesData as $attributeCode => $value) {
                    $attribute = Mage::getSingleton('eav/config')
                            ->getAttribute('catalog_product', $attributeCode);
                    if (!$attribute->getAttributeId()) {
                        unset($attributesData[$attributeCode]);
                        continue;
                    }
                    $data->setData($attributeCode, $value);
                    $attribute->getBackend()->validate($data);
                }
            }
        } catch (Mage_Eav_Model_Entity_Attribute_Exception $e) {
            $response->setError(true);
            $response->setAttribute($e->getAttributeCode());
            $response->setMessage($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $response->setError(true);
            $response->setMessage($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('An error occurred while updating the product(s) attributes.'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

}