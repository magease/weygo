<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * Catalog Product Mass Action processing model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Me_UpdateProducts_Model_Product_Update extends Mage_Core_Model_Abstract {

    /**
     * Initialize resource model
     *
     */
    protected function _construct() {
        $this->_init('updateproducts/product_update');
    }

    /**
     * Retrieve resource instance wrapper
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Action
     */
    protected function _getResource() {
        return parent::_getResource();
//        return Mage::getResourceModel('me/updateproducts');
    }

    /**
     * Update attribute values for entity list per store
     *
     * @param array $productIds
     * @param array $attrData
     * @param int $storeId
     * @return Mage_Catalog_Model_Product_Action
     */
    public function updatePrice($productIds, $attrData, $storeId) {

        Mage::dispatchEvent('catalog_product_attribute_update_before', array(
            'attributes_data' => &$attrData,
            'product_ids' => &$productIds,
            'store_id' => &$storeId
        ));
        
        $this->_getResource()->updatePrice($productIds, $attrData, $storeId);
        $this->setData(array(
            'product_ids' => array_unique($productIds),
            'attributes_data' => $attrData,
            'store_id' => $storeId
        ));

        // register mass action indexer event
        Mage::getSingleton('index/indexer')->processEntityAction(
                $this, Mage_Catalog_Model_Product::ENTITY, Mage_Index_Model_Event::TYPE_MASS_ACTION
        );

        Mage::dispatchEvent('catalog_product_attribute_update_after', array(
            'product_ids' => $productIds,
        ));

        return $this;
    }

}
