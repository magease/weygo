<?php

class Me_Defaultadd_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Retrieve customer session object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function IndexAction() {

        $customer = $this->_getSession()->getCustomer();
        /* @var $address Mage_Customer_Model_Address */
        $address = Mage::getModel('customer/address');
        $addressId = $this->getRequest()->getParam('addid');
        if ($addressId) {
            $existsAddress = $customer->getAddressById($addressId);
            if ($existsAddress->getId() && $existsAddress->getCustomerId() == $customer->getId()) {
                $address->setId($existsAddress->getId());
            }
        }

        $errors = array();

        try {
            $address->setCustomerId($customer->getId())
                    ->setIsDefaultBilling(true)
                    ->setIsDefaultShipping(true);

            if (count($errors) === 0) {
                $address->save();
                $this->_getSession()->addSuccess($this->__('The address has been saved as Default Address.'));
                $this->_redirectSuccess(Mage::getUrl('customer/address', array('_secure' => true)));
                return;
            } else {
                foreach ($errors as $errorMessage) {
                    $this->_getSession()->addError($errorMessage);
                }
            }
        } catch (Mage_Core_Exception $e) {
            $this->addException($e, $e->getMessage());
        } catch (Exception $e) {
            $this->addException($e, $this->__('Cannot save address.'));
        }

        return $this->_redirectError(Mage::getUrl('customer/address'));
    }

}