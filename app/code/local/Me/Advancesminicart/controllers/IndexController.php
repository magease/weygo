<?php

class Me_Advancesminicart_IndexController extends Mage_Core_Controller_Front_Action {

    /**

     * Retrieve shopping cart model object

     *

     * @return Mage_Checkout_Model_Cart

     */
    protected function _getCart() {

        return Mage::getSingleton('checkout/cart');
    }

    /**

     * Get checkout session model instance

     *

     * @return Mage_Checkout_Model_Session

     */
    protected function _getSession() {

        return Mage::getSingleton('checkout/session');
    }

    /**

     * Get current active quote instance

     *

     * @return Mage_Sales_Model_Quote

     */
    protected function _getQuote() {

        return $this->_getCart()->getQuote();
    }

    public function EmptyallAction() {

        $this->_emptyShoppingCart();

        ////////////////////////////
//        $this->loadLayout();
//        $this->getLayout()->getBlock("head")->setTitle($this->__("Titlename"));
//        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
//        $breadcrumbs->addCrumb("home", array(
//            "label" => $this->__("Home Page"),
//            "title" => $this->__("Home Page"),
//            "link" => Mage::getBaseUrl()
//        ));
//
//        $breadcrumbs->addCrumb("titlename", array(
//            "label" => $this->__("Titlename"),
//            "title" => $this->__("Titlename")
//        ));
//
//        $this->renderLayout();
    }

    /**

     * Empty customer's shopping cart

     */
    protected function _emptyShoppingCart() {
        $result = array();
        try {

            $this->_getCart()->truncate()->save();

            $this->_getSession()->setCartWasUpdated(true);

            $result['qty'] = $this->_getCart()->getSummaryQty();

            $this->loadLayout();

            $result['content'] = $this->getLayout()->getBlock('minicart_content')->toHtml();



            $result['success'] = 1;

            $result['message'] = $this->__('Cart was cleared successfully.');

            Mage::dispatchEvent('ajax_cart_remove_item_success', array('id' => $id));
        } catch (Mage_Core_Exception $exception) {

            $this->_getSession()->addError($exception->getMessage());

            $result['success'] = 0;

            $result['error'] = $this->__('Cannot update shopping cart.');
        } catch (Exception $exception) {

            $this->_getSession()->addException($exception, $this->__('Cannot update shopping cart.'));

            $result['success'] = 0;

            $result['error'] = $this->__('Cannot update shopping cart.');
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**

     * Minicart delete action

     */
    public function ajaxDeleteAction() {

        if (!$this->_validateFormKey()) {

            Mage::throwException('Invalid form key');
        }

        $id = (int) $this->getRequest()->getParam('id');

        $result = array();

        if ($id) {

            try {

                $this->_getCart()->removeItem($id)->save();



                $result['qty'] = $this->_getCart()->getSummaryQty();



                $this->loadLayout();

                $result['content'] = $this->getLayout()->getBlock('minicart_content')->toHtml();



                $result['success'] = 1;

                $result['message'] = $this->__('Item was removed successfully.');

                Mage::dispatchEvent('ajax_cart_remove_item_success', array('id' => $id));
            } catch (Exception $e) {

                $result['success'] = 0;

                $result['error'] = $this->__('Can not remove the item.');
            }
        }



        $this->getResponse()->setHeader('Content-type', 'application/json');

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}