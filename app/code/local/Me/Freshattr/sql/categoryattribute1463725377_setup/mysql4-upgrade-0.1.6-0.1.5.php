<?php

$installer = $this;
$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'Fresh Options');





$installer->addAttribute('catalog_category', 'fresh_regular', array(
    'type' => 'varchar',
    'label' => 'Fresh Regular',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Category products regular"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_regular', '82'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_regular');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");




$installer->addAttribute('catalog_category', 'fresh_regular_price', array(
    'type' => 'varchar',
    'label' => 'Regular Price',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Regular Price"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_regular_price', '83'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_regular_price');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");


$installer->addAttribute('catalog_category', 'fresh_special_price', array(
    'type' => 'varchar',
    'label' => 'Special Price',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Special Price"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_special_price', '84'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_special_price');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");


$installer->endSetup();

