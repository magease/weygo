<?php

$installer = $this;
$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'Fresh Options');



$installer->addAttribute('catalog_category', 'linked_confproduct', array(
    'type' => 'varchar',
    'label' => 'Configurable Alyante Product',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Configurable Alyante Product"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'linked_confproduct', '99'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_regular');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");



$installer->endSetup();

