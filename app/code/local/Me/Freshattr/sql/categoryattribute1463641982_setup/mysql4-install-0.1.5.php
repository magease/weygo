<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;
$installer->startSetup();



$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
//$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);




// Add group
$fresh_group = array(
    'name' => 'Fresh Options',
    'sort' => 100,
    'id' => null
);

$installer->addAttributeGroup($entityTypeId, $attributeSetId, $fresh_group['name'], $fresh_group['sort']);
$fresh_group['id'] = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, $fresh_group['name']);

$installer->addAttribute('catalog_category', 'fresh_level', array(
    'type' => 'int',
    'label' => 'Fresh Level',
    'input' => 'select',
    'source' => 'freshattr/eav_entity_attribute_source_categoryoptions14636419820',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'required' => false,
    'default' => 1
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $fresh_group['id'], 'fresh_level', '81'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_level');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");

$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getAttributeGroupId($entityTypeId, $attributeSetId, 'Fresh Options');



$installer->addAttribute('catalog_category', 'fresh_regular', array(
    'type' => 'varchar',
    'label' => 'Fresh Regular',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Category products regular"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_regular', '82'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_regular');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");




$installer->addAttribute('catalog_category', 'fresh_regular_price', array(
    'type' => 'varchar',
    'label' => 'Regular Price',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Regular Price"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_regular_price', '83'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_regular_price');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");


$installer->addAttribute('catalog_category', 'fresh_special_price', array(
    'type' => 'varchar',
    'label' => 'Special Price',
    'input' => 'text',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => false,
    'default' => "",
    "note" => "Special Price"
));

$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'fresh_special_price', '84'
);

//$attributeId = $installer->getAttributeId($entityTypeId, 'fresh_special_price');
//
//$installer->run("
//INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
//(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
//    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
//        FROM `{$installer->getTable('catalog_category_entity')}`;
//");


$installer->endSetup();
