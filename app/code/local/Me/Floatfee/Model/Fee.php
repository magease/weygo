<?php

class Me_Floatfee_Model_Fee extends Varien_Object {

    const FEE_AMOUNT = 10;

    public static function getFee() {
        $fee_amount = 0;
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
        foreach ($items as $item) {
            if (Mage::getModel('catalog/product')->load($item->getProductId())->getAttributeSetId() == 12) {
                $fee_amount = $fee_amount + $item->getRowTotal() * 0.2;
            }
        }
        return $fee_amount;
    }

    public static function canApply($address) {
        //put here your business logic to check if fee should be applied or not
//        if ($address->getAddressType() == 'shipping') {
        return true;
//        }
    }

}