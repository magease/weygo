<?php


class Me_Invitegroup_Block_Adminhtml_Invitegroup extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_invitegroup";
	$this->_blockGroup = "invitegroup";
	$this->_headerText = Mage::helper("invitegroup")->__("Invitegroup Manager");
	$this->_addButtonLabel = Mage::helper("invitegroup")->__("Add New Item");
	parent::__construct();
	
	}

}