<?php

class Me_Invitegroup_Block_Adminhtml_Invitegroup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId("invitegroupGrid");
		$this->setDefaultSort("invite_id");
		$this->setDefaultDir("DESC");
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel("invitegroup/invitegroup")->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	protected function _prepareColumns()
	{
		$this->addColumn("invite_id", array(
			"header" => Mage::helper("invitegroup")->__("ID"),
			"align" =>"right",
			"width" => "50px",
			"type" => "number",
			"index" => "invite_id",
			));

		$this->addColumn("invite_code", array(
			"header" => Mage::helper("invitegroup")->__("invite_code"),
			"index" => "invite_code",
			));
		$groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();
		$this->addColumn("group", array(
			"header" => Mage::helper("invitegroup")->__("group "),
			"index" => "group",
			"type" =>'options',
			"options" =>$groups,
			));
		$this->addColumn("limit", array(
			"header" => Mage::helper("invitegroup")->__("limit "),
			"index" => "limit",
			));
		$this->addColumn("current_register", array(
			"header" => Mage::helper("invitegroup")->__("Already registered"),
			"index" => "current_register",
			));
		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl("*/*/edit", array("id" => $row->getId()));
	}



	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('invite_id');
		$this->getMassactionBlock()->setFormFieldName('invite_ids');
		$this->getMassactionBlock()->setUseSelectAll(true);
		$this->getMassactionBlock()->addItem('remove_invitegroup', array(
			'label'=> Mage::helper('invitegroup')->__('Remove Invitegroup'),
			'url'  => $this->getUrl('*/adminhtml_invitegroup/massRemove'),
			'confirm' => Mage::helper('invitegroup')->__('Are you sure?')
			));
		return $this;
	}


}