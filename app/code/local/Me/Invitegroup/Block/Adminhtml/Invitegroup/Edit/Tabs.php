<?php
class Me_Invitegroup_Block_Adminhtml_Invitegroup_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("invitegroup_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("invitegroup")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("invitegroup")->__("Item Information"),
				"title" => Mage::helper("invitegroup")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("invitegroup/adminhtml_invitegroup_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
