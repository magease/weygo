<?php
	
class Me_Invitegroup_Block_Adminhtml_Invitegroup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "invite_id";
				$this->_blockGroup = "invitegroup";
				$this->_controller = "adminhtml_invitegroup";
				$this->_updateButton("save", "label", Mage::helper("invitegroup")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("invitegroup")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("invitegroup")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("invitegroup_data") && Mage::registry("invitegroup_data")->getId() ){

				    return Mage::helper("invitegroup")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("invitegroup_data")->getId()));

				} 
				else{

				     return Mage::helper("invitegroup")->__("Add Item");

				}
		}
}