<?php
class Me_Invitegroup_Block_Adminhtml_Invitegroup_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset("invitegroup_form", array("legend"=>Mage::helper("invitegroup")->__("Item information")));


						// $fieldset->addField("invite_id", "text", array(
						// "label" => Mage::helper("invitegroup")->__("invite_id"),
						// "name" => "invite_id",
						// ));

		$fieldset->addField("invite_code", "text", array(
			"label" => Mage::helper("invitegroup")->__("invite_code"),
			"name" => "invite_code",
			'required'  => true,
			));

		// $fieldset->addField("group", "text", array(
		// 	"label" => Mage::helper("invitegroup")->__("group "),
		// 	"name" => "group",
		// 	));

		$fieldset->addField('group', 'select', array(
			'name'      => 'group',
			'label'     => Mage::helper('invitegroup')->__('Customer Groups'),
			'title'     => Mage::helper('invitegroup')->__('Customer Groups'),
			'required'  => true,
			'values'    => Mage::getResourceModel('customer/group_collection')->addFieldToFilter('customer_group_id',array('neq'=>0))->toOptionArray(),
			));

		$fieldset->addField("limit", "text", array(
			"label" => Mage::helper("invitegroup")->__("limit"),
			"name" => "limit",
			'required'  => true,
			));


		if (Mage::getSingleton("adminhtml/session")->getInvitegroupData())
		{
			$form->setValues(Mage::getSingleton("adminhtml/session")->getInvitegroupData());
			Mage::getSingleton("adminhtml/session")->setInvitegroupData(null);
		} 
		elseif(Mage::registry("invitegroup_data")) {
			$form->setValues(Mage::registry("invitegroup_data")->getData());
		}
		return parent::_prepareForm();
	}
}
