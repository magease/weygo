<?php

class Me_Invitegroup_InvitationController extends Mage_Core_Controller_Front_Action
{

   public function invitationCodeAction()
   {
    // if (!$this->_validateFormKey()) {

    //     Mage::throwException('Invalid form key');

    // }

    $data= $this->getRequest()->getPost();
    $invatation_code = $data['invatation_code'];

    $result = array();

    if ($invatation_code) {

        try {
            $invite_model = Mage::getModel("invitegroup/invitegroup");

            $invite_collections = $invite_model->getCollection();

            $invSuccess = 0;

            foreach ($invite_collections as $invite) {

                if ($invatation_code == $invite->getInviteCode()) {

                    $invSuccess=1;

                }
            }
            if($invSuccess){

                $result['success'] = 1;

                $result['message'] = $this->__('Invitation Code Validated Success.');
            }else{

                $result['success'] = 0;

                $result['message'] = $this->__('Invitation Code Validated Failed.');
            }

        } catch (Exception $e) {

            $result['success'] = 0;

            $result['error'] = $this->__('Can not remove the item.');

        }

    }



    $this->getResponse()->setHeader('Content-type', 'application/json');

    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
}

}   