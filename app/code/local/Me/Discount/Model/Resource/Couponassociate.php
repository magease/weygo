<?php

class Me_Discount_Model_Resource_Couponassociate extends Mage_Core_Model_Resource_Db_Abstract {

    /**
     * Define main table
     *
     */
    protected function _construct() {
        $this->_init('discount/couponassociate', 'associate_id');
    }

    /**
     * Get rule users
     *
     * @param Mage_Admin_Model_Rules $rule
     * @return array|false
     */
    public function getRuleCustomers(Me_Discount_Model_Couponassociate $rule) {
        $read = $this->_getReadAdapter();
//        die($rule->getData());
        $binds = array(
            'rule_id' => $rule->getId(),
        );

        $select = $read->select()
                ->from($this->getMainTable(), array('customer_ids'))
                ->where('rule_id = :rule_id')
                ->where('customer_ids > 0');

        return $read->fetchCol($select, $binds);
    }

    /**
     * Delete user role
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function deleteFromRule(Mage_Core_Model_Abstract $user) {
        if ($user->getCustomerIds() <= 0) {
            return $this;
        }
        if ($user->getRuleId() <= 0) {
            return $this;
        }

        $dbh = $this->_getWriteAdapter();

        $condition = array(
            'customer_ids = ?' => (int) $user->getCustomerIds(),
            'rule_id = ?' => (int) $user->getRuleId(),
        );

        $dbh->delete($this->getTable('discount/couponassociate'), $condition);
        return $this;
    }

    /**
     * Check if role user exists
     *
     * @param Mage_Core_Model_Abstract $user
     * @return array|false
     */
    public function ruleUserExists(Mage_Core_Model_Abstract $user) {
//        if ($user->getCustomerId() > 0) {
//            $roleTable = $this->getTable('admin/role');
//
//            $dbh = $this->_getReadAdapter();
//
//            $binds = array(
//                'parent_id' => $user->getRoleId(),
//                'user_id' => $user->getUserId(),
//            );
//
//            $select = $dbh->select()->from($roleTable)
//                    ->where('parent_id = :parent_id')
//                    ->where('user_id = :user_id');
//
//            return $dbh->fetchCol($select, $binds);
//        } else {
//            return array();
//        }
    }

    /**
     * Save user roles
     *
     * @param Mage_Core_Model_Abstract $user
     * @return Mage_Admin_Model_Resource_User
     */
    public function add(Mage_Core_Model_Abstract $user) {
        $dbh = $this->_getWriteAdapter();

//        $aRoles = $this->hasAssigned2Role($user);
//        if (sizeof($aRoles) > 0) {
//            foreach ($aRoles as $idx => $data) {
//                $conditions = array(
//                    'role_id = ?' => $data['role_id'],
//                );
//
//                $dbh->delete($this->getTable('admin/role'), $conditions);
//            }
//        }
//        if ($user->getId() > 0) {
//            $role = Mage::getModel('admin/role')->load($user->getRoleId());
//        } else {
//            $role = new Varien_Object();
//            $role->setTreeLevel(0);
//        }
//        die(print_r($user));
        Mage::log('$user', null, 'magease.log');
        Mage::log(print_r($user->getData()), null, 'magease.log');
        Mage::log($user, null, 'magease.log');
        $data = new Varien_Object(array(
            'rule_id' => $user->getRuleId(),
            'name' => $user->getName(),
            'from_date' => $user->getFromDate(),
            'to_date' => $user->getToDate(),
            'is_active' => $user->getIsActive(),
            'coupon_id' => $user->getCouponId(),
            'coupon_code' => $user->getCouponCode(),
            'customer_ids' => $user->getCustomerIds()
        ));

        $insertData = $this->_prepareDataForTable($data, $this->getTable('discount/couponassociate'));

        $dbh->insert($this->getTable('discount/couponassociate'), $insertData);

        return $this;
    }

}

?>