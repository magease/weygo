<?php

/**
 * Sharing data model
 *
 * @method Me_Discount_Model_Resource_Sharingcoupon _getResource()
 * @method Me_Discount_Model_Resource_Sharingcoupon getResource()
 * @method int getCustomerId()
 * @method Me_Discount_Model_Sharingcoupon setCustomerId(int $value)
 * @method string getDate()
 * @method Me_Discount_Model_Sharingcoupon setDate(string $value)
 * @method string getAcceptEmail()
 * @method Me_Discount_Model_Sharingcoupon setEmail(string $value)
 * @method int getReferralId()
 * @method Me_Discount_Model_Sharingcoupon setReferralId(int $value)
 * @method string getProtectionCode()
 * @method Me_Discount_Model_Sharingcoupon setProtectionCode(string $value)
 * @method string getSignupDate()
 * @method Me_Discount_Model_Sharingcoupon setSignupDate(string $value)
 * @method Me_Discount_Model_Sharingcoupon setStoreId(int $value)
 * @method int getGroupId()
 * @method Me_Discount_Model_Sharingcoupon setGroupId(int $value)
 * @method string getMessage()
 * @method Me_Discount_Model_Sharingcoupon setMessage(string $value)
 * @method string getStatus()
 * @method Me_Discount_Model_Sharingcoupon  setStatus(string $value)
 *
 * @category    Me
 * @package     Me_Discount
 * @author      Magease Dev Team <info@magease.com>
 */
class Me_Discount_Model_Sharingcoupon extends Mage_Core_Model_Abstract {

    const STATUS_NEW = 'new';
    const STATUS_SENT = 'sent';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_CANCELED = 'canceled';
    const XML_PATH_EMAIL_IDENTITY = 'mecoupon/sharing/identity';
    const XML_PATH_EMAIL_TEMPLATE = 'mecoupon/sharing/template';
    const ERROR_STATUS = 1;
    const ERROR_INVALID_DATA = 2;
    const ERROR_CUSTOMER_NOT_EXISTS = 3;
    const ERROR_CUSTOMER_GROUP_INVALID = 4;

    private static $_customerExistsLookup = array();

    protected function _construct() {

        $this->_init("discount/sharingcoupon");
    }

    /**
     * Model before save
     *
     * @return Me_Discount_Model_Sharingcoupon
     */
    protected function _beforeSave() {
        if (!$this->getId()) {
            // set initial data for new one
            $this->addData(array(
//                'protection_code' => Mage::helper('core')->uniqHash(),
                'status' => self::STATUS_NEW,
                'sharing_date' => $this->getResource()->formatDate(time()),
//                'store_id' => $this->getStoreId(),
            ));
            $sharinger = $this->getSharinger();
            if ($sharinger) {
                $this->setCustomerId($sharinger->getId());
            }

            $this->makeSureCustomerExists();

            $this->makeSureCanBeBind();
        } else {
            if ($this->dataHasChangedFor('message') && !$this->canMessageBeUpdated()) {
                throw new Mage_Core_Exception(
                Mage::helper('discount')->__('Message cannot be updated.'), self::ERROR_STATUS
                );
            }
        }
        return parent::_beforeSave();
    }

    /**
     * Update status history after save
     *
     * @return Me_Discount_Model_Sharingcoupon
     */
    protected function _afterSave() {

        $websiteIds = Mage::app()->getStore($this->getStoreId())->getWebsiteId();

        Mage::getModel('discount/couponassociate')
                ->load($this->getAssociateId())
                ->setCustomerEmail($this->getAcceptEmail())
                ->setCustomerIds(Mage::getModel('customer/customer')->setWebsiteId($websiteIds)->loadByEmail($this->getAcceptEmail())->getId())
                ->save();
        $parent = parent::_afterSave();
        if ($this->getStatus() === self::STATUS_NEW) {
            $this->setOrigData();
        }

        return $parent;
        
    }

    /**
     * Send coupon sharing email
     *
     * @return bool
     */
    public function sendSharingEmail() {
        $this->makeSureCanBeSent();
        $store = Mage::app()->getStore($this->getStoreId());
        $mail = Mage::getModel('core/email_template');
        $mail->setDesignConfig(array('area' => 'frontend', 'store' => $this->getStoreId()))
                ->sendTransactional(
                        $store->getConfig(self::XML_PATH_EMAIL_TEMPLATE), $store->getConfig(self::XML_PATH_EMAIL_IDENTITY), $this->getAcceptEmail(), null, array(
                    'url' => Mage::helper('discount')->getMecouponUrl($this),
//                    'allow_message' => Mage::app()->getStore()->isAdmin() || Mage::getSingleton('enterprise_invitation/config')->isInvitationMessageAllowed(),
                    'message' => $this->getMessage(),
                    'store' => $store,
                    'store_name' => $store->getGroup()->getName(), // @deprecated after 1.4.0.0-beta1
                    'sharinger_name' => ($this->getSharinger() ? $this->getSharinger()->getName() : null)
        ));
        if ($mail->getSentSuccess()) {
            $this->setStatus(self::STATUS_SENT)->setUpdateDate(true)->save();
            return true;
        }
        return false;
    }

    /**
     * Check whether sharing can be sent
     *
     * @throws Mage_Core_Exception
     */
    public function makeSureCanBeSent() {
//        die(print_r($this->getData()));
        if (!$this->getId()) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Sharing has no ID.'), self::ERROR_INVALID_DATA
            );
        }

        if ($this->getStatus() !== self::STATUS_NEW) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Sharing with status "%s" cannot be sent.', $this->getStatus()), self::ERROR_STATUS
            );
        }

        if (!$this->getAcceptEmail() || !Zend_Validate::is($this->getAcceptEmail(), 'EmailAddress')) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Invalid or empty sharing email.'), self::ERROR_INVALID_DATA
            );
        }
        $this->makeSureCustomerExists();
    }

    /**
     * Check whether message can be updated
     *
     * @return bool
     */
    public function canMessageBeUpdated() {
        return (bool) (int) $this->getId() && $this->getStatus() === self::STATUS_NEW;
    }

    /**
     * Check and get customer if it was set
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getSharinger() {
        $sharinger = $this->getCustomer();
        if (!$sharinger || !$sharinger->getId()) {
            $sharinger = null;
        }
        return $sharinger;
    }

    /**
     * Check whether customer with specified email exists
     *
     * @param string $email
     * @param string $websiteId
     * @throws Mage_Core_Exception
     */
    public function makeSureCustomerExists($email = null, $websiteId = null) {
        if (null === $websiteId) {
            $websiteId = Mage::app()->getStore($this->getStoreId())->getWebsiteId();
        }
        if (!$websiteId) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Unable to determine proper website.'), self::ERROR_INVALID_DATA
            );
        }
        if (null === $email) {
            $email = $this->getAcceptEmail();
        }
        if (!$email) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Email is not specified.'), self::ERROR_INVALID_DATA
            );
        }

        // lookup customer by specified email/website id
        if (!isset(self::$_customerExistsLookup[$email]) || !isset(self::$_customerExistsLookup[$email][$websiteId])) {
            $customer = Mage::getModel('customer/customer')->setWebsiteId($websiteId)->loadByEmail($email);
            self::$_customerExistsLookup[$email][$websiteId] = ($customer->getId() ? $customer->getId() : false);
        }
        if (self::$_customerExistsLookup[$email][$websiteId] > 0) {
            return true;
        }
        throw new Mage_Core_Exception(
        Mage::helper('discount')->__('Customer with email "%s" not exists.', $email), self::ERROR_CUSTOMER_NOT_EXISTS
        );
    }

    /**
     * Check whether customer can be bind coupon
     *
     * @param string $email
     * @param string $websiteId
     * @throws Mage_Core_Exception
     */
    public function makeSureCanBeBind($email = null, $websiteId = null) {
        if (null === $websiteId) {
            $websiteId = Mage::app()->getStore($this->getStoreId())->getWebsiteId();
        }
        if (!$websiteId) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Unable to determine proper website.'), self::ERROR_INVALID_DATA
            );
        }
        if (null === $email) {
            $email = $this->getAcceptEmail();
        }
        if (!$email) {
            throw new Mage_Core_Exception(
            Mage::helper('discount')->__('Email is not specified.'), self::ERROR_INVALID_DATA
            );
        }

        // lookup customer group by specified email/website id
        if (isset($email) && isset($websiteId)) {
            $_acceptCustomer = Mage::getModel('customer/customer')->setWebsiteId($websiteId)->loadByEmail($email);
            $websiteId_email = ($_acceptCustomer->getGroupId() ? $_acceptCustomer->getGroupId() : false);
        }

        $_groupIds = Mage::getModel('salesrule/rule')->load($this->getRuleId())->getData('customer_group_ids');

        if (in_array($websiteId_email, $_groupIds)) {
            return true;
        }
        throw new Mage_Core_Exception(
        Mage::helper('discount')->__('Customer "%s" group is invalid to bind this coupon .', $email), self::ERROR_CUSTOMER_GROUP_INVALID
        );
    }

}