<?php

class Me_Discount_Model_Couponassociate extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("discount/couponassociate");
    }

    /**
     * Return customers for this rule
     *
     * @return array|false
     */
    public function getRuleCustomers() {
        return $this->getResource()->getRuleCustomers($this);
    }

    /**
     * Unassign user from his current role
     *
     * @return Mage_Admin_Model_User
     */
    public function deleteFromRule() {
        $this->_getResource()->deleteFromRule($this);
        return $this;
    }

    /**
     * Check if such combination role/user exists
     *
     * @return boolean
     */
    public function ruleUserExists() {
        $result = $this->_getResource()->ruleUserExists($this);
        return (is_array($result) && count($result) > 0) ? true : false;
    }

    /**
     * Assign user to role
     *
     * @return Mage_Admin_Model_User
     */
    public function add() {
//        Mage::log();
        $this->_getResource()->add($this);
        return $this;
    }

    public function couponValidate($customer, $coupon) {
        $_validcoupons = $this->getCollection()
                ->addFieldToFilter('customer_ids', $customer->getId())
                ->addFieldToFilter('coupon_code', $coupon);
//                ->addFieldToFilter('is_shared',0);
        if (count($_validcoupons)) {
            return true;
        } else {
            return false;
        }
    }

}

