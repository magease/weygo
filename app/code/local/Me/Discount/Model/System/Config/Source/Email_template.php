<?php

class Me_Discount_Model_System_Config_Source_Email_template {

    public function toOptionArray() {
        $options = array(
            array('value' => 'alipay_payment', 'label' => '支付宝'),
            array('value' => 'unionpay', 'label' => '网银支付'),
            array('value' => 'weixinpay', 'label' => '微信支付'),
            array('value' => 'cashondelivery', 'label' => 'Cash On Delivery'),
            array('value' => 'checkmo', 'label' => 'check money order'),
            array('value' => 'others', 'label' => '其他'),
        );
        return $options;
    }

}

?>