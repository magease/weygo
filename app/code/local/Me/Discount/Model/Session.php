<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Me
 * @package     Me_Discount
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * Discount session model
 *
 * @category   Me
 * @package    Me_Discount
 * @author     Magease Dev Team <info@magease.com>
 */
class Me_Discount_Model_Session extends Mage_Core_Model_Session_Abstract {

    public function __construct() {
//        $namespace = 'discount';
//        $namespace .= '_' . (Mage::app()->getStore()->getWebsite()->getCode());

//        $this->init($namespace);
//        Mage::dispatchEvent('discount_session_init', array('discount_session' => $this));
        
        $this->init('discount');
    }

}
