<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
create table salesrule_customer_bind(
    associate_id int not null auto_increment,
    rule_id varchar(100),
    name varchar(100),
    from_date varchar(100),
    to_date varchar(100),
    is_active varchar(100),
    coupon_id varchar(100),
    coupon_code varchar(100),
    customer_ids varchar(100),
    customer_email varchar(100),
    customer_name varchar(100),
    is_shareable int(10),
    is_shared int(10),
    primary key(associate_id)
    );

		
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$conn = $installer->getConnection();
$conn->addColumn($this->getTable('salesrule/rule'), 'is_associate', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable' => true,
    'default' => '0',
        ), 'Is Associate');
$conn->addColumn($this->getTable('salesrule/rule'), 'validity', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
    'nullable' => true,
    'default' => '0',
        ), 'Validity');


//$installer->getTable('salesrule/rule')
//        ->addColumn('is_associate', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
//            'unsigned' => true,
//            'nullable' => true,
//            'default' => '0',
//                ), 'Is Associate')
//        ->addColumn('validity', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
//            'unsigned' => true,
//            'nullable' => true,
//            'default' => '0',
//                ), 'Validity');
//$installer->run("
// ALTER TABLE `{$this->getTable('salesrule')}`
//     ADD COLUMN `times_used` int (11) unsigned DEFAULT '0' NOT NULL
//         AFTER `simple_free_shipping`;
// ");
$installer->endSetup();

