<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT

create table salesrule_customer_sharing(
sharing_id int(10) not null auto_increment,
customer_id int(10),
coupon_id int(10),
coupon_name varchar(100),
coupon_code varchar(100),
sharing_date timestamp,
accept_email varchar(100),
accept_id int(10),
confirm_date timestamp,
to_date timestamp,
message text,
status varchar(8) NOT NULL DEFAULT 'new',
primary key(sharing_id)
);

SQLTEXT;
$installer->run($sql);

$conn = $installer->getConnection();
$conn->addColumn($installer->getTable('discount/couponassociate'), 'is_shareable', array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'length' => '10',
            'nullable' => true,
            'default' => null,
            'comment' => 'Is Shareable'
                )
);
$conn->addColumn($installer->getTable('discount/couponassociate'), 'is_shared', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'length' => '10',
    'nullable' => true,
    'default' => null,
    'comment' => 'Is Shared'
        )
);
$installer->endSetup();

