<?php

class Me_Discount_Helper_Data extends Mage_Core_Helper_Abstract {

    protected function _getCustomerGroupId() {
        return $custGroupID = Mage::getSingleton('customer/session')->getCustomerGroupId();
    }

    protected function _getCustomerId() {
        return $custGroupID = Mage::getSingleton('customer/session')->getCustomerId();
    }

    /**
     * @return Mage_SalesRule_Model_Resource_Rule_Collection
     */
    public function getCouponListNotBind() {
        $storeId = Mage::app()->getStore()->getId();
        $websiteId = Mage::app()->getStore($storeId)->getWebsiteId();
        $customerGroup = $this->_getCustomerGroupId();
        $now = date('Y-m-d');
        $collection = Mage::getModel('salesrule/rule')->getResourceCollection();
        $collection->addWebsiteGroupDateFilter($websiteId, $customerGroup, $now)
                ->addFieldToFilter('coupon_type', ['neq' => 1]) //voglio solo le regole con un codice coupon
                ->addFieldToFilter('is_associate', ['neq' => 1])
                ->setOrder('from_date', 'desc');
        $collection->load();
        return $collection;
    }

    /**
     * @return Mage_SalesRule_Model_Resource_Rule_Collection
     */
    public function getCouponListBinded() {
        $customerId = $this->_getCustomerId();
        $collection = Mage::getModel('discount/couponassociate')->getCollection();
        $collection
//                ->addWebsiteGroupDateFilter($websiteId, $customerGroup, $now)
                ->addFieldToFilter('customer_ids', ['eq' => $customerId]) //shai xuan chu lai dang qian yong hu id de you hui quan.
//                ->addFieldToFilter('is_associate', ['eq' => 0])
                ->setOrder('from_date', 'desc');
        $collection->load();
        return $collection;
    }

    /**
     * @param $rule
     * @param $customer
     *
     * @return bool
     */
    public function checkIfUsed($rule, $customer) {
        $check = Mage::getResourceModel('salesrule/rule_customer_collection');
        $check
                ->addFieldToSelect('times_used')
                ->addFieldToFilter('customer_id', $customer->getId())
                ->addFieldToFilter('rule_id', $rule->getId());
        if ($check->getFirstItem()->getTimesUsed() < $rule->getUsesPerCustomer()) {
            return true;
        } else {
            return false;
        }
        // return (bool) $check->count();
    }

    /**
     * @param $rule
     * @param $customer
     *
     * @return used_times
     */
    public function getTimesUsed($rule, $customer) {
        $check = Mage::getResourceModel('salesrule/rule_customer_collection')
                ->addFieldToFilter('customer_id', $customer->getId())
                ->addFieldToFilter('rule_id', $rule->getId())
                ->addFieldToSelect('times_used');

        if (count($check)) {
            return $check->getFirstItem()->getTimesUsed() ? $check->getFirstItem()->getTimesUsed() : '0';
        }
        return (int) 0;
    }

    /**
     * @param $rule
     * @param $customer
     *
     * @return used_times
     */
    public function getExpirationDate($rule, $customer) {
        if ($rule->getRuleId() == 4) {
            $date = Mage::app()->getLocale()->date(
                    Mage::getModel('customer/customer')->load($customer->getId())->getCreatedAtTimestamp() + 86400 * 90, Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false
            );
            return date('Y-m-d', Mage::getModel('customer/customer')->load($customer->getId())->getCreatedAtTimestamp() + 86400 * 90);
        } else {
            $BeginDate = date('Y-m-01', strtotime(date("Y-m-d")));
//            $date = Mage::app()->getLocale()->date(
//                    strtotime("$BeginDate +1 month -1 day"), Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT), null, false
//            );
            return date('Y-m-d', strtotime("$BeginDate +1 month -1 day"));
        }
    }

    /**
     * @param $rule
     * @param $customer
     *
     * @return aviliable_times
     */
    public function getAviliableTimes($rule, $customer) {
//        $rule = Mage::getModel('salesrule/rule')->load($rule->getRuleId());
        $aviliable_times = $rule->getUsesPerCustomer() - $this->getTimesUsed($rule, $customer);
        return $aviliable_times;
    }

    /**
     * @param $rule
     * @param $customer
     *
     * @return bool
     */
    public function isShowable($rule, $customer) {
        if ($this->getAviliableTimes($rule, $customer) && (strtotime($this->getExpirationDate($rule, $customer)) > strtotime("now"))) {
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getUnsedCoupon() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $collection = $this->getCouponListNotBind();
        $result = $collection->count();
        if ($collection->count() !== 0) {
            foreach ($collection as $coupon) {
                if (!$this->isShowable($coupon, $customer))
                    $result = $result - 1;
            }
        }
        return $result;
    }

    /**
     * Return mecoupon list url
     *
     * @param Me_Discount_Helper_Data $sharing
     * @return string
     */
    public function getMecouponUrl($sharing) {
        return Mage::getModel('core/url')->setStore($sharing->getStoreId())
                        ->getUrl('mecoupon', array(
                            '_store_to_url' => true
        ));
    }

}
