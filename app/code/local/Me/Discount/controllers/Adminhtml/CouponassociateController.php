<?php

class Me_Discount_Adminhtml_CouponassociateController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu("discount/couponassociate")->_addBreadcrumb(Mage::helper("adminhtml")->__("Couponassociate  Manager"), Mage::helper("adminhtml")->__("Couponassociate Manager"));
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__("Discount"));
        $this->_title($this->__("Manager Couponassociate"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__("Discount"));
        $this->_title($this->__("Couponassociate"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("discount/couponassociate")->load($id);
        if ($id !== $model->getId()) {
            $this->_redirect("*/*/new/id/" . $id);
            return;
        }
        if ($model->getId()) {


            $associateData = array();
//        echo $this->getRequest()->getParam("id");
            $_sourceData = Mage::getModel('discount/couponassociate')->load($this->getRequest()->getParam("id"))->getData();
            $associateData = $_sourceData;
            Mage::getSingleton("adminhtml/session")->setCouponassociateData($associateData);


            Mage::register("couponassociate_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("discount/couponassociate");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Couponassociate Manager"), Mage::helper("adminhtml")->__("Couponassociate Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Couponassociate Description"), Mage::helper("adminhtml")->__("Couponassociate Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit"))->_addLeft($this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit_tabs"));
            $this->_addJs(
                    $this->getLayout()->createBlock('adminhtml/template')->setTemplate('discount/rule_customer_grid_js.phtml')
            );
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("discount")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {

        $this->_title($this->__("Discount"));
        $this->_title($this->__("Couponassociate"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("discount/couponassociate")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $associateData = array();
//        echo $this->getRequest()->getParam("id");
        $_sourceData = Mage::getModel('salesrule/rule')->load($this->getRequest()->getParam("id"))->getData();
        $associateData = $_sourceData;
        Mage::getSingleton("adminhtml/session")->setCouponassociateData($associateData);
        Mage::register("couponassociate_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("discount/couponassociate");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Couponassociate Manager"), Mage::helper("adminhtml")->__("Couponassociate Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Couponassociate Description"), Mage::helper("adminhtml")->__("Couponassociate Description"));


        $this->_addContent($this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit"))->_addLeft($this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit_tabs"));

        $this->_addJs(
                $this->getLayout()->createBlock('adminhtml/template')->setTemplate('discount/rule_customer_grid_js.phtml')
        );

        $this->renderLayout();
    }

    public function saveAction() {

        $post_data = $this->getRequest()->getPost();

        $roleUsers = $this->getRequest()->getParam('in_rule_user', null);
        parse_str($roleUsers, $roleUsers);
        $roleUsers = array_keys($roleUsers);

        $oldRuleUsers = $this->getRequest()->getParam('in_rule_user_old');
        parse_str($oldRuleUsers, $oldRuleUsers);
        $oldRuleUsers = array_keys($oldRuleUsers);

        $rule_id = $this->getRequest()->getParam('rule_id');

        if ($post_data) {

            try {

                foreach ($oldRuleUsers as $oUid) {
                    $this->_deleteCustomerFromRule($oUid, $rule_id);
                }

                foreach ($roleUsers as $nRuid) {
                    $this->_addCustomerToRule($post_data,$nRuid, $rule_id);
                }
//                $model = Mage::getModel("discount/couponassociate")
//                        ->addData($post_data)
//                        ->setRuleId($this->getRequest()->getParam("rule_id"))
//                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Couponassociate was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setCouponassociateData(false);

//                if ($this->getRequest()->getParam("back")) {
//                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
//                    return;
//                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setCouponassociateData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Remove user from role
     *
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    protected function _deleteCustomerFromRule($userId, $ruleId) {
        try {
            Mage::getModel('discount/couponassociate')
                    ->setRuleId($ruleId)
                    ->setCustomerIds($userId)
                    ->deleteFromRule();
        } catch (Exception $e) {
            throw $e;
            return false;
        }
        return true;
    }

    /**
     * Assign user to role
     *
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    protected function _addCustomerToRule($post_data,$userId, $ruleId) {

////        die($userId);
////        $user = Mage::getModel('discount/couponassociate')->load($userId);
//        $user = Mage::getModel('discount/couponassociate')->getCollection()
//                ->addFieldToFilter('customer_ids', $userId)
//                ->addFieldToFilter('rule_id', $ruleId)
//                ->getFirstItem();
////                ->addFieldToFilter('customer_ids', array('eq' => $userId))
////                ->addFieldToFilter('rule_id', array('eq' => $ruleId));
////                array(
////                    array('customer_ids' => 'customer_ids', 'eq' => $userId),
////                    array('rule_id' => 'rule_id', 'eq' => $ruleId)
////                )
////        );
////        ->getFirstItem();
////        $user->setRoleId($ruleId)->setUserId($userId);
////        if ($user->ruleUserExists() === true) {
//        if ($user) {
//            return false;
//        } else {
//            Mage::log('aaa');
//            die('ok');
//            $user->add();
//            return true;
//        }




        $model = Mage::getModel("discount/couponassociate")
                ->addData($post_data)
//                ->setRuleId($ruleId)
                ->setCustomerIds($userId)
                ->save();
    }

    /**
     * Action for ajax request from assigned users grid
     *
     */
    public function bindcustomergridAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('discount/adminhtml_couponassociate_edit_tab_customer')->toHtml()
        );
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("discount/couponassociate");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('associate_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("discount/couponassociate");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'couponassociate.csv';
        $grid = $this->getLayout()->createBlock('discount/adminhtml_couponassociate_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'couponassociate.xml';
        $grid = $this->getLayout()->createBlock('discount/adminhtml_couponassociate_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
