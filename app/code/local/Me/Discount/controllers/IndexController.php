<?php

class Me_Discount_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Only logged in users can use this functionality,
     * this function checks if user is logged in before all other actions
     *
     */
    public function preDispatch() {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->getResponse()->setRedirect(Mage::helper('customer')->getLoginUrl());
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    public function indexAction() {
        $this->loadLayout();
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('discount')->__('Coupons'));
        }
        $this->renderLayout();
    }

//    public function sharepostAction() {
//        $this->loadLayout();
//        die(print_r($this->getRequest()->getPost()));
//        $this->renderLayout();
//    }

    /**
     * Validate coupons before sharing.
     *
     */
    private function _couponValidate($customer, $coupon) {
        $_validcoupons = Mage::getModel('discount/couponassociate')->getCollection()
                ->addFieldToFilter('customer_ids', $customer->getId())
                ->addFieldToFilter('coupon_code', $coupon);
        if (count($_validcoupons)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Share coupon to customer from frontend
     *
     */
    public function sharepostAction() {

        $data = $this->getRequest()->getPost();
        if ($data) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customerNotExists = 0;
//            $email = $data['email'];

            if (!Mage::getModel('discount/couponassociate')->couponValidate($customer, $data['sh_coupon'])) {

                Mage::getSingleton('core/session')->addError(
                        Mage::helper('discount')->__('coupons for your sharing is invalid.')
                );
                $this->_redirect('*/*/');
                return;
            }

            try {
                $sharing = Mage::getModel('discount/sharingcoupon')->setData($data,
                                array(
                                    'customer' => $customer,
//                                    'accept_email' => $email,
//                                    'rule_id'=>$data['rule_id'],
//                                    'coupon_id' => $data['sh_coupon'],
//                                    'associate_id' => $data['associate_id'],
//                                    'coupon_code' => $data['sh_coupon'],
//                                    'coupon_name' => $data['sh_coupon'],
//                                    'message' => (isset($data['message']) ? $data['message'] : ''),
                        ))
                        ->save();
                if ($sharing->sendSharingEmail()) {
                    Mage::getSingleton('core/session')->addSuccess(
                            Mage::helper('discount')->__('Coupons has been sent to %s already.', $data['accept_email'])
                    );
                } else {
                    die();
                    Mage::getSingleton('core/session')->addError(
                            Mage::helper('discount')->__('Coupons send failure.')
                    );
                    throw new Exception(''); // not Mage_Core_Exception intentionally
                }
            } catch (Mage_Core_Exception $e) {
                if (Me_Discount_Model_Sharingcoupon::ERROR_CUSTOMER_NOT_EXISTS === $e->getCode()) {
                    $customerNotExists = 1;
                } else {
                    Mage::getSingleton('core/session')->addError($e->getMessage());
                }
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError(
                        Mage::helper('discount')->__('Failed to send email to %s.', $data['accept_email'])
                );
            }
            if ($customerNotExists) {
                Mage::getSingleton('core/session')->addNotice(
                        Mage::helper('discount')->__('%d sharing was not sent, because customer accounts not exist for specified email addresses.', $customerNotExists)
                );
            }
            $this->_redirect('*/*/');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->loadLayoutUpdates();
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->setTitle(Mage::helper('discount')->__('Send Sharing'));
        }
        $this->renderLayout();
    }

}