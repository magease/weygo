<?php

class Me_Discount_Block_Adminhtml_Couponassociate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId("couponassociateGrid");
		$this->setDefaultSort("associate_id");
		$this->setDefaultDir("DESC");
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel("discount/couponassociate")->getCollection();
		// ->joinAttribute(
  //               'name',
  //               'catalog_product/name',
  //               'entity_id',
  //               null,
  //               'left'
  //           );
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn("associate_id", array(
			"header" => Mage::helper("discount")->__("ID"),
			"align" =>"right",
			"width" => "50px",
			"type" => "number",
			"index" => "associate_id",
			));

		$this->addColumn("rule_id", array(
			"header" => Mage::helper("discount")->__("rule_id"),
			"index" => "rule_id",
			));
		$this->addColumn("rule_name", array(
			"header" => Mage::helper("discount")->__("rule_name"),
			"index" => "name",
			));
		$this->addColumn("from_date", array(
			"header" => Mage::helper("discount")->__("from_date"),
			"index" => "from_date",
			));
		$this->addColumn("to_date", array(
			"header" => Mage::helper("discount")->__("to_date"),
			"index" => "to_date",
			));
		$this->addColumn("is_active", array(
			"header" => Mage::helper("discount")->__("is_active"),
			"index" => "is_active",
			'type'      => 'options',
			'options'   => array(
				1 => 'Active',
				0 => 'Inactive',
				),
			));
		$this->addColumn("coupon_id", array(
			"header" => Mage::helper("discount")->__("coupon_id"),
			"index" => "coupon_id",
			));
		$this->addColumn("coupon_code", array(
			"header" => Mage::helper("discount")->__("coupon_code"),
			"index" => "coupon_code",
			));
		$this->addColumn("customer_ids", array(
			"header" => Mage::helper("discount")->__("customer_ids"),
			"index" => "customer_ids",
			));
				$this->addColumn("customer_email", array(
			"header" => Mage::helper("discount")->__("email"),
			"index" => "email",
			));
			// $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			// $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		// return $this->getUrl("*/*/edit", array("id" => $row->getId()));
	}



	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('associate_id');
		$this->getMassactionBlock()->setFormFieldName('associate_ids');
		$this->getMassactionBlock()->setUseSelectAll(true);
		$this->getMassactionBlock()->addItem('remove_couponassociate', array(
			'label'=> Mage::helper('discount')->__('Remove Couponassociate'),
			'url'  => $this->getUrl('*/adminhtml_couponassociate/massRemove'),
			'confirm' => Mage::helper('discount')->__('Are you sure?')
			));
		return $this;
	}


}