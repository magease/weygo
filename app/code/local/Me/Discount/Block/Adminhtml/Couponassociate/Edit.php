<?php

class Me_Discount_Block_Adminhtml_Couponassociate_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "associate_id";
        $this->_blockGroup = "discount";
        $this->_controller = "adminhtml_couponassociate";
        $this->_updateButton("save", "label", Mage::helper("discount")->__("Save Item"));
        // $this->_updateButton("delete", "label", Mage::helper("discount")->__("Delete Item"));
        $this->removeButton("delete");

        // $this->_addButton("saveandcontinue", array(
        //     "label" => Mage::helper("discount")->__("Save And Continue Edit"),
        //     "onclick" => "saveAndContinueEdit()",
        //     "class" => "save",
        //         ), -100);



        $this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    public function getHeaderText() {
        if (Mage::registry("couponassociate_data") && Mage::registry("couponassociate_data")->getId()) {

            return Mage::helper("discount")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("couponassociate_data")->getId()));
        } else {

            return Mage::helper("discount")->__("Add Item");
        }
    }

}