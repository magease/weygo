<?php

/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition End User License Agreement
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magento.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license http://www.magento.com/license/enterprise-edition
 */

/**
 * Product in category grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Me_Discount_Block_Adminhtml_Couponassociate_Edit_Tab_Customer extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('discount_bind_customer');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

//    public function getCategory() {
//        return Mage::registry('category');
//    }

    protected function _addColumnFilterToCollection($column) {
        // Set custom filter for in category flag
        if ($column->getId() == 'in_category') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection() {
//        if ($this->getCategory()->getId()) {
//            $this->setDefaultFilter(array('in_category' => 1));
//        }
        $collection = Mage::getResourceModel('customer/customer_collection')
                // ->addFieldToFilter('group_id',)
                ->addNameToSelect()
                ->addAttributeToSelect('email')
                ->addAttributeToSelect('created_at')
                ->addAttributeToSelect('group_id')
                // ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
                ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
                ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left');
                // ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
                // ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');

        $this->setCollection($collection);
//        $collection = Mage::getModel('customer/customer')->getCollection()
//                ->addAttributeToSelect('email')
//                ->addAttributeToSelect('name')
//                ->addAttributeToSelect('group_id');
//                ->addStoreFilter($this->getRequest()->getParam('store'));
//                ->joinField('position', 'catalog/category_product', 'position', 'product_id=entity_id', 'category_id=' . (int) $this->getRequest()->getParam('id', 0), 'left');
        // $this->setCollection($collection);

//        if ($this->getCategory()->getProductsReadonly()) {
//            $productIds = $this->_getSelectedProducts();
//            if (empty($productIds)) {
//                $productIds = 0;
//            }
//            $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
//        }

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
//        if (!$this->getCategory()->getProductsReadonly()) {
        $this->addColumn('in_rule_users', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_rule_users',
            'values' => $this->_getUsers(),
            'align' => 'center',
            'index' => 'entity_id'
        ));
//        }
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('customer')->__('ID'),
            'width' => '50px',
            'index' => 'entity_id',
            'type' => 'number',
        ));
        $this->addColumn('email', array(
            'header' => Mage::helper('catalog')->__('email'),
            'sortable' => true,
            'width' => '60',
            'index' => 'email'
        ));
        $this->addColumn('customer_name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name'
        ));
        $groups = Mage::getResourceModel('customer/group_collection')
                ->addFieldToFilter('customer_group_id', array('gt' => 0))
                ->load()
                ->toOptionHash();

        $this->addColumn('group', array(
            'header' => Mage::helper('customer')->__('Group'),
            'width' => '100',
            'index' => 'group_id',
            'type' => 'options',
            'options' => $groups,
        ));

        $this->addColumn('Telephone', array(
            'header' => Mage::helper('customer')->__('Telephone'),
            'width' => '100',
            'index' => 'billing_telephone'
        ));
        // $this->addColumn('billing_postcode', array(
        //     'header' => Mage::helper('customer')->__('ZIP'),
        //     'width' => '90',
        //     'index' => 'billing_postcode',
        // ));
        // $this->addColumn('billing_region', array(
        //     'header' => Mage::helper('customer')->__('State/Province'),
        //     'width' => '100',
        //     'index' => 'billing_region',
        // ));

        $this->addColumn('customer_since', array(
            'header' => Mage::helper('customer')->__('Customer Since'),
            'type' => 'datetime',
            'align' => 'center',
            'index' => 'created_at',
            'gmtoffset' => true
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('website_id', array(
                'header' => Mage::helper('customer')->__('Website'),
                'align' => 'center',
                'width' => '80px',
                'type' => 'options',
                'options' => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(true),
                'index' => 'website_id',
            ));
        }

//        $this->addColumn('sku', array(
//            'header' => Mage::helper('catalog')->__('SKU'),
//            'width' => '80',
//            'index' => 'sku'
//        ));
//        $this->addColumn('price', array(
//            'header' => Mage::helper('catalog')->__('Price'),
//            'type' => 'currency',
//            'width' => '1',
//            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
//            'index' => 'price'
//        ));
//        $this->addColumn('position', array(
//            'header' => Mage::helper('catalog')->__('Position'),
//            'width' => '1',
//            'type' => 'number',
//            'index' => 'position',
//            'editable' => !$this->getCategory()->getProductsReadonly()
//                //'renderer'  => 'adminhtml/widget_grid_column_renderer_input'
//        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        $id = $this->getRequest()->getParam('id');
        return $this->getUrl('*/*/bindcustomergrid', array('id' => $id));
    }

    protected function _getUsers($json = false) {
        if ($this->getRequest()->getParam('in_rule_users') != "") {
            return $this->getRequest()->getParam('in_rule_users');
        }
        $id = ( $this->getRequest()->getParam('id') > 0 ) ? $this->getRequest()->getParam('id') : Mage::registry('ID');

        $customers = Mage::getModel('discount/couponassociate')->setId($id)->getRuleCustomers();
        if (sizeof($customers) > 0) {
            if ($json) {
                $jsonCustomers = Array();
                foreach ($customers as $customer)
                    $jsonCustomers[$customer] = 0;
                return Mage::helper('core')->jsonEncode((object) $jsonCustomers);
            } else {
                return array_values($customers);
            }
        } else {
            if ($json) {
                return '{}';
            } else {
                return array();
            }
        }
    }

}

