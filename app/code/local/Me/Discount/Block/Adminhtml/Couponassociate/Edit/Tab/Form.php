<?php

class Me_Discount_Block_Adminhtml_Couponassociate_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("discount_form", array("legend" => Mage::helper("discount")->__("Item information")));


        // $fieldset->addField("associate_id", "hidden", array(
        //     "label" => Mage::helper("discount")->__("associate_id"),
        //     "name" => "associate_id",
        //     "required" =>true,
        //     ));

        $fieldset->addField('in_rule_user', 'hidden', array(
            'name' => 'in_rule_user',
            'id' => 'in_rule_user',
                )
        );

        $fieldset->addField('in_rule_user_old', 'hidden', array(
            'name' => 'in_rule_user_old',
            'id' => 'in_rule_user_old',
                )
        );

        $fieldset->addField("rule_id", "text", array(
            "label" => Mage::helper("discount")->__("rule_id"),
            "name" => "rule_id",
            "required" => true,
        ));
        // $fieldset->addField("disabled_rule_id", "text", array(
        //     "label" => Mage::helper("discount")->__("rule_id"),
        //     "name" => "disabled_rule_id",
        //     "required" =>true,
        //     "disabled" =>true,
        //     ));

        $fieldset->addField("name", "text", array(
            "label" => Mage::helper("discount")->__("name"),
            "name" => "name",
            "required" => true,
        ));
        // $fieldset->addField("disabled_name", "text", array(
        //     "label" => Mage::helper("discount")->__("name"),
        //     "name" => "disabled_name",
        //     "required" =>true,
        //     "disabled" =>true,
        //     ));

        $fieldset->addField("from_date", "text", array(
            "label" => Mage::helper("discount")->__("from_date"),
            "name" => "from_date",
        ));
        // $fieldset->addField("disabled_from_date", "text", array(
        //     "label" => Mage::helper("discount")->__("from_date"),
        //     "name" => "disabled_from_date",
        //     "required" =>true,
        //     "disabled" =>true,
        //     ));

        $fieldset->addField("to_date", "text", array(
            "label" => Mage::helper("discount")->__("to_date"),
            "name" => "to_date",
        ));
        // $fieldset->addField("disabled_to_date", "text", array(
        //     "label" => Mage::helper("discount")->__("to_date"),
        //     "name" => "disabled_to_date",
        //     "required" =>true,
        //     "disabled"=>true,
        //     ));

        $fieldset->addField("is_active", "select", array(
            "label" => Mage::helper("discount")->__("is_active"),
            "name" => "is_active",
            "required" => true,
            "values" =>Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        // $fieldset->addField("disabled_is_active", "text", array(
        //     "label" => Mage::helper("discount")->__("is_active"),
        //     "name" => "disabled_is_active",
        //     "required" =>true,
        //     "disabled"=>true,
        //     ));

        $fieldset->addField("coupon_id", "text", array(
            "label" => Mage::helper("discount")->__("coupon_id"),
            "name" => "coupon_id",
        ));
        // $fieldset->addField("disabled_coupon_id", "text", array(
        //     "label" => Mage::helper("discount")->__("coupon_id"),
        //     "name" => "disabled_coupon_id",
        //     "required" =>true,
        //     "disabled"=>true,
        //     ));

        $fieldset->addField("coupon_code", "text", array(
            "label" => Mage::helper("discount")->__("coupon_code"),
            "name" => "coupon_code",
            "required" => true,
        ));
        // $fieldset->addField("disabled_coupon_code", "text", array(
        //     "label" => Mage::helper("discount")->__("coupon_code"),
        //     "name" => "disabled_coupon_code",
        //     "required" =>true,
        //     "disabled"=>true,
        //     ));
        // $fieldset->addField("customer_ids", "text", array(
        // "label" => Mage::helper("discount")->__("customer_ids"),
        // "name" => "customer_ids",
        // ));


        if (Mage::getSingleton("adminhtml/session")->getCouponassociateData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getCouponassociateData());
            Mage::getSingleton("adminhtml/session")->setCouponassociateData(null);
        } elseif (Mage::registry("couponassociate_data")) {
            $form->setValues(Mage::registry("couponassociate_data")->getData());
        }
        return parent::_prepareForm();
    }

}
