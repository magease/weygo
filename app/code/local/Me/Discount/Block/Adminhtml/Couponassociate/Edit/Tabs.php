<?php
class Me_Discount_Block_Adminhtml_Couponassociate_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId("couponassociate_tabs");
		$this->setDestElementId("edit_form");
		$this->setTitle(Mage::helper("discount")->__("Item Information"));
	}
	protected function _beforeToHtml()
	{
		$this->addTab("form_section", array(
			"label" => Mage::helper("discount")->__("Item Information"),
			"title" => Mage::helper("discount")->__("Item Information"),
			"content" => $this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit_tab_form")->toHtml(),
			));
		$this->addTab("bind_customer", array(
			"label" => Mage::helper("discount")->__("Rule Bind Customer"),
			"title" => Mage::helper("discount")->__("Rule Bind Customer"),
			"content" => $this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit_tab_customer")->toHtml(),
			));
		// $this->addTab('products', array(
		// 	'label'     => Mage::helper('catalog')->__('Category Products'),
		// 	'content'   => $this->getLayout()->createBlock(
		// 		'adminhtml/catalog_category_tab_product',
		// 		'category.product.grid'
		// 		)->toHtml(),
		// 	));
		return parent::_beforeToHtml();
	}

}
