<?php

class Me_Discount_Block_Adminhtml_Couponlist_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("discount_form", array("legend" => Mage::helper("discount")->__("Item information")));


        $fieldset->addField("associate_id", "text", array(
            "label" => Mage::helper("discount")->__("associate_id"),
            "name" => "associate_id",
        ));

        $fieldset->addField("rule_id", "text", array(
            "label" => Mage::helper("discount")->__("rule_id"),
            "name" => "rule_id",
        ));

        $fieldset->addField("rule_name", "text", array(
            "label" => Mage::helper("discount")->__("rule_name"),
            "name" => "rule_name",
        ));

        $fieldset->addField("from_date", "text", array(
            "label" => Mage::helper("discount")->__("from_date"),
            "name" => "from_date",
        ));

        $fieldset->addField("to_date", "text", array(
            "label" => Mage::helper("discount")->__("to_date"),
            "name" => "to_date",
        ));

        $fieldset->addField("is_active", "text", array(
            "label" => Mage::helper("discount")->__("is_active"),
            "name" => "is_active",
        ));

        $fieldset->addField("coupon_id", "text", array(
            "label" => Mage::helper("discount")->__("coupon_id"),
            "name" => "coupon_id",
        ));

        $fieldset->addField("coupon_code", "text", array(
            "label" => Mage::helper("discount")->__("coupon_code"),
            "name" => "coupon_code",
        ));

        $fieldset->addField("customer_ids", "text", array(
            "label" => Mage::helper("discount")->__("customer_ids"),
            "name" => "customer_ids",
        ));


        if (Mage::getSingleton("adminhtml/session")->getCouponassociateData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getCouponassociateData());
            Mage::getSingleton("adminhtml/session")->setCouponassociateData(null);
        } elseif (Mage::registry("couponassociate_data")) {
            $form->setValues(Mage::registry("couponassociate_data")->getData());
        }
        return parent::_prepareForm();
    }

}
