<?php
class Me_Discount_Block_Adminhtml_Couponlist_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("couponassociate_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("discount")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("discount")->__("Item Information"),
				"title" => Mage::helper("discount")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("discount/adminhtml_couponassociate_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
