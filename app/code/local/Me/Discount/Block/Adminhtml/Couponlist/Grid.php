<?php

class Me_Discount_Block_Adminhtml_Couponlist_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId("couponassociateGrid");
		$this->setDefaultSort("rule_id");
		$this->setDefaultDir("DESC");
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('salesrule/rule')
		->getResourceCollection()
        ->addFieldToFilter('coupon_type',2)
        ->addFieldToFilter('is_associate',true);
		$this->setCollection($collection);
		$collection->addWebsitesToResult();
		return parent::_prepareCollection();
	}
	protected function _prepareColumns()
    {
        $this->addColumn('rule_id', array(
            'header'    => Mage::helper('salesrule')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'rule_id',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('salesrule')->__('Rule Name'),
            'align'     =>'left',
            'index'     => 'name',
        ));

        $this->addColumn('coupon_code', array(
            'header'    => Mage::helper('salesrule')->__('Coupon Code'),
            'align'     => 'left',
            'width'     => '150px',
            'index'     => 'code',
        ));

        $this->addColumn('from_date', array(
            'header'    => Mage::helper('salesrule')->__('Date Start'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'index'     => 'from_date',
        ));

        $this->addColumn('to_date', array(
            'header'    => Mage::helper('salesrule')->__('Date Expire'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'to_date',
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('salesrule')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array(
                1 => 'Active',
                0 => 'Inactive',
            ),
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('rule_website', array(
                'header'    => Mage::helper('salesrule')->__('Website'),
                'align'     =>'left',
                'index'     => 'website_ids',
                'type'      => 'options',
                'sortable'  => false,
                'options'   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(),
                'width'     => 200,
            ));
        }

        $this->addColumn('sort_order', array(
            'header'    => Mage::helper('salesrule')->__('Priority'),
            'align'     => 'right',
            'index'     => 'sort_order',
            'width'     => 100,
        ));

        parent::_prepareColumns();
        return $this;
    }

	public function getRowUrl($row)
	{
		return $this->getUrl("*/adminhtml_couponassociate/edit", array("id" => $row->getId()));
	}



	// protected function _prepareMassaction()
	// {
	// 	$this->setMassactionIdField('associate_id');
	// 	$this->getMassactionBlock()->setFormFieldName('associate_ids');
	// 	$this->getMassactionBlock()->setUseSelectAll(true);
	// 	$this->getMassactionBlock()->addItem('remove_couponassociate', array(
	// 		'label'=> Mage::helper('discount')->__('Remove Couponassociate'),
	// 		'url'  => $this->getUrl('*/adminhtml_couponassociate/massRemove'),
	// 		'confirm' => Mage::helper('discount')->__('Are you sure?')
	// 		));
	// 	return $this;
	// }


}