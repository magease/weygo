<?php


class Me_Discount_Block_Adminhtml_Couponlist extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		$this->_controller = "adminhtml_couponlist";
		$this->_blockGroup = "discount";
		$this->_headerText = Mage::helper("discount")->__("Couponlist Manager");
	// $this->_addButtonLabel = Mage::helper("discount")->__("Add New Item");
		parent::__construct();
		$this->_removeButton('add');
		
	}

}