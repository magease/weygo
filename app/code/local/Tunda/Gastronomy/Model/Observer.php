<?php
class Tunda_Gastronomy_Model_Observer
{
	public function checkoutCartSaveBefore(Varien_Event_Observer $observer)
	{

$resource = Mage::getSingleton('core/resource');
$read = $resource->getConnection('catalog_read');  
		$cart = $observer->getCart();
		/** @var Mage_Sales_Model_Quote $quote */
		$quote = $observer->getCart()->getQuote();
		$gastronomyQty = 0;
		$items = $quote->getItemsCollection();
		foreach ($items as $item) {
			//in EE non posso usare $item->getProduct() perché in checkout/cart gli attributi caricati sono diversi
			$product = Mage::getModel('catalog/product')->load($item->getProductId());
			if ((int)$product->getWeygoGastronomy()) {
				$gastronomyQty += $item->getQty();
			}

				 if ((int)$product->getAttributeText('grm')) {
                              $_grm[$product->getAttributeText('grm')] += $item->getQty();
                        }
		}


if ($product){
$_max_qty = $read->fetchAll("SELECT ao.sort_order, aov.value
FROM eav_attribute_option_value as aov, eav_attribute_option as ao 
where aov.value='".$product->getAttributeText('grm')."' and aov.option_id = ao.option_id and attribute_id=207 limit 1");
$_max_qty=$_max_qty[0];
//echo "<pre>";
//die(var_dump($_max_qty));
		$qty = (int)Mage::getStoreConfig('tunda_gastronomy/settings/amount');
		if ($gastronomyQty > $qty) {
			$quote->preventSaving();
			$message = Mage::helper('core')->__('You got the maximum quantity for gastronomy products');
			Mage::throwException($message);
		}
		//Mage::throwException("max" . $_max_qty["sort_order"]. "actual".$_grm[$product->getAttributeText('grm')] . "....max:".$_max_qty["sort_order"]);
	//	var_dump($product->getAttributeText('grm'));
	//	die();
		if ($_grm[$product->getAttributeText('grm')] > $_max_qty["sort_order"] &&  ($_max_qty["sort_order"]>0)) {
			$quote->preventSaving();
			$message = Mage::helper('core')->__('Hai raggiunto la quantità massima ordinabile per la categoria');
			Mage::throwException($message);
		}
		

	}
}
}

