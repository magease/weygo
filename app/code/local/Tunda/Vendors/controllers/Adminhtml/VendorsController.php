<?php 
class Tunda_Vendors_Adminhtml_VendorsController 
extends Mage_Adminhtml_Controller_Action
{
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('tunda_vendors/vendors')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('customerbrands')->__('Customer Brands'));		
		return $this;
	}   
	
	public function indexAction()
    {    	
        $this->loadLayout();
        $this->_setActiveMenu('tunda_vendors/vendors');
        $this->renderLayout();
    }

    public function importVendorsAction()
    {    	
        Mage::getModel('tunda_vendors/import')->process();
    }		
    
 	public function importVendorsProductsAction()
    {    	    	
        Mage::getModel('tunda_vendors/import_products')->process();
    }
    
	public function importVendorsStockAction()
    {    	    	    	
        Mage::getModel('tunda_vendors/import_products_stock')->process();
    }
    
	public function importVendorsPricesAction()
    {    	    	
        Mage::getModel('tunda_vendors/import_products_prices')->process();
    }
}