<?php
error_reporting(E_ALL);
set_time_limit(0);
ini_set("memory_limit","2548M");
class Tunda_Vendors_Model_Import extends Mage_Core_Model_Abstract
{	
	const VENDOR_STATUS_UPDATED 		= 0;
	const VENDOR_STATUS_ONGOING 		= 1;
	
	const VENDOR_IMPORT_TYPE_DIRECT		= 'direct';
	const VENDOR_IMPORT_TYPE_LOCAL 		= 'local';	
	const VENDOR_IMPORT_TYPE_HTTP 		= 'http';
	const VENDOR_IMPORT_TYPE_FTP 		= 'ftp';
	const VENDOR_IMPORT_TYPE_SSH 		= 'ssh';
		
	const VENDOR_FILE_EXTENSION_ZIP		= 'zip';
	
	const VENDOR_FILE_TYPE_TXT			= 'txt';
	const VENDOR_FILE_TYPE_CSV			= 'csv';
	const VENDOR_FILE_TYPE_XLS			= 'xls';
	const VENDOR_FILE_TYPE_XML			= 'xml';
	const VENDOR_FILE_TYPE_POSITIONAL	= 'positional';
	
	const PRODUCT_PRICE_DECIMAL			= 2;
	
	protected $_eventPrefix = 'tunda_vendors_import';
	protected $_localPath;
	protected $_resource;	
	protected $_readConn;
	protected $_logFile;
	protected $_today;
	protected $_prices;
	
	
	public function _construct()
	{
		$this->_localPath = Mage::getBaseDir('var').DS.'import'.DS.'vendors'.DS;		
		$this->_resource = Mage::getSingleton('core/resource');    
    	$this->_readConn = $this->_resource->getConnection('core_read');
    	$this->_writeConn = $this->_resource->getConnection('core_write');
    	$this->_logFile = date('Ymd').'_vendors.log';
    	$this->_today = date('Y-m-d');		
    	$this->_prices = array('cost', 'price', 'special_price', 'weight');		
	}

	
	public function process()
	{
		$params = array('import' => $this);		
		Mage::dispatchEvent($this->_eventPrefix.'_process_before', $params);				
		$this->_import();		
		Mage::dispatchEvent($this->_eventPrefix.'_process_after', $params);
		//$this->_sendMail();		
		$this->_reindexData();	
		return $this;					
	}
	
	
	protected function _import()
	{				
		foreach($this->getVendorCollection() as $vendor)
		{												
			$this->_checkMvFile($vendor)
					->setVendor($vendor)
					->_updateStatus(self::VENDOR_STATUS_ONGOING)
					->log("IMPORT VENDOR ".$this->getVendor()->getName())			
					->_downloadFile()
					->_parseFile()
					->_mapRows()
					->_importProducts()
					->_updateStatus(self::VENDOR_STATUS_UPDATED);							
			$this->log("FINE IMPORT VENDOR");
			$this->log("");						
		}				 		
		$this->_checkMvFile();
		return $this;
	}	
	
	
	public function getVendorCollection()
	{
		$collection = Mage::getModel('tunda_vendors/vendors_files')->getCollection();
		$collection->getSelect()->joinLeft(array('tv' => 'tunda_vendors'), 'main_table.entity_id = tv.entity_id',array('*'));		
		$collection->addFieldToFilter('main_table.file_enable', array('eq' => 1));
		$collection->addFieldToFilter('tv.enabled', array('eq' => 1));
		$collection->getSelect()->order(array('main_table.priority', 'main_table.name_file', 'main_table.entity_id'));
		//echo $collection->getSelect() . '<br /><br />';		 	
		return $collection;
	}
	
	
	protected function _downloadFile()
	{				
		$this->setLocalFile();
		$this->log("\tDOWNLOAD FILE ".$this->getVendor()->getNameFile());
		$folderName = $this->_manageFolder(Mage::helper('tunda_vendors')->formatFolderName($this->getVendor()->getName()));			
		switch ($this->getVendor()->getImportType())
		{
			case self::VENDOR_IMPORT_TYPE_HTTP :	
				break;
			case self::VENDOR_IMPORT_TYPE_FTP :
				$this->_getFtpFile();	
				break;						
			case self::VENDOR_IMPORT_TYPE_LOCAL :	
				$this->setLocalFile($this->_localPath . $folderName . DS . $this->getVendor()->getNameFile(). '.'. $this->getVendor()->getExtFile());					
				var_dump($this->getLocalFile());
				break;
			case self::VENDOR_IMPORT_TYPE_DIRECT :
				break;
		}
		$this->setProcessedFile($this->_localPath . $folderName . DS . 'processati' . DS .date('Ymdhis').'_'. $this->getVendor()->getNameFile().'.'. $this->getVendor()->getExtFile());			
		return $this;
	}
	
	
	protected function _parseFile()
	{			
		$this->log("\tPARSE FILE ".$this->getVendor()->getNameFile());
		if($this->getLocalFile())
		{
			if($this->getVendor()->getExtFile() == self::VENDOR_FILE_EXTENSION_ZIP)
			{
				//unzippo il file
			}			
			switch ($this->getVendor()->getFileType())
			{
				case self::VENDOR_FILE_TYPE_XML :	
					break;
				case self::VENDOR_FILE_TYPE_XLS :				
					break;										
				case self::VENDOR_FILE_TYPE_CSV :	
					$this->_parseCsvFile();				
					break;
				case self::VENDOR_FILE_TYPE_POSITIONAL :
					$this->_parsePositionalFile();				
					break;
			}		
		}
		return $this;
	}
	
	
	protected function _updateStatus( $status )
	{				
		$vendor = Mage::getModel('tunda_vendors/vendors')->load($this->getVendor()->getEntityId());						
		$vendor->setStatus($status)
			->save();
		
		return $this;
	}
	
	
	protected function _getFtpFile()
	{
		$vendor = $this->getVendor();
		$port = 22;		
		if($vendor->getFtpSpecialPort())
		{
			$port = $vendor->getFtpSpecialPort();
		}				
		$connection = ftp_connect($vendor->getAddress(), $port);
		$loginResult = ftp_login($connection, $vendor->getUsername(), $vendor->getPassword());
		if ((!$connection) || (!$loginResult))
		{
			Mage::throwException("FTP Connection Failed");
		}	 
		if ($vendor->getChdir())
		{		
			if (ftp_chdir($connection, $vendor->getChdir()) == false) {
	            Mage::throwException('Change Dir Failed:'. $vendor->getChdir());	         
	        } 				
		}
		ftp_pasv($connection, true);		
		$folderName = $this->_manageFolder(Mage::helper('tunda_vendors')->formatFolderName($vendor->getName()));
		$this->setLocalFile($this->_localPath . $folderName . DS . $vendor->getNameFile(). '.'. $vendor->getExtFile());		
		if(!ftp_get($connection, $this->getLocalFile(), $vendor->getNameFile().".".$vendor->getExtFile(), FTP_BINARY))
		{
			Mage::throwException('There was problem to download file :'. $vendor->getNameFile().".".$vendor->getExtFile());
		}
		return $this;		
	}
	
	
	protected function _manageFolder( $folderName )
	{
		if(!file_exists($this->_localPath . $folderName))
		{			
			mkdir($this->_localPath . $folderName, 0777, true);
		}
		
		return $folderName;
	}
	
	
	protected function _parseCsvFile()
	{					
		$fileContent = '';
		$lines = null;
		$rows = null;			
		if (file_exists($this->getLocalFile()))
		{
			$fp = fopen($this->getLocalFile(), 'r');
			if ($fp){
				while(!feof($fp)) 
				{
					$fileContent.= fgets($fp, 4096);
				}
				fclose($fp);									
				$lines = preg_split('~'.$this->getVendor()->getRowDelimiter().'~', $fileContent);
				foreach($lines as $row)
				{
					if($this->getVendor()->getSkipFirstline())
					{
						$this->getVendor()->setSkipFirstline(0);
						continue;						
					}
					$row = preg_split('~'.$this->getVendor()->getFieldDelimiter().'~', $row);
					$rows[] = $row;					
				}
				$this->setRows($rows);
			}
		}
	}	
	

	protected function _parsePositionalFile()
	{
	
	$fileContent = '';
		$lines = null;
		$rows = null;					
		if (file_exists($this->getLocalFile()))
		{
			$fp = fopen($this->getLocalFile(), 'r');
			if ($fp){
				while(!feof($fp)) 
				{
					$fileContent.= fgets($fp, 4096);
				}
				fclose($fp);									
				$lines = preg_split('~'.$this->getVendor()->getRowDelimiter().'~', $fileContent);				
				$this->setRows($lines);
			}
		}
	}
	
	
	protected function _mapRows()
	{			
		$this->log("\tMAP ROWS");	
		$products = null;
		$product = null;
		$this->setProducts();
		if($this->getRows())
		{					
			if($map = $this->_getMap())
			{								
							
				foreach ($this->getRows() as $row)
				{	
					if($this->getVendor()->getFileType() == self::VENDOR_FILE_TYPE_POSITIONAL)
					{	
						if(strlen($row)> 0 )
						{
							$startPosition = 0;
							if($this->getVendor()->getStartPosition() > 0)
							{
								$startPosition = $this->getVendor()->getStartPosition();
							}		
							
							foreach ($map as $mapCol)
							{																																
								$product[$mapCol['name']] = trim(substr($row, ($mapCol['position']-$startPosition), $mapCol['lenght']));							
							}
						}
					}	
					else 
					{
						if(count($row) > 1)
						{
							foreach ($map as $mapCol)
							{				
								$product[$mapCol['name']] = $row[$mapCol['column_id']]; 										
							}					
						}		
					}	

					$product['entity_id'] = $this->getVendor()->getEntityId();
					$product['file_id'] = $this->getVendor()->getFileId();
					$product["product_type"] = $this->getVendor()->getFileContentType();			
					$product["store"] = $this->getVendor()->getStore();
					
					$product = $this->_dbConvertVarchar($product);
					$product = $this->_dbConvertPrices($product);	

				}							
				$this->setProducts($products);
			}
		}
		//var_dump($products->getData());
		return $this;
	}
	
	
	protected function _getMap()
	{
		$query = '	SELECT * 
					FROM tunda_vendors_files_schema
					WHERE file_id = '.$this->getVendor()->getFileId();
		    
    	$results = $this->_readConn->fetchAll($query);    	
    	return $results;    	
	}
	
	
	protected function _importProducts()
	{		
		$this->log("\tIMPORT ITEMS");
		
		if($this->getProducts())
		{
			foreach($this->getProducts() as $product)
			{				
				$_product = Mage::getModel('tunda_vendors/products')
								->setEntityId($this->getVendor()->getEntityId())
								->setStore($this->getVendor()->getStore())
								->setFileId($this->getVendor()->getFileId())
								->loadBySku($product->getVendorSku());		
								
				$data = $product->getData() + $_product->getData();
				
				$_product->setData($data);						
				$_product->save();		
				
			}	
		}
		return $this;			
	}
	
	
	protected function _reindexData()
	{
		if($this->getHasToReindex())
		{	
			//die('reindicizzo');
			$this->log("");
			//$this->log("REINDEX");					
			//$indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection(); 
			//foreach ($indexingProcesses as $process) {
			//      $process->reindexEverything();
			//}		
		}
	}
	
	
	protected function _checkMvFile( $vendor = null )
	{
		$canMove = false;
		if($this->getLocalFile())
		{
			if(($this->getVendor()) && ($vendor))
			{		
				if($vendor->getNameFile() != $this->getVendor()->getNameFile())
				{
					$canMove = true;								
				}
			}		
			elseif(($this->getVendor()) && (!$vendor))
			{
				$canMove = true;		
			}
			if($canMove)
			{
				if (copy($this->getLocalFile(), $this->getProcessedFile())) {
						unlink($this->getLocalFile());
					}
			}
		}
		return $this;
	}
	
	
	public function log( $message )
	{		
		Mage::log($message, null, $this->_logFile, true);
		return $this;
	}
	
	
	protected function _sendMail()
	{
		$body = "
			    	<h1>Importazione Eseguita</h1>
			    	<br />".$this->_eventPrefix."</br />
			    	";	           
        $mail = new Zend_Mail();   
	    $mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), Mage::getStoreConfig('trans_email/ident_general/name'))	     	    	
	    	->addTo("m.camoni@tunda.it", "m.camoni@tunda.it")
	    	->addBcc("f.delucia@tunda.it", "f.delucia@tunda.it")   
	    	//->addBcc(Mage::getStoreConfig('trans_email/ident_support/email'), Mage::getStoreConfig('trans_email/ident_support/name'))
	    	->setSubject('Import '.$this->_eventPrefix)	    	
	     	->setBodyHtml($body);	    
		try {
			$mail->send();			
		}
		catch (Exception $e) 
		{
			Mage::getSingleton('core/session')->addError('Unable to send email.');			
		}
		return $this;
	}
	
	
	protected function _dbConvertPrices( $product )
	{			
		foreach($this->_prices as $keyPrice)
		{
			if(array_key_exists($keyPrice, $product))
			{
				if($this->getVendor()->getFileType() == self::VENDOR_FILE_TYPE_POSITIONAL)
				{
					$int = (int)substr($product[$keyPrice],0 , - self::PRODUCT_PRICE_DECIMAL);
					$decimal = (int)substr($product[$keyPrice],- self::PRODUCT_PRICE_DECIMAL);
					$product[$keyPrice] = $int.'.'.$decimal;
				}
				$product[$keyPrice] = str_replace(',', '.', $product[$keyPrice]);
			}					
		}		
		return $product;
	} 
	
	protected function _dbConvertVarchar( $product )
	{	
		$newProduct = array();	
		$exclude = array('vendor_sku', 'manufacturer_sku', 'product_type', 'description', 'short_description');		
		foreach($product as $key => $value)
		{
			if(!in_array($key, $exclude))
			{
				$newProduct[$key] = ucwords(strtolower($value));
			}
			else
			{
				$newProduct[$key] = $value;
			}					
		}		
		return $newProduct;
	} 
	
}
