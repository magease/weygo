<?php
class Tunda_Vendors_Model_Import_Products extends Tunda_Vendors_Model_Import
{
	
	protected $_eventPrefix = 'tunda_vendors_import_products';
	protected $_excludeAttributeOnEdit = array();
	
	const FILE_CONTENT_TYPE_SIMPLE  		= 'simple';
	const FILE_CONTENT_TYPE_CONFIGURABLE	= 'configurable';
	const FILE_CONTENT_TYPE_STOCK 			= 'stock';
	const FILE_CONTENT_TYPE_PRICES 			= 'prices'; 
	
	const PRODUCT_ENTITY_TYPE_ID			= 4;
	const PRODUCT_ATTRIBUTE_SET_DEFAULT 	= 4;
	const PRODUCT_STATUS					= 1;
	const PRODUCT_TYPE_CONFIGURABLE 		= 'configurable';
	const PRODUCT_TYPE_SIMPLE 				= 'simple';	
	const PRODUCT_VISIBILITY_NOT_VISIBLE	= 1;
	const PRODUCT_VISIBILITY_CATALOG		= 2;
	const PRODUCT_VISIBILITY_SEARCH			= 2;
	const PRODUCT_VISIBILITY_CATALOG_SEARCH = 4;
	const PRODUCT_TAX_CLASS_ID				= 2;
	const PRODUCT_CONFIGURABLE_ATTRIBUTE	= 'size';
	const PRODUCT_IMPORTED					= 1;		
	const PRODUCT_NOT_IMPORTED					= 0;
	
	const ATTRIBUTE_INPUT_TYPE_SELECT		= 'select';
	const ATTRIBUTE_INPUT_TYPE_MULTISELECT	= 'multiselect';
	
	
	public function getVendorCollection()
	{
		$collection = parent::getVendorCollection();
		$collection->addFieldToFilter('main_table.file_content_type',
						array(
							array('eq' => self::FILE_CONTENT_TYPE_SIMPLE),
							array('eq' => self::FILE_CONTENT_TYPE_CONFIGURABLE),
							)
						);		
		return $collection; 
	}
	
	protected function _import()
	{	
		
		parent::_import();	
				
		$this->log('IMPORT PRODUCTS');	
		foreach($this->getVendorCollection() as $vendor)
		{
						
			$this->setVendor($vendor);
			$this->_updateFileStatus(self::VENDOR_STATUS_ONGOING);
			
			$this->_checkPoint();
			
			foreach($this->getProductsCollection() as $item)
			{				
				$this->log("\tMANAGE PRODUCTS");												
					
				$this->setItem($item);
				
				foreach($this->_getStoreIds() as $storeId)
				{		
					$this->setStoreId($storeId);					
					$this->log("\t\tSTORE ". $this->getStoreId());												
					$this->unsProduct();					
					$this->setProduct($this->loadProduct());	
					
					$attributes = $item->getData();
					
					if(!$this->getProduct())
					{					
						$this->setProduct(Mage::getModel('catalog/product'));
						$this->_initProduct();
						$this->log("\t\tADD NEW PRODUCT ".$this->getProduct()->getSku());
					}else{
						$this->log("\t\tEDIT PRODUCT ".$this->getProduct()->getSku());																		
					}												
					$this->getProduct()->setStoreId($this->getStoreId());	
					
					
					$attributes = $this->_manageAttributes($attributes);					
					$data = $attributes + $this->getProduct()->getData();	

					
					$this->getProduct()->setData($data);	
														
					$this->_managePrices();
					
					$this->getProduct()->setUrlKey($this->getProduct()->getName()."-".$this->getProduct()->getSku()."-".$this->getStoreId());					
					$this->getProduct()->save();						
				}
				$this->_manageCatalogInventory();
				$this->_manageConfigurable();
				$this->_manageRelated();
				$this->_manageUpsells();				
				
				$this->updateItem($item, self::PRODUCT_IMPORTED);							
			}	
			$this->_updateFileStatus(self::VENDOR_STATUS_UPDATED);		
		}		
		$this->setHasToReindex(1);
	
		$this->log('FINE IMPORT PRODUCTS');
		$this->log("");
	}
	
	
	public function getProductsCollection()
	{
		$collection = Mage::getModel('tunda_vendors/products')->getCollection();
		if($this->getVendor()->getImportType() != self::VENDOR_IMPORT_TYPE_DIRECT)
		{
			$collection->addFieldToSelect('product_id');
			$collection->addFieldToSelect('store');
			$collection->addFieldToSelect('product_type');
			$collection->addFieldToSelect('imported');
			$collection->addFieldToFilter('entity_id', array('eq' => $this->getVendor()->getEntityId()));
			$collection->addFieldToFilter('file_id', array('eq' => $this->getVendor()->getFileId()));
			if($this->getCheckPoint())
			{
				$collection->addFieldToFilter('imported', array('lt' => $this->getCheckPoint()));
			}	
			
			foreach($this->_getMap() as $map)
			{
				$collection->addFieldToSelect($map['name']);
			}		
		}		
		$collection->addFieldToFilter('imported', array('eq' => 0));
		$collection->addFieldToFilter('product_type', array('eq' => 'simple'));
		
		
		$collection->getSelect()->where("vendor_sku NOT IN (SELECT sku from catalog_product_entity)");
		
		$collection->getSelect()->order('file_id DESC');
		echo "\n".$collection->getSelect()."\n";				
		//die();
		return $collection;
	}
	
	
	public function loadProduct()
	{
		$product = null;
		if($item = $this->getItem())
		{						
			$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getVendorSku());
			if((!$product) && ($item->getManufacturerSku()))
			{				
				$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getManufacturerSku());
			}
			//if((!$product) && ($item->getEan()))
			//{			
			//	$product = Mage::getModel('catalog/product')->loadByAttribute('ean', $item->getEan());
			//}
			if($product)
			{
				$product = Mage::getModel('catalog/product')->load($product->getEntityId());
			}
			return $product;
		}
		else{
			return null;
			$this->log("\t\tNO ITEM");
		}
	}
	
	
	protected function _initProduct()
	{
		$this->getProduct()->setSku($this->getItem()->getVendorSku());		
		$this->getProduct()->setTypeId($this->getItem()->getProductType());
		$this->getProduct()->setAttributeSetId(self::PRODUCT_ATTRIBUTE_SET_DEFAULT);
		$this->getProduct()->setStatus(self::PRODUCT_STATUS);
		$this->getProduct()->setTaxClassId(self::PRODUCT_TAX_CLASS_ID);
		$this->getProduct()->setVisibility(self::PRODUCT_VISIBILITY_CATALOG_SEARCH);		
		//$this->getProduct()->setWebsiteIds($this->_getWebsiteIds());
		$this->getProduct()->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
		$this->getProduct()->setPrice(1);
		
		if($this->getProduct()->getTypeId() == self::PRODUCT_TYPE_CONFIGURABLE)
		{
			$this->getProduct()->setHasOptions(1);
			$this->getProduct()->setRequiredOptions(1);			
		}			
	}
	

	protected function _manageAttributes($attributes)
	{					
		$_return_array=array();
		foreach($attributes as $code => $value)
		{			
			if(!empty($value))
			{	
				$attribute = Mage::getModel('eav/entity_attribute')->loadByCode(self::PRODUCT_ENTITY_TYPE_ID, $code);
				if($attribute->getAttributeId())
				{
					switch ($attribute->getFrontendInput()) 
					{
						case $attribute->getFrontendInput() == self::ATTRIBUTE_INPUT_TYPE_SELECT:
								$attributeValue = $this->_attributeValueExists($code, $value);
								if(!$attributeValue)
								{
									$attributeValue = $this->_addAttributeValue($code, $value);
								}
								$_return_array[$code] = $attributeValue;
						break;						
						case $attribute->getFrontendInput() == self::ATTRIBUTE_INPUT_TYPE_MULTISELECT:
							$attributeValues = array();
								foreach (explode(',', $value) as $val)
								{
									$attributeValue = $this->_attributeValueExists($code, trim($val));
									if(!$attributeValue)
									{
										$attributeValue = $this->_addAttributeValue($code, trim($val));
									}
									$attributeValues[] = $attributeValue;
								}
								$_return_array[$code] = $attributeValues;
						break;
						default:
							$_return_array[$code] = $value;
						break;
					}					
				}	
			}
		}	
		return $_return_array;			
	}
	
	
	protected function _addAttributeValue($arg_attribute, $arg_value)
    {        
        $attribute_model        = Mage::getModel('eav/entity_attribute');
        $attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute              = $attribute_model->load($attribute_code);
                	
        $value['option'] = array($arg_value,$arg_value);
        $result = array('value' => $value);
        $attribute->setData('option',$result);
        $attribute->save();   

		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;
        $attribute_table        = $attribute_options_model->setAttribute($attribute);
        $options                = $attribute_options_model->getAllOptions(false);

        foreach($options as $option)
        {
            if ($option['label'] == $arg_value)
            {
                return $option['value'];
            }
        }
       return false;
    }
    
    
	protected function _attributeValueExists($arg_attribute, $arg_value)
    {
        $attribute_model        = Mage::getModel('eav/entity_attribute');
        $attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;

        $attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute              = $attribute_model->load($attribute_code);

        $attribute_table        = $attribute_options_model->setAttribute($attribute);
        $options                = $attribute_options_model->getAllOptions(false);
        
        foreach($options as $option)
        {
            if ($option['label'] == $arg_value)
            {
                return $option['value'];
            }
        }

        return false;
    }
    
    
    protected function _manageConfigurable()
    {    	
    	$this->log("\tMANAGE CONFIGURABLE PRODUCTS");    	
        if($this->getProduct()->getTypeId() == self::PRODUCT_TYPE_CONFIGURABLE)
		{   
			
			$superAttributes = array( Mage::getModel('eav/entity_attribute')
			  							->loadByCode('catalog_product',self::PRODUCT_CONFIGURABLE_ATTRIBUTE)
			  							->getData('attribute_id')
			  						);
			$cProduct = $this->getProduct();
	    	$cProductTypeInstance = $cProduct->getTypeInstance();
	    	if (!$cProductTypeInstance->getUsedProductAttributeIds()) 
	    	{ /* This is a new product without the Configurable Attribue Ids set */
	    		$cProductTypeInstance->setUsedProductAttributeIds($superAttributes);    			
	    	}
	    	$attributes_array = $cProductTypeInstance->getConfigurableAttributesAsArray();
	    	foreach($attributes_array as $key => $attribute_array) 
	    	{
			    $attributes_array[$key]['use_default'] = 1;
			    $attributes_array[$key]['position'] = 0;
			 
			    if (isset($attribute_array['frontend_label'])) {
			        $attributes_array[$key]['label'] = $attribute_array['frontend_label'];
			    }
			    else {
			        $attributes_array[$key]['label'] = $attribute_array['attribute_code'];
			    }
			}
			$cProduct->setConfigurableAttributesData($attributes_array);
			$cProduct->save();	 
		}
		else
		{			
			$cProduct = null;
			if($this->getItem()->getFatherSku())
			{
				$cProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getItem()->getFatherSku());
			}
			else
			{			
				$fathers =  Mage::getResourceSingleton('catalog/product_type_configurable')
	                  		->getParentIdsByChild($this->getProduct()->getEntityId());
				if(count($fathers) > 0)
				{					
					$cProduct = Mage::getModel('catalog/product')->load($fathers[0]);
				}                  		
			}    
			if($cProduct)
			{  		
				if($cProduct->getTypeId() == self::PRODUCT_TYPE_CONFIGURABLE)                 		     	    
		    	{	    			    			    	
		    		$cProductTypeInstance = $cProduct->getTypeInstance();	    				
			    	$dataArray[$this->getProduct()->getEntityId()] = array();		    	
			    	$childIds = $cProductTypeInstance->getChildrenIds($cProduct->getId());			                    
					foreach ($childIds as $childId)
					{
						foreach($childId as $id)
						{
							$dataArray[$id] = array();					
						}
					}	
			    	
					$attributes_array = $cProductTypeInstance->getConfigurableAttributesAsArray();				
			    	foreach($dataArray as $id => $values)
					{	    				
						foreach ($attributes_array as $attrArray) 
						{								
							array_push(
					            $dataArray[$id],
					            array(
					                'attribute_id' => $attrArray['attribute_id'],
					                'label' => $attrArray['label'],
					                'is_percent' => false,
					                'pricing_value' => false
					            )
					       );
						}					
					}			
					// This tells Magento to associate the given simple products to this configurable product..
					$cProduct->setConfigurableProductsData($dataArray);								
					$cProduct->save();
												
		    		$fatherStock = 0;
		    		$fatherIsInStock = 0;    		
		    		$children = $cProductTypeInstance->getUsedProducts(null,$cProduct);			                    
					foreach ($children as $child)
					{				
						if($child->getStockItem())
						{														
							$fatherStock+= $child->getStockItem()->getQty();					
						}				
					}					
					if($fatherStock > 0)
					{
						$this->log("\t\tCONFIGURABLE PRODUCT ". $cProduct->getSku()." IS IN STOCK");
						$fatherIsInStock = 1;
					}else{
						$this->log("\t\tCONFIGURABLE PRODUCT ". $cProduct->getSku()." IS OUT OF STOCK");	
					}			
					$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($cProduct->getEntityId());
					//var_dump($cProduct->getEntityId(), $stock->getData());
					//die('   MANNAGGIA IL CONFIGURABLE    ');
					$stock->setData('is_in_stock', $fatherIsInStock);		
					$stock->save();
			 	}   
			} 	
			
		}
    }
    
    
	protected function _manageCatalogInventory()
	{		
		
		$this->log("\tMANAGE CATALOG INVENTORY");		
		//$stock = $this->getProduct()->getStockItem();	
		$this->log("\t".$this->getProduct()->getVendorSku());
		$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($this->getProduct()->getEntityId());	
		//var_dump(count($stock->getData()));
		if(count($stock->getData()) == 0)
		{		
			$stock->setData('manage_stock',1);
		    $stock->setData('is_in_stock',0);
		    $stock->setData('use_config_manage_stock', 0);
		    $stock->setData('stock_id',1);
		    $stock->setData('product_id',$this->getProduct()->getEntityId());
		    $stock->setData('qty',0);
		    $stock->save();
			$this->getProduct()->save();			
		}
				
/*		
 				var_dump(count($stock->getData()));					
				echo '------>';
				var_dump($this->getProduct()->getEntityId(), $stock->getData());
				echo '<------';
				$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($this->getProduct()->getEntityId());
				var_dump($this->getProduct()->getEntityId(), $stock->getData());
				die('   NON VA LO STOCK????    ');
		
*/		
		if($this->getProduct()->getTypeId() != self::PRODUCT_TYPE_CONFIGURABLE)
		{						
			if($qty = $this->getItem()->getQty()) 
			{												
				$vendorName = Mage::helper('tunda_vendors')->formatDbName($this->getVendor()->getName());			
				/* DA TOGLIERE DA QUA E FARE NELL'INSERT DI UN NUOVO VENDOR 
				
				if(!$stock->getData('qty_'.$vendorName))
				{	
							
					$this->_writeConn->addColumn('cataloginventory_stock_item', 'qty_'.$vendorName, 
						array(			        
					        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
					        'length' => '12,4',
							'default' => '0',	
							'comment' => 'qty for vendor '.$this->getVendor()->getName(),	        			       
					    )
					);
					
					$this->_writeConn->addColumn('cataloginventory_stock_status', 'qty_'.$vendorName, 
						array(			        
					        'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
					        'length' => '12,4',
							'default' => '0',
							'comment' => 'qty for vendor '.$this->getVendor()->getName(),			        			       
					    )
					);
				}		
						
															
				$stock->setData('qty_'.$vendorName, $qty);
				/* FINE */
				
				
				/* SUM */			
				//$qty = $stock->getQty() + $stock->getData('qty_'.$vendorName);							
				$stock->setQty($qty); 
				if($qty > 0)
				{
					$stock->setIsInStock(1);
					$this->log("\t\tPRODUCT ". $this->getProduct()->getSku()." HAS QTY = ".$qty." IS IN STOCK");	
				}
				else
				{
					$this->log("\t\tPRODUCT ". $this->getProduct()->getSku()." HAS QTY = ".$qty." IS OUT OF STOCK");
					$stock->setIsInStock(0);
				}	
				$stock->save();				
			}													
		}
	}
	
	
	protected function _checkPoint()
	{	
		$today = strtotime($this->_today);
		$updateAt = strtotime(date('Y-m-d', strtotime($this->getVendor()->getLastImportStart())));		
		$this->setCheckPoint(1);			
		if(($today - $updateAt) > 0)
		{						
			$this->setCheckPoint(0);
			foreach($this->getProductsCollection() as $item)
			{
				
				$this->updateItem($item, self::PRODUCT_NOT_IMPORTED);
			}			
		}
	}
	
	protected function _updateFileStatus( $status )
	{				
		parent::_updateStatus( $status );		
		$file = Mage::getModel('tunda_vendors/vendors_files')->load($this->getVendor()->getFileId());		
		if($status == self::VENDOR_STATUS_ONGOING)
		{	
			$file->setLastImportStart(date('Y-m-d H:i:s'));
		}
		if($status == self::VENDOR_STATUS_UPDATED)
		{				
			$file->setLastImportEnd(date('Y-m-d H:i:s'));
		}
		$file->save();
		
		return $this;
	}
	
	public function updateItem( $item, $value )
	{		
		if($item)
		{
			$item->setImported( $value );
			$item->save();
		}
	}
	
	protected function _manageRelated()
	{
		if($this->getItem()->getProductType() == self::PRODUCT_TYPE_CONFIGURABLE)
		{													
			$this->setProduct($this->loadProduct());
			$this->_assignRelated();					
		}
	}
	
	
	protected function _assignRelated()
	{
		$params = array();
		$this->getProduct()->setRelatedLinkData($params)->save();
		$position = 0; 
		foreach($this->_getSimilarProducts() as $related)
		{
			$params[$related->getEntityId()] = array(
					'position'     => $position
			);
			$position++;
		}
		$this->getProduct()->setRelatedLinkData($params)->save(); 		
	}
	
	
	protected function _manageUpsells()
	{
		if($this->getItem()->getProductType() == self::PRODUCT_TYPE_CONFIGURABLE)
		{
			$this->setProduct($this->loadProduct());
			$this->_assignUpsells();
		}
	}
	
	
	protected function _assignUpsells()
	{
		$params = array();
		$this->getProduct()->setUpSellLinkData($params)->save();
		$position = 0;
		foreach($this->_getSimilarProducts() as $upsell)
		{
			$params[$upsell->getEntityId()] = array(
					'position'     => $position
			);
			$position++;
		}
		$this->getProduct()->setUpSellLinkData($params)->save();
	}
	
	
	
	protected function _getSimilarProducts()
	{
		$skuLike = substr($this->getProduct()->getSku(), 0, -4);
		$collection = Mage::getModel('catalog/product')->getCollection();		
		$collection->addAttributeToFilter('sku', array('like' => $skuLike.'%'));
		$collection->addAttributeToFilter('sku', array('neq' => $this->getProduct()->getSku()));
		$collection->addAttributeToFilter('type_id', array('eq' => Tunda_Vendors_Model_Import_Products::PRODUCT_TYPE_CONFIGURABLE));		
		return $collection;
	}

	
	protected function _managePrices()    
	{						
		$this->_roundPrices();			
    }
	
	
	protected function _roundPrices()
    {    	
    	foreach($this->_prices as $keyPrice)
		{
			
			if(array_key_exists($keyPrice, $this->getProduct()->getData()))
			{
				$value = sprintf("%01.4f", $this->getProduct()->getData($keyPrice));
				$this->getProduct()->setData($keyPrice, $value);
				$this->log("\t\tPRODUCT ". $this->getProduct()->getSku()." -- HAS ".strtoupper($keyPrice)." = ".$value);
			}
		}		
    }
    
    protected function _getStoreIds()
    {
    	$stores = array();
    	if($this->getItem()->getStore() == '*')
    	{
    		$_stores = Mage::app()->getStores();
    		$stores[] = 0;
			foreach ($_stores as $_eachStoreId => $val)
			{				
				$stores[] = Mage::app()->getStore($_eachStoreId)->getId();
			}			
    	}
    	else 
    	{
    		$stores = explode(';', $this->getItem()->getStore());
    	}
    	return $stores;
    }
    
    protected function _getWebsiteIds()
    {
    	$_websites = Mage::app()->getWebsites();
    	$wids = array(); 
    	foreach($_websites as $website)
    	{     
    		$wids[] = $website->getId();
    	}
    	return $wids;
    }
}
