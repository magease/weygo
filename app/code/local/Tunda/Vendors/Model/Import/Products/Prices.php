<?php
class Tunda_Vendors_Model_Import_Products_Prices extends Tunda_Vendors_Model_Import_Products
{	
	public function getVendorCollection()
	{
		$collection = Tunda_Vendors_Model_Import::getVendorCollection();
		$collection->addFieldToFilter('main_table.file_content_type', array('eq' => self::FILE_CONTENT_TYPE_PRICES));
		return $collection; 
	}
	
	
	protected function _import()
	{		
		Tunda_Vendors_Model_Import::_import();					
		$this->log('IMPORT PRICES');	
		foreach($this->getVendorCollection() as $vendor)
		{
			$this->setVendor($vendor);
			$this->_updateFileStatus(self::VENDOR_STATUS_ONGOING);

			$this->_checkPoint();
			
			foreach($this->getProductsCollection() as $item)
			{	
				$this->setItem($item);
				foreach($this->_getStoreIds() as $storeId)
				{		
					$this->setStoreId($storeId);					
					$this->log("\t\tSTORE ". $this->getStoreId());												
					$this->unsProduct();					
					$this->setProduct($this->loadProduct());		
					
					if ($this->getProduct()){
						$this->log("\t\t ITEM ".$this->getProduct()->getSku());
						$this->getProduct()->setStoreId($this->getStoreId());		
						$this->_managePrices();
						$this->getProduct()->setUrlKey($this->getProduct()->getName()."-".$this->getStoreId());																	
						$this->getProduct()->save();
					}
					else
						$this->log("\t\t SKIP!" . $item->getManufacturerSku());
						
				}
				$this->updateItem($item, self::PRODUCT_IMPORTED);							
			}
			$this->_updateFileStatus(self::VENDOR_STATUS_UPDATED);
		}		
		$this->setHasToReindex(1);
		$this->log('FINE IMPORT PRICES');
		$this->log("");
	}	
}