<?php
class Tunda_Vendors_Model_Import_Products_Stock extends Tunda_Vendors_Model_Import_Products
{	
	public function getVendorCollection()
	{
		$collection = Tunda_Vendors_Model_Import::getVendorCollection();
		$collection->addFieldToFilter('main_table.file_content_type', array('eq' => self::FILE_CONTENT_TYPE_STOCK));	
		return $collection; 
	}
	
	protected function _import()
	{							
		Tunda_Vendors_Model_Import::_import();						
		$this->log('IMPORT PRODUCTS');	
		foreach($this->getVendorCollection() as $vendor)
		{						
			$this->setVendor($vendor);
			$this->_updateFileStatus(self::VENDOR_STATUS_ONGOING);
			
			foreach($this->getProductsCollection() as $item)
			{				
				$this->unsProduct();
				$this->setItem($item);					
				$this->setProduct($this->loadProduct());			
				
				$this->_manageCatalogInventory();
				$this->_manageConfigurable();													
			}
			$this->_updateFileStatus(self::VENDOR_STATUS_UPDATED);
		}		
		$this->setHasToReindex(1);
		$this->log('FINE IMPORT PRODUCTS');		
		$this->log("");
	}
	
	
	protected function _manageConfigurable()
    {
    	$this->log("\tMANAGE CONFIGURABLE PRODUCTS");    	
        if($this->getProduct()->getTypeId() != self::PRODUCT_TYPE_CONFIGURABLE)
        {
			$fateher_sku = null;
			if($this->getItem()->getFatherSku())
			{
				$cProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getItem()->getFatherSku());
			}
			else
			{			
				$fathers =  Mage::getResourceSingleton('catalog/product_type_configurable')
	                  		->getParentIdsByChild($this->getProduct()->getEntityId());
				if(count($fathers) > 0)
				{					
					$cProduct = Mage::getModel('catalog/product')->load($fathers[0]);
				}                  		
			}      		
			if($cProduct)                  		     	    
	    	{		    		    		    			    	
	    		$cProductTypeInstance = $cProduct->getTypeInstance();
	    		
	    		$fatherStock = 0;
	    		$fatherIsInStock = 0;    		
	    		$children = $cProductTypeInstance->getUsedProducts(null,$cProduct);			                    
				foreach ($children as $child)
				{				
					if($child->getStockItem())
					{														
						$fatherStock+= $child->getStockItem()->getQty();					
					}				
				}					
				if($fatherStock > 0)
				{
					$this->log("\t\tCONFIGURABLE PRODUCT ". $cProduct->getSku()." IS OUT OF STOCK");
					$fatherIsInStock = 1;
				}else{
					$this->log("\t\tCONFIGURABLE PRODUCT ". $cProduct->getSku()." IS OUT OF STOCK");	
				}			
				$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($cProduct->getEntityId());
				$stock->setData('is_in_stock', $fatherIsInStock);		
				$stock->save();
		 	}    	
		}
    }
}