<?php 
class Tunda_Vendors_Model_Resource_Vendors
extends Mage_Core_Model_Resource_Db_Abstract
{
	public function _construct()
    {       	    	
    	$this->_init('tunda_vendors/vendors', 'entity_id');
    }
    
 	protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {    	        	
    	parent::_beforeSave($object);
    	$object->setUpdatedAt(date('Y-m-d H:i:s'));    	
    	return $this;    	
    }
}