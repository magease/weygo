<?php
class Tunda_Vendors_Model_Resource_Vendors_Files_Collection 
extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{  		
    	$this->_init('tunda_vendors/vendors_files');
    }        
}   