<?php
class Tunda_Vendors_Model_Vendors extends Mage_Core_Model_Abstract
{
	public function _construct()
	{		
		parent::_construct();
		$this->_init('tunda_vendors/vendors');
	}
}