<?php
class Tunda_Vendors_Model_Products extends Mage_Core_Model_Abstract
{
	public function _construct()
	{		
		parent::_construct();
		$this->_init('tunda_vendors/products');
	}
	
	public function loadBySku( $sku )
	{
		$return = $this;
		if(is_null($this->getEntityId()))
		{
			Mage::throwException(Mage::helper('tunda_vendors')->__('entity_id is not set.'));
		}
		if(is_null($this->getStore()))
		{
			Mage::throwException(Mage::helper('tunda_vendors')->__('store is not set.'));
		}
		if(is_null($this->getFileId()))
		{
			Mage::throwException(Mage::helper('tunda_vendors')->__('file id is not set.'));
		}
		$collection = $this->getCollection();
		$collection->addFieldToFilter('entity_id', array('eq' => $this->getEntityId()));
		$collection->addFieldToFilter('file_id',array('eq' => $this->getFileId()));
		$collection->addFieldToFilter('store',array('eq' => $this->getStore()));
		$collection->addFieldToFilter(array('vendor_sku','manufacturer_sku'), array($sku, $sku));						
		$return = $collection->getFirstItem();
		
		return $return;
	}
}