<?php
/*
$installer = $this;
$installer->startSetup();
$installer->run("	
	CREATE TABLE tunda_vendors (
	  entity_id int(11) NOT NULL AUTO_INCREMENT,
	  name varchar(50) DEFAULT NULL,
	  description varchar(50) DEFAULT NULL,
	  enabled tinyint(1) NOT NULL,
	  status int(10) DEFAULT NULL,
	  created_at datetime DEFAULT NULL,
	  updated_at datetime DEFAULT NULL,
	  last_import_start datetime DEFAULT NULL,
	  last_import_end datetime DEFAULT NULL,
	  active_products int(11) DEFAULT NULL,
	  PRIMARY KEY (entity_id)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");

$installer->run("	
	CREATE TABLE tunda_vendors_files (
	  file_id int(11) NOT NULL AUTO_INCREMENT,
	  entity_id int(11) NOT NULL,
	  file_enable tinyint(1) NOT NULL,
	  import_type varchar(10) DEFAULT NULL,
	  address varchar(200) DEFAULT NULL,
	  name_file varchar(50) DEFAULT NULL,
	  ext_file varchar(4) DEFAULT NULL,
	  username varchar(50) DEFAULT NULL,
	  password varchar(50) DEFAULT NULL,
	  field_delimiter varchar(6) DEFAULT NULL,
	  row_delimiter varchar(6) DEFAULT NULL,
	  field_container varchar(1) DEFAULT NULL,
	  skip_firstline tinyint(1) DEFAULT NULL,
	  file_type varchar(30) DEFAULT NULL,
	  file_content_type varchar(30) DEFAULT NULL,
	  chdir varchar(30) DEFAULT NULL,
	  ftp_special_port varchar(30) DEFAULT NULL,
	  priority int(11) DEFAULT NULL,
	  xml_structure varchar(200) DEFAULT NULL,
	  serialfrom varchar(50) DEFAULT NULL,
	  serialto varchar(50) DEFAULT NULL,
	  seriallast varchar(50) DEFAULT NULL,
	  PRIMARY KEY (file_id,entity_id)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");	
	
$installer->run("	
	CREATE TABLE tunda_vendors_files_schema (
	  file_schema_id int(11) NOT NULL AUTO_INCREMENT,
	  file_id int(11) NOT NULL,
	  name varchar(50) NOT NULL,
	  column_id varchar(50) NOT NULL,
	  column_type varchar(10) DEFAULT NULL,
	  eval_action varchar(100) DEFAULT NULL,
	  column_id_obj varchar(50) DEFAULT NULL,
	  position varchar(50) DEFAULT NULL,
	  lenght int(5) DEFAULT NULL,
	  PRIMARY KEY (file_schema_id,file_id)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");

$installer->run("	
	CREATE TABLE tunda_vendors_products (
	  product_id int(11) NOT NULL AUTO_INCREMENT,
	  store	varchar(2) NOT NULL,
	  file_id int(11) NOT NULL,
	  product_type varchar(20) NOT NULL,			
	  entity_id int(11) NOT NULL,
	  vendor_sku varchar(30) NOT NULL,
	  manufacturer_sku varchar(30) DEFAULT NULL,
	  ean varchar(14) DEFAULT NULL,
	  name varchar(255) DEFAULT NULL,
	  short_description varchar(255) DEFAULT NULL,
	  description varchar(255) DEFAULT NULL,
	  price decimal(18,4) DEFAULT NULL,
	  special_price decimal(18,4) DEFAULT NULL,
	  qty int(10) DEFAULT NULL,
	  qty_arrival int(10) DEFAULT NULL,
	  qty_arrival_from datetime DEFAULT NULL,
	  weight decimal(18,4) DEFAULT NULL,18,4
	  product_url varchar(100) DEFAULT NULL,
	  imported tinyint(4) NOT NULL DEFAULT '0',
	  state tinyint(4) DEFAULT NULL,
	  created_at datetime DEFAULT NULL,
	  update_at datetime DEFAULT NULL,
	  category varchar(100) DEFAULT NULL,
	  manufacturer varchar(30) DEFAULT NULL,
	  siae decimal(18,4) DEFAULT NULL,
	  raee decimal(18,4) DEFAULT NULL,
	  PRIMARY KEY (product_id)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
");

$installer->endSetup();

*/
