<?php
class Tunda_Vendors_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function formatFolderName( $name )
	{		
		$name = ucwords(strtolower($name));
		$name = str_replace('-','', $name);
		$name = str_replace('_','', $name);
		$name = str_replace(' ','', $name);
		return $name; 
	}

	public function formatDbName( $name )
	{		
		$name = strtolower($name);
		$name = str_replace('-','', $name);
		$name = str_replace('_','', $name);
		$name = str_replace(' ','_', $name);
		return $name; 
	}	
}