<?php
class Tunda_Vendors_Block_Adminhtml_Vendors
extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {    	    	    	
    	parent::__construct();
    	$this->_blockGroup = 'tunda_vendors';
        $this->_controller = 'adminhtml_vendors';
        $this->_headerText = Mage::helper('tunda_vendors')->__('Vendors');       
        $this->_removeButton('add');                 
    }	
}
