<?php
class Tunda_Vendors_Block_Adminhtml_Vendors_Grid 
extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('vendors_grid');
        //$this->setDefaultSort('created_at');
        //$this->setDefaultDir('DESC');
        //$this->setSaveParametersInSession(true);				
		$this->setUseAjax(false);			
		$this->_controller = 'adminhtml_vendors';
	}


	protected function _prepareCollection()
    {
    	$collection = Mage::getModel('tunda_vendors/vendors')
    				->getCollection();
		/*    				  				
					->addExpressionFieldToSelect('fullname', '
						(
						SELECT CONCAT_WS(\' \', firstname_table.value, lastname_table.value)
						FROM customer_entity 
						LEFT JOIN customer_entity_varchar AS lastname_table
						ON lastname_table.entity_id = customer_entity.entity_id
						AND lastname_table.attribute_id = 7
						LEFT JOIN customer_entity_varchar AS firstname_table
    					ON firstname_table.entity_id = customer_entity.entity_id
      					AND firstname_table.attribute_id = 5       					
      					WHERE customer_entity.entity_id = main_table.customer_id
      	  				)
      	  				','fullname');		
    	$collection->getSelect()->joinLeft('customer_entity', 'main_table.customer_id = customer_entity.entity_id','email');
    	*/				    								
    				
    	$this->setCollection($collection);		
		return parent::_prepareCollection();
	}

 	
	protected function _prepareColumns()
	{
		/*
		$filter   = $this->getParam($this->getVarNameFilter(), null);
		$data = $this->helper('adminhtml')->prepareFilterString($filter);				
		
		$this->addColumn('entity_id', array(
            'header'=> Mage::helper('customerbrands')->__('#'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'entity_id',			
			'filter_index' => 'main_table.entity_id',
		));

		$this->addColumn('manufacturers_id', array(
            'header'	=> Mage::helper('customerbrands')->__('Brand Name'),
            'index' 	=> 'manufacturers_id',
			'type'  	=> 'options',
			'options' 	=> Mage::helper('customerbrands')->getManufactures(),
			'renderer'	=> 'Buyonz_CustomerBrands_Block_Adminhtml_Customerbrands_Grid_Renderer_Manufacturers',
        	'filter_index' => 'main_table.manufacturers_id',
		));		
		
		$this->addColumn('fullname', array(
            'header'    => Mage::helper('plus')->__('Customer Name'),
            'index'     => 'fullname',
			'renderer'	=> 'Buyonz_CustomerBrands_Block_Adminhtml_Customerbrands_Grid_Renderer_Customername',
			'filter_condition_callback' => array($this, '_customerNameCondition'),
		));
		
		$this->addColumn('email', array(
            'header'    => Mage::helper('plus')->__('Email'),
            'width'     => '200px',
            'index'     => 'email',
			'renderer'	=> 'Buyonz_CustomerBrands_Block_Adminhtml_Customerbrands_Grid_Renderer_Customeremail',			
		));				
		
		$this->addColumn('like_it', array(
            'header' 	=> Mage::helper('customerbrands')->__('Like'),
            'index' 	=> 'like_it',
			'type'  	=> 'options',
			'options' 	=> array( 0 => Mage::helper('adminhtml')->__('No'), 1 => Mage::helper('adminhtml')->__('Yes')),			
			'width' 	=> '80px',
        	'filter_index' => 'main_table.like_it',
		));
		
		$this->addColumn('created_at', array(
            'header' => Mage::helper('customerbrands')->__('Created At'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '150px',
			'filter_index' => 'main_table.created_at',
		));
		
		$this->addColumn('updated_at', array(
            'header' => Mage::helper('customerbrands')->__('Updated At'),
            'index' => 'updated_at',
            'type' => 'datetime',
            'width' => '150px',
			'filter_index' => 'main_table.updated_at',
		));				 
        
		return $this;
		*/
	}

	public function getRowUrl($row)
	{
		/* 
		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			return $this->getUrl('adminhtml/sales_order/view/' ,array('order_id' => $row->getId()));
		}
		*/
		return null;

	}

	public function getRowClass($row)
	{
		return '';
	}

	protected function _prepareMassaction()
	{
		return $this;
	}
}