<?php

class Tunda_CustomerOriginChooser_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * @return array
	 */
	public function getStoreConfigSettings()
	{
		return Mage::getStoreConfig('customer_origin_chooser/settings');
	}

	/**
	 * Checks if module has been activated in backend
	 *
	 * @return bool
	 */
	public function isModuleActive()
	{
		return (int)$this->getStoreConfigSettings()['active'];
	}

	public function getCapList()
	{
		return $capList = Mage::getModel('tunda_customeroriginchooser/cap')
		                      ->getCapList(true);
	}
	/**
	 * @return array
	 */
	public function getCapListAsOptionArray()
	{
		$capList = Mage::getModel('tunda_customeroriginchooser/cap')
			->getCapList(true);

		$list =[];
		$list[] = [
			'value'=>'',
			'label'=> '---'
		];

		foreach ($capList as $k => $v) {
			$list[] = [
				'value'=>$v,
			    'label'=>$v
			];
		}
		return $list;
	}

	public function checkCapFlag()
	{
		$flag = (bool)Mage::getModel('checkout/session')->getCustomerOriginChooserCap();
		return $flag;
	}


	public function getValidationUrl()
	{
		return $this->_getUrl('customeroriginchooser/validation/validate');
	}
}