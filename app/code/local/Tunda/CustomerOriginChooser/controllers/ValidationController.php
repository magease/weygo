<?php
class Tunda_CustomerOriginChooser_ValidationController extends Mage_Core_Controller_Front_Action
{

	public function validateAction()
	{
		$session =Mage::getSingleton('checkout/session');
		if ($this->getRequest()->isPost()) {
			$session->setCustomerOriginChooserCap(false);

			$cap = $this->getRequest()->getParam('cap');
			$isValid = Mage::getModel('tunda_customeroriginchooser/cap')->validate($cap);

			if ($isValid) {
				$session->setCustomerOriginChooserCap(true);
				Mage::getSingleton('core/cookie')->set('customer_origin_modal-no-selection', 'false', 36000,null,null,null, false);
				$session->setCustomerOriginCap($cap);
			}
		}

		$this->_redirectReferer();
	}
}