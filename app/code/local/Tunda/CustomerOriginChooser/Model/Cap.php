<?php
class Tunda_CustomerOriginChooser_Model_Cap extends Mage_Core_Model_Abstract
{

	/**
	 * @return array
	 */
	protected function _getStoreConfigSettings()
	{
		return Mage::getStoreConfig('customer_origin_chooser/settings');
	}

	/**
	 * @param bool $asArray
	 *
	 * @return string|array
	 */
	public function getCapList($asArray = false) {
		$capList = $this->_getStoreConfigSettings()['cap'];
		if ( $asArray ) {
			return explode( ',', $capList );
		}

		return $capList;
	}

	public function validate($cap)
	{
		$capList = $this->getCapList(true);

		if(null !== $cap || !empty($cap)){
			if(in_array($cap, $capList)){
				return true;
			}
		}

		return false;
	}
}