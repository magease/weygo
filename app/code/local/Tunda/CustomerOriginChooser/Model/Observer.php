<?php

class Tunda_CustomerOriginChooser_Model_Observer
{
	/*public function catalogProductIsSalableAfter(Varien_Event_Observer $observer)
	{
		if(!Mage::helper('tunda_customeroriginchooser')->checkCapFlag()){
			//$product = $observer->getProduct();
			$salable = $observer->getSalable();
			$salable->setIsSalable(false);
			return $salable->getIsSalable();
		}
	}*/

	public function checkoutCartSaveBefore(Varien_Event_Observer $observer)
	{
		/** @var Mage_Checkout_Model_Cart $cart */
		$cart = $observer->getCart();
		if (!Mage::helper('tunda_customeroriginchooser')->checkCapFlag()) {
			$cart->getQuote()->preventSaving();
			$message = Mage::helper('core')->__('We cannot ship this product');

			if ($cart->getIsAjax()) {
				Mage::throwException($message);
			}

			$cart->getCheckoutSession()->addNotice($message);
		}
	}

	public function customerLogin(Varien_Event_Observer $observer)
	{
		$customerAddressId = $observer->getCustomer()->getDefaultShipping();
		if ($customerAddressId) {
			$address = Mage::getModel('customer/address')->load($customerAddressId);
			$postCode = $address->getPostcode();
			if(null !== $postCode){
				$session =Mage::getSingleton('checkout/session');
				$session->setCustomerOriginChooserCap(true);
				Mage::getSingleton('core/cookie')->set('customer_origin_modal-no-selection', 'false', 36000,null,null,null, false);
				$session->setCustomerOriginCap($postCode);
			}
		}
	}
}