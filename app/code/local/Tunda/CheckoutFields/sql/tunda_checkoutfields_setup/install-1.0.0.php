<?php
/** @var Mage_Customer_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer_address', 'tunda_numero_civico', array(
		'type'             => 'varchar',
		'input'            => 'text',
		'label'            => 'Numero civico',
		'position'         => 142,
		'global'           => true,
		'visible'          => true,
		'required'         => false,
		'user_defined'     => true,
		'visible_on_front' => true,
	)
);

$installer->addAttribute('customer_address', 'tunda_consegna_piano', array(
		'type'             => 'varchar',
		'input'            => 'text',
		'label'            => 'Consegna al piano',
		'position'         => 143,
		'global'           => true,
		'visible'          => true,
		'required'         => false,
		'user_defined'     => true,
		'visible_on_front' => true,
	)
);

$installer->addAttribute('customer_address', 'tunda_ascensore', array(
		'type'             => 'int',
		'input'            => 'select',
		'label'            => 'Ascensore?',
		'global'           => true,
		'visible'          => true,
		'required'         => false,
		'user_defined'     => true,
		'visible_on_front' => true,
		'source'           => 'eav/entity_attribute_source_boolean'
	)
);

$numeroCivico = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'tunda_numero_civico');
$numeroCivico->setData('used_in_forms', array(
	'adminhtml_customer_address',
	'customer_address_edit',
	'customer_register_address'
));
$numeroCivico->save();

$consegnaAlPiano = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'tunda_consegna_piano');
$consegnaAlPiano->setData('used_in_forms', array(
	'adminhtml_customer_address',
	'customer_address_edit',
	'customer_register_address'
));
$consegnaAlPiano->save();

$ascensore = Mage::getSingleton('eav/config')->getAttribute('customer_address', 'tunda_ascensore');
$ascensore->setData('used_in_forms', array(
	'adminhtml_customer_address',
	'customer_address_edit',
	'customer_register_address'
));

$ascensore->save();

$installer->endSetup();

$salesInstaller = Mage::getResourceModel('sales/setup','tunda_checkoutfields');
$salesInstaller->addAttribute("quote_address", "tunda_numero_civico", array("type"=>Varien_Db_Ddl_Table::TYPE_VARCHAR));
$salesInstaller->addAttribute("quote_address", "tunda_consegna_piano", array("type"=>Varien_Db_Ddl_Table::TYPE_TEXT));
$salesInstaller->addAttribute("quote_address", "tunda_ascensore", array("type"=>Varien_Db_Ddl_Table::TYPE_INTEGER));

$salesInstaller->addAttribute("order_address", "tunda_numero_civico", array("type"=>Varien_Db_Ddl_Table::TYPE_VARCHAR));
$salesInstaller->addAttribute("order_address", "tunda_consegna_piano", array("type"=>Varien_Db_Ddl_Table::TYPE_TEXT));
$salesInstaller->addAttribute("order_address", "tunda_ascensore", array("type"=>Varien_Db_Ddl_Table::TYPE_INTEGER));
$salesInstaller->endSetup();