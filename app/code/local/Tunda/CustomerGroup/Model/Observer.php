<?php
class Tunda_CustomerGroup_Model_Observer
{
	public function salesOrderSaveAfter(Varien_Event_Observer $observer)
	{

		/** @var Mage_Sales_Model_Order $order */
		$order  = $observer->getOrder();
		$customerId = $order->getCustomerId();
		$customer = $order->getCustomer();
		$customerIsGuest = (int)$order->getCustomerIsGuest();
		$autoswitchAmount = (int)Mage::getStoreConfig('tunda_customer_group/settings/amount');
		$autoswitchCustomerGroupId = (int)Mage::getStoreConfig('tunda_customer_group/settings/default_group');

		if (!$customerIsGuest) {
			if (null === $customer) { //check necessario per usare un solo observer sia in frontend che in backend
				$customer = Mage::getModel('customer/customer')->load($customerId);
			}

			if ((int)$customer->getGroupId() !== $autoswitchCustomerGroupId) { //controllo che non sia già del gruppo da associare
				$totals = Mage::getModel('sales/order')->getCollection()
				              ->addAttributeToFilter('status',Mage_Sales_Model_Order::STATE_COMPLETE) //voglio solo gli ordini completi
				              ->addFieldToFilter('customer_id',['eq'=>$customerId])
				              ->addAttributeToSelect('grand_total')
				              ->getColumnValues('grand_total')
				;



				$tot = array_sum($totals);
				if ($tot >= $autoswitchAmount) {
					try{
						$customer->setGroupId($autoswitchCustomerGroupId);
						$customer->save();
					}catch (Exception $e){
						Mage::logException($e);
					}

				}
			}
		}
	}
}