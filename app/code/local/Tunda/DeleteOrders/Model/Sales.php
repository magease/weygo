<?php 
class Tunda_DeleteOrders_Model_Sales extends Mage_Core_Model_Abstract
{
	public function cancelOldOrders()
	{
		$today = date('Y-m-d H:i:s');
		$delay = $storeConfig = Mage::getStoreConfig('tunda_delete_orders/settings/hours');
		$dateDiff = date('Y-m-d H:i:s', strtotime($today . ' - ' . $delay . ' hours'));
		$orders = Mage::getModel('sales/order')->getCollection()
		              ->addFieldToFilter('status', array('in' => array('pending', Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)))
		              ->addFieldTofilter('created_at', array('lt' => $dateDiff));

		echo "START\n";
		foreach($orders as $order)
		{
			var_dump($order->getIncrementId());
			$order->setState(Mage_Sales_Model_Order::STATE_CANCELED);
			$order->setStatus(Mage_Sales_Model_Order::STATE_CANCELED);
			$order->save();
		}
		echo "DONE\n";
	}
}