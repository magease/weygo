<?php
class Tunda_ImportExport_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function clear( $field )
	{
		$field = trim($field);
		$field = str_replace("\n", "", $field);
		$field = str_replace("\r", "", $field);
		return $field;
	}
}