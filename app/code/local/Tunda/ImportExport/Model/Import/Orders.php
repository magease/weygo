<?php 
class Tunda_ImportExport_Model_Import_Orders extends Tunda_ImportExport_Model_Import_Abstract
{
	
	protected function _construct()
	{
		parent::_construct();
		$this->_importPath.= 'orders' . DS . 'orders.csv';
		$this->_logFile = $this->_logPath.'orders.log';
	}
	
	protected function _import()
	{
		$csv = $this->_getCsvReader();
		//$csv->setLineLength(1000);
		$csv->setDelimiter('|');
		//$csv->setEnclosure('"');
		$rows = $csv->getData($this->_importPath);
		$i = 0;
		foreach($rows as $row)
		{
			$num = count($row);
			if ($i<=0)
			{
				$header = $row;
			}
			else
			{
				for ($c=0; $c<$num; $c++)
				{
					$data[strtolower($header[$c])] = $row[$c];
				}
				$this->_manageOrder($data);
			}
			$i++;
		}
	}
	
	protected function _manageOrder($data)
	{		
		$data = new Varien_Object($data);

		echo '<pre>';
		//var_Dump($data->getData());
		//die();
		
		$store = $this->_getStore($data->getStore());	
		if($store)
		{							
			$productId = Mage::getModel('catalog/product')->getIdBySku(trim($data->getSku()));						
			$_product = Mage::getModel('catalog/product')->load($productId);
			if($_product->getEntityId())
			{							
											    								
					$request = Mage::getSingleton('tax/calculation')->getRateRequest(null, null, null, $store);
			    	$taxclassid = $_product->getData('tax_class_id');
				    $productTax = Mage::getSingleton('tax/calculation')->getRate($request->setProductClassId($taxclassid));
				    	  
					$shippingTaxClass = Mage::getResourceModel('tax/calculation_rate_collection')
					    ->addFieldToFilter('code', 'IVA 22')
					    ->load()
					    ->getFirstItem();
				    $shippingTax = $shippingTaxClass->getRate();
				    
				    
				    $price = $data->getPrice();
				    $price = $price / (1 + ($productTax /100)); 
				    
					$_product->setPrice($price);
					$_product->setSpecialPrice();
					$_product->setTierPrice();
					
					$stock = $_product->getStockItem();
					if($stock->getQty() <= 0)
					{
						$this->_alerts[] = Mage::helper('sales')->__('ALERT: Sku \''.$_product->getSku().'\' has Quantity < 0');			
						$stock->setQty($data->getQty())
								->setIsInStock($data->getQauntita())
								->save();
					}
					
					$buyInfo = array(
							'qty' => $data->getQty(),
					);
		
	
					$customer = $this->_manageCustomer($data);
					
					$quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());
					$quote->setCustomerId($customer->getEntityId());
					$quote->setCustomerEmail($customer->getEmail());	
					$quote->setCustomerFirstname($customer->getFirstname());
					$quote->setCustomerLastname($customer->getLastname());
					$quote->addProduct($_product, new Varien_Object($buyInfo));
					
					$customerBillingAddress	= Mage::getModel('customer/address')->load($customer->getDefaultBilling());
					$quote->getBillingAddress()->addData($customerBillingAddress->getData());				
														
					$customerShippingAddress = Mage::getModel('customer/address')->load($customer->getDefaultShipping());
					$shippingAddress = $quote->getShippingAddress()->addData($customerShippingAddress->getData());
					
					$shippingAddress->setCollectShippingRates(true)->collectShippingRates()
		        			->setShippingMethod('flatrate_flatrate');
		        	
		        	$shippingPrice = $data->getShippingCosts();
			    	$shippingPrice = $shippingPrice / (1 + ($shippingTax /100)); 

			    	$rates = $quote->getShippingAddress()->getShippingRatesCollection();
			    	
			    	$rate = $rates->getFirstItem();
					$rate->setPrice($shippingPrice);
		   			
					$quote->getPayment()->importData(array('method' => 'purchaseorder'));
					$quote->collectTotals()->save();
					
					$service = Mage::getModel('sales/service_quote', $quote);
					$service->submitAll();
					$order = $service->getOrder();
											
					$order = Mage::getModel('sales/order')->load($order->getEntityId()); 
					$order->setCreatedAt($this->_formatDate($data->getDate()))
							->setUpdatedAt($this->_formatDate($data->getDate()))
							;
					$order->save();

					$order->setStatus('processing');
					$order->setState('processing');
					
					$order->save();
					
					$this->_counter++;
						
			}
		}
	}
	
	protected function _manageCustomer( $data )
	{		
		$email = $data->getEmail();
		$store = $this->_getStore($data->getStore());
		$customer = Mage::getModel('customer/customer');
		$customer->setStore($store);
		$customer->setWebsiteId($store->getWebsiteId());		
		$customer->loadByEmail($email);				
		if($customer->getEntityId())
		{						
			$customerAddressCollection = Mage::getResourceModel('customer/address_collection')
											->addAttributeToFilter('parent_id' ,$customer->getEntityId())
											->getItems();
        	foreach($customerAddressCollection as $customerAddress)
        	{
            	$customer_address_id = $customerAddress->getEntityId();
            	if($customer_address_id != '')
            	{            
               		Mage::getModel('customer/address')->load($customer_address_id)->delete();
            	}
			}
		}
		else 
		{
			$customer->setStore($store);
			$customer->setWebsiteId($store->getWebsiteId());
			$customer->setGroupId($this->_getCustomerGroupId($data->getCustomerGroup()));
			$customer->setEmail($email);			
			$customer->setPassword("password_".$store->getCode());
		}
				
		//$name = $this->_getCustomerName($data->getCliente());	
				
		
		$customer->setFirstname(ucwords(strtolower($data->getBillingFirstname())));
		$customer->setLastname(ucwords(strtolower($data->getBillingLastname())));			
		$customer->save();
		
		$countryId = $this->_getCountryId($data->getBillingCountry());						
		$regionId = $this->_getRegionId($countryId, $data->getBillingRegion());
		$billing = Mage::getModel("customer/address");		
		$billing->setCustomerId($customer->getId());
		$billing->setFirstname(ucwords(strtolower($data->getBillingFirstname())));
		$billing->setLastname(ucwords(strtolower($data->getBillingLastname())));
		$billing->setCountryId($countryId);
		$billing->setStreet($data->getBillingAddress());
		$billing->setPostcode($data->getBillingZip());
		$billing->setCity(ucwords(strtolower($data->getBillingCity())));
		if ($data->getBillingTelephone() == "")
		{
			$data->setBillingTelephone("123");
		}
		$billing->setTelephone($data->getBillingTelephone());
		$billing->setRegionId($regionId);			
		$billing->setIsDefaultBilling(1);

		$billing->save();
		
		if(!$data->getShippingFirstname())
		{
			$data->setShippingFirstname($data->getBillingFirstname());
		  	$data->setShippingLastname($data->getBillingLastname());
		  	$data->setShippingAddress($data->getBillingAddress());
		  	$data->setShippingCity($data->getBillingCity());
		  	$data->setShippingZip($data->getBillingZip());
		  	$data->setShippingRegion($data->getBillingRegion());
		  	$data->setShippingCountry($data->getBillingCountry());
		  	$data->setShippingTelephone($data->getBillingTelephone());
		}
				
		
		$countryId = $this->_getCountryId($data->getShippingCountry());			
		$regionId = $this->_getRegionId($countryId, $data->getShippingRegion());

		$shipping = Mage::getModel("customer/address");		
		$shipping->setCustomerId($customer->getId());
		$shipping->setFirstname(ucwords(strtolower($data->getShippingFirstname())));
		$shipping->setLastname(ucwords(strtolower($data->getShippingLastname())));
		$shipping->setCountryId($countryId);
		$shipping->setStreet($data->getShippingAddress());
		$shipping->setPostcode($data->getShippingZip());
		$shipping->setCity(ucwords(strtolower($data->getShippingCity())));
		if ($data->getShippingTelephone() == "")
		{
			$data->setShippingTelephone("123");
		}
		$shipping->setTelephone($data->getShippingTelephone());
		$shipping->setRegionId($regionId);
		$shipping->setIsDefaultShipping(1);		
		$shipping->save();
					
		return Mage::getModel('customer/customer')->load($customer->getEntityId());	
	}
		
}