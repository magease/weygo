<?php
class Tunda_ImportExport_Model_Import_Abstract extends Mage_Core_Model_Abstract
{
	const CUSTOMER_GROUP_ID = 1;
	
	protected $_importPath;
	protected $_archivedPath;
	protected $_logPath;
	
 	protected function _construct()
    {
    	set_time_limit(999999);
		ini_set('memory_limit','8192M');
    	$this->_importPath = Mage::getBaseDir('var').DS.'import'.DS;
    	$this->_archivedPath = $this->_importPath.'archived'.DS;
    	$this->_logPath =  Mage::getBaseDir('var').DS.'log'.DS.'import';
    	$this->_adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
    }
	
	public function import()
	{
		$this->_beforeImport();
		$this->_import();
		$this->_afterImport();
	}
	
	protected function _getCsvReader()
    {
    	return new Varien_File_Csv();
    }
    
	/*
	 * Leggo i file opportuni della cartella di import
	 */
	protected function _getFiles($path = null, $pattern = null)
	{	
		if(!$path)
		{
			$path = $this->_importPath;
		}
		$folder = str_replace($this->_importPath, '', $path);			
		$_files = array();
		$files = preg_grep('/^([^.])/', scandir($path));	
		$hidden_files = scandir($path);
		$this->_cleanfiles($path);
		if($files){
			foreach($files as $key => $file){				
				if($file!='.' && $file!='..')
				{								
					chmod($path.$file, 0777);					
					if(is_file($path.$file)) 
					{								
						if(isset($pattern))
						{
							preg_match($this->_pattern, $file, $matches);
							if($matches)
							{
								$_files[] = array('folder' => $folder, 'file' => $file);								
							}
						}else{							
							$_files[] = array('folder' => $folder, 'file' => $file);
						}						
					}
					elseif(is_dir($path.$file))
					{																					
						$_files+=$this->_getFiles($path.$file.DS, $pattern);						
					}
				}
			}
		}		
		return $_files;
	}
    
	protected function _moveFile($_file, $_newFile = null)
	{
		if(!$_newFile)
		{
			$_newFile = $file;
		}
		if (copy($_file, $_newFile)) {
			
			unlink($_file);
		}
	}
	
	protected function _cleanFiles($path){		
		foreach (scandir($path) as $key => $file)
		{
			if($file!='.' && $file!='..')
			{
				if($file == 'Thumbs.db')
				{
					unlink($path.$file);
				}
			}
		}
		foreach(array_diff(scandir($path),preg_grep('/^([^.])/', scandir($path) )) as $key => $file)
		{				
				if($file!='.' && $file!='..')
				{					
					chmod($path.$file, 0777);
					if(is_file($path.$file))						
					{
						//echo "unlink: ".$path.$file."<br />";						
						unlink($path.$file);
					}
					else 
					{														
						foreach(scandir($path.$file) as $ind => $sub)
						{													
							if($sub!='.' && $sub!='..')
							{
								//var_dump($path.$file.DS);																																
								if(is_file($path.$file.DS.$sub))						
								{
									//echo "unlink: ".$path.$file.DS.$sub."<br />";
									chmod($path.$file.DS.$sub, 0777);
									unlink($path.$file.DS.$sub);
								}
								else
								{
									//echo 'cartella: '; var_dump($path.$sub);
									//die('ricorro');
									$this->_cleanFiles($path.$sub);
									chmod($path, 0777);
									rmdir($path);
									//echo "rmdir: ".$path."<br />";								
								}								
							}															
						}						
						rmdir($path.$file);
						//echo "rmdir: ".$path.$file."<br />";												
					}
				} 
		}		
	}
	
	protected function _getStore( $storeCode )
	{
		$storeCode = strtolower($storeCode);
		$stores = array_keys(Mage::app()->getStores());
		if($storeCode)
		{
			foreach($stores as $id)
			{
				$store = Mage::app()->getStore($id);
				if($store->getCode() == $storeCode)
				{
					return $store;
				}
			}
		}
		return null;
	}
	
	protected function _getCountryId( $countryName )
	{
		$countryId = 'IT';
		$countryCollection = Mage::getModel('directory/country')->getCollection();
		foreach ($countryCollection as $country)
		{
			if ($countryName == $country->getName())
			{
				$countryId = $country->getCountryId();
				break;
			}
		}
		return $countryId;
	}
	
	protected function _getRegionId( $countryId, $regionName )
	{
		$regionId = 1;
		$regionCollection = Mage::getModel('directory/region_api')->items($countryId);
		foreach ($regionCollection as $region)
		{
			if($region['name'] == $regionName)
			{
				$regionId = $region['region_id'];
				break;
			}
			elseif($region['code'] == strtoupper($regionName))
			{
				$regionId = $region['region_id'];
				break;
			}
	
		}
		return $regionId;
	}
	
	protected function _getCustomerGroupId( $groupName )
	{
		$return = self::CUSTOMER_GROUP_ID;
		if($groupName)
		{
			$customerGroup = Mage::getModel('customer/group')->load($groupName, 'customer_group_code');
			$return = $customerGroup->getCustomerGroupId();
		}
		return $return;
	}
	
	protected function _formatDate( $date )
	{
		return date('Y-m-d', strtotime(str_replace('/', '-', $date)));
	}
	
	public function helper()
	{
		return Mage::helper('tunda_importexport');
	}
	
    protected function _beforeImport()
    {    	
    	
    } 
      
    protected function _import()
    {    	
    } 
    
    protected function _afterImport()
    {    	
    }
}