<?php
class Tunda_ImportExport_Model_Import_Photo extends Tunda_ImportExport_Model_Import_Abstract
{		
	protected function _construct()
	{	
		parent::_construct();		
		$this->_importPath.= 'photo' . DS;
		$this->_archivedPath.= 'processed' . DS ;
		$this->_logFile = $this->_logPath.'photo.log';
		$this->_move = true;
		$this->_exclude = false;		
	}
	
	protected function _import()
	{
		//echo '<pre>';
		$_sku = null;
		var_dump($this->_importPath);
		$gallery = array();				
		foreach($this->_getFiles($this->_importPath, $this->_pattern) as $file)
		{
			//var_dump($file);						
			$tmp = explode('.', $file['file']);
			$imgProduct = explode('_', $tmp[0]);
			$newSku = trim($imgProduct[0]);	
			$position = $imgProduct[1];
			if($_sku){
				var_dump($_sku);
				if($_sku != $newSku){
					$this->addGalleryToProduct($gallery);
					$gallery = array();					
				}											
			}
			$gallery[$newSku][$position] = $file;
			$_sku = $newSku;								
		}
		$this->addGalleryToProduct($gallery);		
	}
	
	public function addGalleryToProduct( array $gallery )
	{
		if($gallery)
		{
			//var_dump($gallery);
			foreach ($gallery as $_sku => $files)
			{
				$_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$_sku);
				if($_product)
				{								
					if($_product->getTypeId() == "configurable")
					{
						$this->_deleteChildrenImages($_product);						
						$this->_assignChildrenImages($_product, $files, false, $this->_exclude);													
					}
					$this->_deleteImages($_product);
					$this->_assignImages($_product, $files, $this->_move, $this->_exclude);					
					if($this->_move)
					{
						$this->_moveFile($files);
							
					}																																																										 
				}
			}	
		}		
	} 
	
	protected function _deleteImages( $_product )
	{
		$mediaApi = Mage::getModel("catalog/product_attribute_media_api");
    	$items = $mediaApi->items($_product->getId());
    	foreach($items as $item)
    	{
        	$mediaApi->remove($_product->getId(), $item['file']);
    	}    			
	}
	
	protected function _assignImages( $_product, $files, $move, $exclude )
	{
		//echo "<br />SKU: ".$_product->getSku().'<br />';
		foreach($files as $position => $image)
		{						
			$file = $this->_renameImageAsProduct($_product, $image, $position);
			//echo 'position: '; var_dump($position);
			//echo 'move: '; var_dump($move);								
			if($position == 1){
				//echo '<br />IMMAGINE PRINCIPALE<br />';
				$_product->addImageToMediaGallery($file, array('image', 'small_image', 'thumbnail'), $move, $exclude);
			}else{
				//echo '<br />GALLERIA<br />';
				$_product->addImageToMediaGallery($file, null, $move, $exclude);
			}
			if(!$this->_move){		
				unlink($file);
			}        						
		}
		
		$_product->save();		
	}
	
	protected function _renameImageAsProduct( $_product, $image, $position )
	{		
		$oldFile = $this->_importPath.$image['folder'].$image['file'];
		//var_dump($oldFile);		
		$tmp = explode('.', $image['file']);
		$extension = $tmp[1];		
		$name = strtolower(
							str_replace(' ', '-', 
								str_replace('-', '', 
									preg_replace('/[^\p{L}\p{N}\s]/u',' ',
										trim(
											$_product->getAttributeText('manufacturer').' '.$_product->getName()
											)
										)
									)
								)
							);
		$newFile = $this->_importPath.$image['folder'].$name.'_'.$position.'.'.$extension;
		//var_dump($newFile);		
		if(!file_exists($newFile)) 
		{		
			//var_dump('copio');			
			copy($oldFile , $newFile);			
		}		
		return $newFile;
	}
	
	protected function _deleteChildrenImages( $_product )
	{
		$childrenIds = Mage::getModel('catalog/product_type_configurable')
							->getChildrenIds($_product->getId());
		if(count($childrenIds[0]) > 0)
		{							
			foreach($childrenIds[0] as $key => $childId)
			{
				$_child = Mage::getModel('catalog/product')->load($childId);
				if($_child)
				{					
					$this->_deleteImages($_child);
				}	
			}
		}
	}
	
	protected function _assignChildrenImages( $_product, $files, $move, $exclude )
	{
		$childrenIds = Mage::getModel('catalog/product_type_configurable')
							->getChildrenIds($_product->getId());
		if(count($childrenIds[0]) > 0)
		{							
			foreach($childrenIds[0] as $key => $childId)
			{
				$_child = Mage::getModel('catalog/product')->load($childId);
				if($_child)
				{									
					$this->_assignImages($_child, $files, $move, $exclude);
				}	
			}
		}
	}
	
	protected function _moveFile($_file, $_newFile = null)
	{
		foreach($_file as $position => $image)
		{
			@mkdir($this->_archivedPath.$image['folder']);
							
			$oldFile = $this->_importPath.$image['folder'].$image['file'];
			$newFile = $this->_archivedPath.$image['folder'].$image['file'];
			parent::_moveFile($oldFile, $newFile);
			if($image['folder'])
			{			
				@rmdir($this->_importPath.$image['folder']);
			}
		}	
	}
}