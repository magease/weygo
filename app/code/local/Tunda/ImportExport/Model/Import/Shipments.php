<?php

class Tunda_ImportExport_Model_Import_Shipments extends Tunda_ImportExport_Model_Import_Abstract
{
	protected $_Path;
	protected $_Name;
	
	public function _construct()
	{
		$this->_db = Mage::getSingleton('core/resource')->getConnection('core_read');
		//$this->_importPath = Mage::getBaseDir('var').DS.'import'.DS.'vendors'.DS.'Gilmar'.DS.'returns'.DS;	
		//$this->_Path = $this->_importPath;
		//$this->_pattern = "/10SHEA/";
	}
	
	public function _import()
	{
		echo "start";
		$resource = Mage::getSingleton('core/resource');
		$con = $resource->getConnection('core_write');
		$sql = "SELECT * FROM nav_order_status WHERE imported =0 AND status='SHIPPED' and created_at>='".date('Y-m-d H:i:s', time() - 60 * 60 * 24)."'";
		$orderStatuses = $con->fetchAll($sql);
		echo "strae";
		foreach ($orderStatuses as $orderStatus)
		{
			$this->_manageData($orderStatus);	
			
			$con->query($sql);	
		}
 	}
		

	protected function _manageData($orderStatus)
	{
		$resource = Mage::getSingleton('core/resource');
		$con = $resource->getConnection('core_write');
		
		echo "-->" . $orderStatus['increment_id']."\n";
		
		
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderStatus['increment_id']);

			if ($order->getEntityId()) 
			{
				//echo "her1";
				if ($orderStatus['status']=="SHIPPED")
				{
			//echo "here3333333333333";
			
				 if ($order->canShip()) {
				 	
				 	//echo "here4444444";
					  /*      try {
					        	echo "555555";
					        	
					            $shipment = Mage::getModel('sales/service_order', $order);
					            echo "2222222";
					                     $shipment->prepareShipment($this->_getItemQtys($order));
					            echo "66666";
					            $shipmentCarrierCode = 'SPECIFIC_CARRIER_CODE';
					            $shipmentCarrierTitle = 'SPECIFIC_CARRIER_TITLE';
					            echo "666666";
					            $arrTracking = array(
					                'carrier_code' => isset($shipmentCarrierCode) ? $shipmentCarrierCode : $order->getShippingCarrier()->getCarrierCode(),
					                'title' => isset($shipmentCarrierTitle) ? $shipmentCarrierTitle : $order->getShippingCarrier()->getConfigData('title'),
					                'number' => $shipmentTrackingNumber,
					            );
					            echo "777777";
					            $track = Mage::getModel('sales/order_shipment_track')->addData($arrTracking);
					            $shipment->addTrack($track);
					            echo "88888888";
					            // Register Shipment
					            $shipment->register();
					            echo "999999";
					            // Save the Shipment
					            $this->_saveShipment($shipment, $order, $customerEmailComments);
					            echo "0000000";
					            // Finally, Save the Order
					            $this->_saveOrder($order);
					        } catch (Exception $e) {
					            throw $e;
					            var_dump($e);
					        }*/
					    }
					   // else
					   // 	echo "non posso shippare";
					$new_status="shipped";
					$new_state="shipped";
					
					
				}
				elseif ($orderStatus['status']=="READ"){
					$new_status="logistic";
					$new_state="logistic";
					
					$sql = 'UPDATE nav_order_status SET imported = 1 WHERE id = '.$orderStatus['id'];
					$con->query($sql);
					
				}
			
				//echo "passo3";
				if (($order->getStatus()!="shipped") && ($order->getStatus()!="complete") && ($order->getStatus()!="canceled") && ($order->getStatus()!="closed") )
				{
				//salvo l'ordine
				//var_dump($order_nr,$new_state,$new_status);

					try {
						$order->setStatus($new_status,$new_state,"change status to".$new_status)->save();
						
					} catch (Exception $e) {
						throw $e;
					}
				}
				
				
			}
		
	}

	
	protected function _getItemQtys(Mage_Sales_Model_Order_Item $item)
	{
		$qty = array();
	
		if ($item->getParentItemId())
		{
			$qty[$item->getParentItemId()] = $item->getQtyOrdered();
		} else {
			$qty[$item->getId()] = $item->getQtyOrdered();
		}
		
		return $qty;
	}
	
	 
	/**
	 * Saves the Shipment changes in the Order
	 *
	 * @param $shipment Mage_Sales_Model_Order_Shipment
	 * @param $order Mage_Sales_Model_Order
	 * @param $customerEmailComments string
	 */
	protected function _saveShipment(Mage_Sales_Model_Order_Shipment $shipment, Mage_Sales_Model_Order $order, $customerEmailComments = '')
	{
		die("save");
		$resource = Mage::getSingleton('core/resource');
		$con = $resource->getConnection('core_write');			
		
		$shipment->getOrder()->setIsInProcess(true);
		$transactionSave = Mage::getModel('core/resource_transaction')
							   ->addObject($shipment)
							   ->addObject($order)
							   ->save();
		$customerEmailComments="";
		
		
		/* CHECK ORDINE AMAZON O ALTRO */
		$sql = "SELECT * from m2epro_order WHERE magento_order_id = ".$order->getId();
		$externalOrder = $con->fetchAll($sql);
		
		if (count($externalOrder) <= 0) 
		{
			$shipment->sendEmail(true, $customerEmailComments);
			$shipment->setEmailSent(true);
		}					   
		/* END CHECK ORDINE AMAZON O ALTRO */
		
		/*
		$emailSentStatus = $shipment->getData('email_sent');
		if (!is_null($customerEmail) && !$emailSentStatus) {
			$shipment->sendEmail(true, $customerEmailComments);
			$shipment->setEmailSent(true);
		}
		
		*/
		return $this;
	}

	
	
	
}
