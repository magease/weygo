<?php
class Tunda_ImportExport_Model_Import_Customers 
extends Tunda_ImportExport_Model_Import_Abstract
{	
	protected function _construct()
	{	
		parent::_construct();		
		$this->_importPath.= 'customers' . DS . 'clienti.csv';		
		$this->_logFile = $this->_logPath.'customers.log';		
	}
	
	protected function _import()
	{
		$csv = $this->_getCsvReader();
		//$csv->setLineLength(1000);
		$csv->setDelimiter(';');
		//$csv->setEnclosure('"');
		//$csv->setDelimiter(',');
		$csv->setEnclosure('"');
		$rows = $csv->getData($this->_importPath);
		$i = 0;						
		foreach($rows as $row)
		{
			if($i > 0){				
				try{						
					$customer = $this->_setCustomer($row);					
				}catch (Exception $e){
					echo "Can't save the Customer ". $customer->getEmail().": ",  $e->getMessage(), "\n";
				}
			}
			$i++;																
		}			
	}
	
	/**
	 * Protected function to match all row filelds width
	 * customer fields.
	 * 
	 * @param array $row
	 * @return Mage_Customer_Model_Customer
	 */
	protected function _setCustomer( $row )
	{
		$customer = Mage::getModel('customer/customer');		
		/* Here match the fields
		 * example: 
		 *
		 * $customer->setName($row[0]);		 		
		 */
		return $customer; 		
	}
	
	protected function _getRegion( $countryId, $field )
	{	
		$regionCollection = Mage::getModel('directory/region_api')->items($countryId);		
		foreach ($regionCollection as $region)
		{		
			if(
				(strtolower($region['name']) == strtolower($field))
				|| ($region['code'] == strtoupper($field))
			)
			{				
				return $region;
			}
		}		
	}
}