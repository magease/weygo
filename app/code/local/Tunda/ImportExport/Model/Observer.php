<?php 
class Tunda_ImportExport_Model_Observer
{
	public function exportNewOrder( $observer )
	{
		$order = $observer->getEvent()->getOrder();
		Mage::getModel('tunda_importexport/export_orders')->export($order);


		/*
		$block = Mage::app()->getLayout()->createBlock('paypalph/redirect', 'paypalph_redirect', array('template' => 'paypalph/redirect.phtml'));
		$block->setOrder($order);
		$block->toHtml(); 		
		die();
		*/
		
	}
}