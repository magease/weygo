<?php 
class Tunda_ImportExport_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action
{
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('system/importexport')
			->_addBreadcrumb($this->helper()->__('System'), $this->helper()->__('Import Photo'));		
		return $this;
	}   
	
	public function photoAction()
    {       	
    	$model = Mage::getModel('tunda_importexport/import_photo');
		$return = $model->import();
    }	
    
	public function backupPhotoAction()
    {       	
    	$model = Mage::getModel('tunda_importexport/import_backupPhoto');
		$return = $model->import();
    }
    
    public function customersAction()
    {       	
    	$model = Mage::getModel('tunda_importexport/import_customers');
		$return = $model->import();
    }
    
    public function ordersAction()
    {
    	$model = Mage::getModel('tunda_importexport/import_orders');
    	$return = $model->import();
    }
    
}