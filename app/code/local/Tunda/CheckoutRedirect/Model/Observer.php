<?php
class Tunda_CheckoutRedirect_Model_Observer extends Mage_Core_Model_Abstract
{
	public function redirect(Varien_Event_Observer $observer)
	{
		if(!Mage::getSingleton('customer/session')->isLoggedIn()){
			/** @var Mage_Checkout_Controller_Action $test */
			$controllerAction = $observer->getControllerAction();
			$controllerAction->setRedirectWithCookieCheck('customer/account/login');
		}
	}
}