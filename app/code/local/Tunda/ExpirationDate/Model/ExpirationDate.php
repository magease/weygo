<?php

/**
 * Class Tunda_ExpirationDate_Model_ExpirationDate
 */
class Tunda_ExpirationDate_Model_ExpirationDate extends Mage_Core_Model_Abstract
{
	protected $_list = [];

	/**
	 * Controlla tutte le date di scadenza dei prodotti.
	 * Se un prodotto è scaduto o scade oggi lo disabilita.
	 *
	 * Ritorna un array dei prodotti scaduti con sku e nome
	 *
	 * @return array
	 */
	public function checkDates()
	{
		$collection = Mage::getModel('catalog/product')
		                  ->getCollection()
		                  ->addAttributeToSelect('*')
		                  ->addAttributeToFilter('status', ['eq'=>Mage_Catalog_Model_Product_Status::STATUS_ENABLED])
		                  ->addAttributeToFilter('weygo_expiration_date',['lteq'=>[Mage::getModel('core/date')->date('Y-m-d H:i:s')]])
		                  ->load()
		;

		foreach ($collection as $product) {
			try{
				$product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
				$product->setUrlKey(false);
				$product->save();
			}catch (Exception $e){
				Mage::logException($e);
			}

			$pl = ['sku'=>$product->getSku(), 'name'=>$product->getName()];
			$this->_list[] = $pl;
		}
		return $this->_list;
	}
}