<?php

class Tunda_Coupon_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected function _getCustomerGroupId()
	{
		return $custGroupID = Mage::getSingleton('customer/session')->getCustomerGroupId();
	}

	/**
	 * @return Mage_SalesRule_Model_Resource_Rule_Collection
	 */
	public function getCouponList()
	{
		$storeId = Mage::app()->getStore()->getId();
		$websiteId     = Mage::app()->getStore($storeId)->getWebsiteId();
		$customerGroup = $this->_getCustomerGroupId();
		$collection = Mage::getModel('salesrule/rule')->getResourceCollection();
		$now           = date('Y-m-d');
		$collection->addWebsiteGroupDateFilter($websiteId, $customerGroup, $now)
		           ->addFieldToFilter('coupon_type',['neq'=>1]) //voglio solo le regole con un codice coupon
		           ->setOrder('from_date', 'desc');
		$collection->load();
		return $collection;
	}

	/**
	 * @param $rule
	 * @param $customer
	 *
	 * @return bool
	 */
	public function checkIfUsed($rule, $customer)
	{
		$check = Mage::getResourceModel('salesrule/rule_customer_collection');
		$check
			->addFieldToSelect('times_used')
			->addFieldToFilter('customer_id', $customer->getId())
			->addFieldToFilter('rule_id', $rule->getId());
		return (bool)$check->count();
	}

	/**
	 * @return int
	 */
	public function getUnsedCoupon()
	{
		$customer =  Mage::getSingleton('customer/session')->getCustomer();
		$collection = $this->getCouponList();
		$result = $collection->count();
		if ($collection->count() !== 0) {
			foreach($collection as $coupon){
				if(Mage::helper('tunda_coupon')->checkIfUsed($coupon, $customer)){
					$result--;
				}
			}
		}
		return $result;

	}
}