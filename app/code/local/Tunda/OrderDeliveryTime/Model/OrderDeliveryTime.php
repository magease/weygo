<?php

class Tunda_OrderDeliveryTime_Model_OrderDeliveryTime extends Mage_Core_Model_Abstract
{
	protected function _construct()
	{
		$this->_init('tunda_orderdeliverytime/orderDeliveryTime');
	}
}