<?php

class Tunda_OrderDeliveryTime_Model_Resource_OrderDeliveryTime extends Mage_Core_Model_Resource_Db_Abstract
{
	protected function _construct()
	{
		$this->_init('tunda_orderdeliverytime/orderdeliverytime','id');
	}
}