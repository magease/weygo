<?php

class Tunda_OrderDeliveryTime_Model_Resource_OrderDeliveryTime_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	protected function _construct()
	{
		$this->_init('tunda_orderdeliverytime/orderDeliveryTime');
	}
}