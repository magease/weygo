<?php

class Tunda_OrderDeliveryTime_Model_Observer
{

	public function salesQuoteSaveBefore(Varien_Event_Observer $observer) //questo si
	{
		$quote = $observer->getEvent()->getQuote();
		$deliveryTime = Mage::app()->getFrontController()->getRequest()->getParam('delivery_time');

		if(null !== $deliveryTime){
			$quote->setDeliveryTime($deliveryTime);
		}
	}

	public function salesConvertQuoteToOrder(Varien_Event_Observer $observer) //questo si
	{
		$observer->getEvent()->getOrder()->setDeliveryTime($observer->getEvent()->getQuote()->getDeliveryTime());
		return $this;
	}

}