<?php

class Tunda_OrderDeliveryTime_Block_Checkout_Onepage_OrderDeliveryTime extends Mage_Core_Block_Template
{
	protected $_rangeTimeCollection = null;

	protected function _getRangeTimeCollection()
	{
		$resource = Mage::getSingleton('core/resource');
		$db = $resource->getConnection('core_read');
		
		
		if($this->_rangeTimeCollection !== null){
			return $this->_rangeTimeCollection;
		}

		$resultCollection = new Varien_Data_Collection();

		//recupero il cap dall'indirizzo di spedizione e lo passo alla collection per recuperare gli orari
		$cap = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getData('postcode');

		$time = Mage::getModel('core/date')->date('H:i:s', time());
		$firstCollection = Mage::getModel('tunda_orderdeliverytime/orderDeliveryTime')
		                       ->getCollection()
		                       ->addFieldToFilter('ora',["gt"=> new Zend_Db_Expr("(sec_to_time(time_to_sec('".$time."') + (main_table.delay*3600)))")])
							//->addFieldToFilter('ora',["gt"=> new Zend_Db_Expr("(sec_to_time(time_to_sec('".$time."') ))")])
		                       ->addFieldToFilter('cap', $cap)
		                       ->setOrder('ora', 'ASC')
		;
		//$select = $firstCollection->getSelect()->__toString();
		$j = 1;

		//primo giro, recupero le date odierne
		foreach ($firstCollection as $item) {
			$origItemData = $item->getData();
			$name = $item->getName();
			$object = new Varien_Object();
			$object->setData($origItemData);
			//$object = Mage::getModel('tunda_orderdeliverytime/orderDeliveryTime')->setData($origItemData);
			$newDate =  Mage::getModel('core/date')->date('d/m ');
			$object->setId($j);
			$object->setName($newDate . $name);
			$object->setRange($name);
			$object->setTimeStamp(Mage::getModel('core/date')->timestamp());
			
			$spediti  = $db->fetchOne("SELECT COUNT(*) FROM sales_flat_order where delivery_time='".$newDate . $name."'");
			if ($spediti<=10)
				$resultCollection->addItem($object);
			//die($newDate . $name);
			++$j;
		}


		$secondCollection = Mage::getModel('tunda_orderdeliverytime/orderDeliveryTime')
		                        ->getCollection()
		                        ->addFieldToFilter('cap', $cap)
		                        ->setOrder('ora', 'ASC');

		$days = 6;

		if ($resultCollection->count() === 0) { //se per oggi sono finiti i range, aggiungi un giorno in più
			$days++;
		}
		//$a è impostato a 1 e non a 0 perché viene utilizzato direttamente per il conto dei giorni #44
		for ($a = 1; $a<=$days; ++$a) {
			foreach ($secondCollection as $item) {
				$origItemData = $item->getData();
				$name = $item->getName();
				$object = new Varien_Object();
				$object->setData($origItemData);
				//$object = Mage::getModel('tunda_orderdeliverytime/orderDeliveryTime')->setData($origItemData);
				$newDate =  Mage::getModel('core/date')->date('d/m/ ', strtotime("+".$a." day"));
				$object->setTimeStamp(Mage::getModel('core/date')->timestamp(strtotime("+".$a." day")));
				$object->setRange($name);
				$object->setId($j);
				$object->setName($newDate . $name);
				
				$spediti  = $db->fetchOne("SELECT COUNT(*) FROM sales_flat_order where delivery_time='".$newDate . $name."'");
				if ($spediti<=10)
						$resultCollection->addItem($object);
				++$j;
			}
		}

		return $this->_rangeTimeCollection = $resultCollection;
	}

	public function getRange()
	{
		$collection = $this->_getRangeTimeCollection();
		$result = [];
		foreach($collection as $item){
			$timestamp = $item->getTimeStamp();
			$result[$timestamp][] = $item;
		}
		return $result;
	}

	public function getOptions()
	{
		$collection = $this->_getRangeTimeCollection();
		$options    = $collection->toOptionArray();
		return $options;
	}
}