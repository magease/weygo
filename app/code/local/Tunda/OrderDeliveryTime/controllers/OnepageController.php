<?php
require_once(Mage::getModuleDir('controllers','GoMage_Checkout').DS.'OnepageController.php');

class Tunda_OrderDeliveryTime_OnepageController extends GoMage_Checkout_OnepageController
{
	public function indexAction()
	{

		$helper = Mage::helper('gomage_checkout');

		if (( bool )$helper->getConfigData('general/enabled') == false) {
			return $this->_redirect('checkout/onepage');
		}
		$quote = $this->getOnepage()->getQuote();

		if (!$quote->hasItems()) {
			$this->_redirect($this->getCartPath());
			return;
		}

		if (!$quote->validateMinimumAmount()) {
			$error = Mage::getStoreConfig('sales/minimum_order/error_message');
			Mage::getSingleton('checkout/session')->addError($error);
			if (!$helper->getConfigData('general/disable_cart')) {
				$this->_redirect('checkout/cart');
				return;
			}
			$warning = Mage::getStoreConfig('sales/minimum_order/description');
			Mage::getSingleton('checkout/session')->addNotice($warning);
		}

		Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
		$this->getOnepage()->initCheckout();

		$calculator = Mage::getModel('gomage_checkout/type_onestep_calculator', $this->getRequest());
		$calculator->cleanCache();

		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->_initLayoutMessages('checkout/session');

		$title = $helper->getConfigData('general/title');
		$this->getLayout()->getBlock('head')->setTitle($title ? $title : $this->__('Checkout'));
		//blocco valerio
		$quote = $this->getOnepage()->getQuote();
		$customerOriginCap = Mage::getSingleton('checkout/session')->getCustomerOriginCap();
		$quotePostCode = $quote->getShippingAddress()->getPostcode();

		//se non è stato impostato il cap e non esiste nemmeno nell'indirizzo predefinito rimando al carrello
		if (null == $customerOriginCap && null == $quotePostCode ){
			$message = Mage::helper('core')->__('We cannot ship this product');
			Mage::getSingleton('checkout/session')->addNotice($message);
			$this->_redirect('checkout/cart');
		}

		if(null !== $customerOriginCap && null == $quotePostCode){
			$quote->getShippingAddress()->setPostcode($customerOriginCap);
			$quote->save();
		}
		//fine blocco valerio
		$this->renderLayout();
	}

	public function printTotalsAction()
	{
		//$this->loadLayout();
		/** @var Mage_Checkout_Block_Onepage $block */
		$block = $this->getLayout()->createBlock('checkout/onepage')->setTemplate('tunda/orderdeliverytime/checkout/onepage.phtml');
		$childBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('gomage/checkout/onepage/review/totale_complessivo.phtml');
		$block->setChild('totale_complessivo',$childBlock);
		$this->getResponse()->setBody($block->toHtml());
	}

	public function printOrderDeliverTimeBlock()
	{

	}
}