<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;

$installer->startSetup();
//create table tunda_order_delivery_time
$table = $this->getConnection()->newTable($this->getTable('tunda_order_delivery_time'));

$table->addColumn(
	'id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, [
		'identity' => true,
	    'primary' =>true,
	    'unsigned' => false,
	    'nullable' =>false
	], null
);

$table->addColumn('cap',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,['nullable'=>false],null);
$table->addColumn('name',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255,['nullable'=>false],null);
$table->addColumn('delay',Varien_Db_Ddl_Table::TYPE_VARCHAR,255,['nullable'=>false],null);
$table->addColumn('ora',Varien_Db_Ddl_Table::TYPE_TIME,6,['nullable'=>false],null);
$table->addColumn('max_ordini',Varien_Db_Ddl_Table::TYPE_INTEGER,10,['nullable'=>false],null);


$this->getConnection()->createTable($table);


$installer->endSetup();


$salesInstaller = Mage::getResourceModel('sales/setup','tunda_order_delivery_time');

$salesInstaller->startSetup();

$salesInstaller->getConnection()->addColumn(
	$salesInstaller->getTable('sales/quote'), 'delivery_time', array(
	'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
	'length'  => 255,
	'comment' => 'Order delivery time by Tunda'
	)
);

$table = $salesInstaller->getTable('sales/order');

$installer->getConnection()->addColumn(
	$installer->getTable('sales/order'), 'delivery_time', [
	'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
	'length'  => 255,
	'comment' => 'Order delivery time by Tunda'
]);

$salesInstaller->endSetup();
