<?php
/**
 * @category    Bubble
 * @package     Bubble_FullPageCache
 * @version     3.5.1
 * @copyright   Copyright (c) 2016 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_FullPageCache_Model_Url_Container extends ArrayObject
{
    public function __construct()
    {
        Mage::getSingleton('core/session')->setAutoGenerateUrls($this);
    }
}