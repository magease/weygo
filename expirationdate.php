<?php
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', 0);

require 'app/Mage.php';
Mage::setIsDeveloperMode(true);
Mage::app('admin');

error_reporting(E_ERROR);

$list = Mage::getModel('tunda_expirationdate/expirationDate')->checkDates();

$mail = new Zend_Mail();
$bodyText = '';
if (count($list) !== 0) {
	foreach ( $list as $product ) {
		$bodyText .= $product['sku'] . ' - ' . $product['name'] . PHP_EOL;
	}

	$mail->setBodyText( $bodyText )
	     ->setFrom( 'info@weygo.it' )
	     ->setSubject( 'Prodotti scaduti Weygo' )
	     ->addTo( 'm.camoni@tunda.com' );

	if ( $mail->send() ) {
		echo 'email inviata con successo' . PHP_EOL;
	} else {
		echo 'errore nell\'invio della mail' . PHP_EOL;
	}
}